// -*- c++ -*-
#pragma once
// clang-format off

/*
 *
 *  Originally from Graphics Gems 1, A. Glassner. Academic Press, 1990.
 *  From chapter VIII.2 and XI.7:
 *  Solving the Nearest Point-on-Curve Problem 
 *  and
 *  A Bezier Curve-Based Root-Finder
 *  by Philip J. Schneider
 */

/*
 *
 *  Code imported from:
 *  https://github.com/erich666/GraphicsGems/blob/master/gems/NearestPoint.c
 *
 *  ConvertToBezierForm :
 *		Given a point, P,  and a Bezier curve, Q, generate a 5th-degree
 *		Bezier-format equation whose solution finds the point on the
 *              curve nearest the user-defined point.
 *
 *  Adoption to C++11 and annotation by Ola Nilsson
 *
 *   [Q(t) - P] \cdot Q'(t) = 0
 *   Q1(t) \cdot Q'(t) = 0,
 *   Q1(t) = Q(t)-P = \sum_{i=0}^{n} c_i B_i^n(t),
 *                    c_i = V_i - P
 *   Q'(t) = n\sum_{i=0}^{n-1} ( V_{i+1} - V_i ) B_i^{n-1}
 *         = \sum_{i=0}^{n-1} d_i B_i^{n-1},
 *           d_i = n(V_{i+1} - V_i)
 *       0 = \sum_{i=0}^{n} c_i B_i^n(t) \cdot \sum_{j=0}^{n-1} d_j B_j^{n-1}
 *       0 = \sum_{i=0}^{n} \sum_{j=0}^{n-1} c_i \cdot d_j B_i^n(t) B_j^{n-1}
 *       0 = \sum_{i=0}^{n} \sum_{j=0}^{n-1} c_i \cdot d_j {n\choose i}(1-t)^{n-i}t^i {n-1\choose j}(1-t)^{n-1-j}t^j
 *       0 = \sum_{i=0}^{n} \sum_{j=0}^{n-1} c_i \cdot d_j {n\choose i}{n-1\choose j} (1-t)^{(2n-1)-(i+j)}t^{i+j}
 *       0 = \sum_{i=0}^{n} \sum_{j=0}^{n-1} c_i \cdot d_j \frac{{n\choose i}{n-1\choose j}}{{2n-1\choose i+j}} B_{i+j}^{2n-1}(t)
 *       0 = \sum_{i=0}^{n} \sum_{j=0}^{n-1} c_i \cdot d_j z_{i,j} B_{i+j}^{2n-1}(t)
 *           z_{i,j}={n\choose i}{(n-i)\choose j}/{2n-1 \choose i+1}
 *
 *   The final coeffients, c_i \cdot d_j z_{i,j}, are scalar valued,
 *   the 'x'-component is set to be linearly spaced in [0,1].
 *
 */
// clang-format on
#include <array>

template<typename Vec2d>
inline std::array<Vec2d, 5 + 1> ConvertToBezierForm(
    const Vec2d& P,                    /* The point to find t for	*/
    const std::array<Vec2d, 3 + 1>& V) /* The control points		*/
{
    constexpr int DEGREE = 3, W_DEGREE = 5;

    /* Determine the c's -- these are vectors created by subtracting*/
    /* point P from each of the control points				*/
    Vec2d c[DEGREE + 1]; /* V(i)'s - P			*/
    for (int i = 0; i <= DEGREE; i++) {
        c[i] = V[i] - P;
    }

    /* Determine the d's -- these are vectors created by subtracting*/
    /* each control point from the next					*/
    Vec2d d[DEGREE]; /* V(i+1) - V(i)		*/
    for (int i = 0; i <= DEGREE - 1; i++) {
        d[i] = (V[i + 1] - V[i]) * 3.0;
    }

    /* Create the c,d table -- this is a table of dot products of the */
    /* c's and d's							*/
    double cdTable[DEGREE][DEGREE + 1]; /* Dot product of c, d		*/
    for (int row = 0; row <= DEGREE - 1; row++) {
        for (int column = 0; column <= DEGREE; column++) {
            cdTable[row][column] = dot(d[row], c[column]);
        }
    }

    /* Now, apply the z's to the dot products, on the skew diagonal*/
    /* Also, set up the x-values, making these "points"		*/
    std::array<Vec2d, W_DEGREE + 1> w;
    for (int i = 0; i <= W_DEGREE; i++) {
        w[i].y = 0.0;
        w[i].x = static_cast<double>(i) / W_DEGREE;
    }

    constexpr int n = DEGREE;
    constexpr int m = DEGREE - 1;
    constexpr double z[DEGREE][DEGREE + 1] = {
        /* Precomputed "z" for cubics	*/
        {1.0, 0.6, 0.3, 0.1},
        {0.4, 0.6, 0.6, 0.4},
        {0.1, 0.3, 0.6, 1.0},
    };
    for (int k = 0; k <= n + m; k++) {
        int lb = std::max(0, k - m);
        int ub = std::min(k, n);
        for (int i = lb; i <= ub; i++) {
            int j = k - i;
            w[i + j].y += cdTable[j][i] * z[j][i];
        }
    }

    return w;
}

template<typename Vec2d>
inline std::array<typename Vec2d::value_type, 5 + 1> ConvertToBezierForm2(
    const Vec2d& P,                    /* The point to find t for	*/
    const std::array<Vec2d, 3 + 1>& V) /* The control points		*/
{
    constexpr int DEGREE = 3, W_DEGREE = 5;

    /* Determine the c's -- these are vectors created by subtracting*/
    /* point P from each of the control points				*/
    Vec2d c[DEGREE + 1]; /* V(i)'s - P			*/
    for (int i = 0; i <= DEGREE; i++) {
        c[i] = V[i] - P;
    }

    /* Determine the d's -- these are vectors created by subtracting*/
    /* each control point from the next					*/
    Vec2d d[DEGREE]; /* V(i+1) - V(i)		*/
    for (int i = 0; i <= DEGREE - 1; i++) {
        d[i] = (V[i + 1] - V[i]) * 3.0;
    }

    /* Create the c,d table -- this is a table of dot products of the */
    /* c's and d's							*/
    double cdTable[DEGREE][DEGREE + 1]; /* Dot product of c, d		*/
    for (int row = 0; row <= DEGREE - 1; row++) {
        for (int column = 0; column <= DEGREE; column++) {
            cdTable[row][column] = dot(d[row], c[column]);
        }
    }

    /* Now, apply the z's to the dot products, on the skew diagonal*/
    /* Also, set up the x-values, making these "points"		*/
    //std::array<Vec2d, W_DEGREE + 1> w;
    std::array<typename Vec2d::value_type, W_DEGREE + 1> w{};
    // for (int i = 0; i <= W_DEGREE; i++) {
    //     w[i].y = 0.0;
    //     w[i].x = static_cast<double>(i) / W_DEGREE;
    // }

    constexpr int n = DEGREE;
    constexpr int m = DEGREE - 1;
    constexpr double z[DEGREE][DEGREE + 1] = {
        /* Precomputed "z" for cubics	*/
        {1.0, 0.6, 0.3, 0.1},
        {0.4, 0.6, 0.6, 0.4},
        {0.1, 0.3, 0.6, 1.0},
    };
    for (int k = 0; k <= n + m; k++) {
        int lb = std::max(0, k - m);
        int ub = std::min(k, n);
        for (int i = lb; i <= ub; i++) {
            int j = k - i;
            //w[i + j].y += cdTable[j][i] * z[j][i];
	    w[i + j] += cdTable[j][i] * z[j][i];
        }
    }

    return w;
}
