// -*- c++ -*-
#pragma once

#include "Array2.h"
#include "DistanceTags.h"
#include "DebugAssert.h"


namespace distance {
// ------------------------------------------------------------
// ------------------------------------------------------------
class FastSweeping2 {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx1_, dx2_, dx12_;
    enum class SweepDirection
    {
        Dim1Plus_Dim2Plus,
        Dim1Minus_Dim2Plus,
        Dim1Minus_Dim2Minus,
        Dim1Plus_Dim2Minus
    };

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int dim1, int dim2, double dx1,
        double dx2)
    {
      FastSweeping2 fig(begin, end, dim1, dim2, dx1, dx2);
        fig.run();
        return std::make_tuple(fig.distanceGrid_, fig.stateGrid_,
                               fig.updateGrid_);
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    FastSweeping2(ForwardIter begin, ForwardIter end, int dim1, int dim2, double dx1,
                  double dx2)
        : distanceGrid_(dim1, dim2),
          stateGrid_(dim1, dim2),
          updateGrid_(dim1, dim2),
          dx1_(dx1),
          dx2_(dx2),
          dx12_(std::sqrt(dx1 * dx1 + dx2 * dx2))
    {
        // initialize distance and states to "Far"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // initialize narrow band
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                if (std::abs(std::get<1>(p)) <
                    std::abs(d(std::get<0>(p))))  // the smallest initializer
                                                  // no matter sign should
                                                  // "win"
                    touch(std::get<0>(p), std::get<1>(p), State::Initial);
            });
    }

    // ------------------------------------------------------------
    void run()
    {
        const int dim1 = distanceGrid_.dim1();
        const int dim2 = distanceGrid_.dim2();
        //	void visit(int x, int y, double dij, double djk, double dik,
        //double dj, double
        // dk)

        // Sweep.
        // 1st sweep. x+y+
        for (int i = 1, endi = dim1; i < endi; i++)
            for (int j = 1, endj = dim2; j < endj; j++)
                if (stateGrid_(i, j) != State::Initial)
                    visit(i, j, dx2_, dx12_, dx1_, d(i, j - 1), d(i - 1, j));

        // 2nd sweep. x-y+
        for (int i = dim1 - 2; i >= 0; i--)
            for (int j = 1, endy = dim2; j < endy; j++)
                if (stateGrid_(i, j) != State::Initial)
                    visit(i, j, dx1_, dx12_, dx2_, d(i + 1, j), d(i, j - 1));

        // 3rd sweep. x-y-
        for (int i = dim1 - 2; i >= 0; i--)
            for (int j = dim2 - 2; j >= 0; j--)
                if (stateGrid_(i, j) != State::Initial)
                    visit(i, j, dx2_, dx12_, dx1_, d(i, j + 1), d(i + 1, j));

        // 4th sweep. x+y-
        for (int i = 1, endi = dim1; i < endi; i++)
            for (int j = dim2 - 2; j >= 0; j--)
                if (stateGrid_(i, j) != State::Initial)
                    visit(i, j, dx1_, dx12_, dx2_, d(i - 1, j), d(i, j + 1));
    }

  protected:
    // ------------------------------------------------------------
    double d(int x, int y) const { return distanceGrid_(x, y); }
    double d(const Index2& p) const { return distanceGrid_(p); }
    // ------------------------------------------------------------
    void visit(int x, int y, double dij, double djk, double dik, double dj,
               double dk)
    {
        double oldval = d(x, y);
        double newval =
            compute_interpolate(dij, djk, dik, std::abs(dj), std::abs(dk));
        double sign =
            std::abs(dj) < std::abs(dk) ? dj < 0 ? -1 : 1 : dk < 0 ? -1 : 1;
        if (newval < std::abs(oldval)) touch(Index2{x, y}, sign * newval);
    }
    // ------------------------------------------------------------
    double compute_interpolate(double dij, double djk, double dik, double dj,
                               double dk)
    {
        ASSERT(dij > 0 && djk > 0 && dik > 0 && dj >= 0 && dk >= 0
	       ,       "invariants");
        double nx = (dk - dj) / djk;
        double ny = -std::sqrt(std::max(0.0, 1 - nx * nx));

        double vix = (djk * djk + dij * dij - dik * dik) / (2 * djk);
        double viy = -std::sqrt(dij * dij - vix * vix);

        // di = {nx, ny}.{Vix, Viy} + Dj
        double di = nx * vix + ny * viy + dj;

        // {-ny,nx} . vj/vk/vi
        const double tj = 0;          //-ny*0 + nx*0;
        const double tk = -ny * djk;  // y-component of Vk is zero
        const double ti = -ny * vix + nx * viy;
        if (tj <= ti and ti <= tk) {
            return di;
        }

        // Dijkstra version
        return std::min(dij + dj, dik + dk);
    }
    // ------------------------------------------------------------
    void touch(const Index2& index, double val, State state = State::Active)
    {
        distanceGrid_(index) = val;
        stateGrid_(index) = state;
    }
};

}  // namespace distance

