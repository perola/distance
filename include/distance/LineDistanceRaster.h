// -*- c++ -*-
#ifndef linedistanceraster_h
#define linedistanceraster_h

#include <algorithm> // for min/max
#include <math.h> // for round, floor, ceil
#include <assert.h>
#include <iostream>
#include "aabb.h"
#include "Array2.h"
#include "Bresenham.h"
#include "Vec2d.h"

namespace distance {

  void bresenhamFillRaster(const Vec2d &p1, const Vec2d &p2, Array2<double> &pixels)
  {
    int x0 = round(p1.x);
    int y0 = round(p1.y);
    int x1 = round(p2.x);
    int y1 = round(p2.y);
    
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1; 
    int err = dx+dy, e2; /* error value e_xy */
    
    for(;;){  /* loop */
      pixels(x0, y0) = 1;
      if (x0==x1 && y0==y1) break;
      e2 = 2*err;
      if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
      if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
    }
  }

  void wideBresenhamFillRaster(int x0, int y0, int x1, int y1, float wd, distance::Array2<double> &pixels)
  { 
    int dx = abs(x1-x0), sx = x0 < x1 ? 1 : -1; 
    int dy = abs(y1-y0), sy = y0 < y1 ? 1 : -1; 
    int err = dx-dy, e2, x2, y2;                          /* error value e_xy */
    float ed = dx+dy == 0 ? 1 : sqrt((float)dx*dx+(float)dy*dy);
    
    //    mexPrintf("Vars: dx %d sx %d dy %d sy %d\nerr %f ed %f.\n", dx, sx, dy, sy, err, ed);
    for (wd = (wd+1)/2; ; ) {                                   /* pixel loop */
      //setPixelColor(x0,y0,max(0,255*(abs(err-dx+dy)/ed-wd+1)));
      pixels(x0, y0) = 1; //std::max(0.f,255*(abs(err-dx+dy)/ed-wd+1));
      e2 = err; x2 = x0;
      if (2*e2 >= -dx) {                                           /* x step */
	for (e2 += dy, y2 = y0; e2 < ed*wd && (y1 != y2 || dx > dy); e2 += dx)
	  {
	    //setPixelColor(x0, y2 += sy, max(0,255*(abs(e2)/ed-wd+1)));
	    pixels(x0, y2 += sy) = 2; //std::max(0.f,255*(abs(e2)/ed-wd+1));
	  }
	if (x0 == x1) break;
	e2 = err; err -= dy; x0 += sx; 
      } 
      if (2*e2 <= dy) {                                            /* y step */
	for (e2 = dx-e2; e2 < ed*wd && (x1 != x2 || dx < dy); e2 += dy)
	  {
	    //setPixelColor(x2 += sx, y0, max(0,255*(abs(e2)/ed-wd+1)));
	    pixels(x2 += sx, y0) = 3; //std::max(0.f,255*(abs(e2)/ed-wd+1));
	  }
	if (y0 == y1) break;
	err += dx; y0 += sy; 
      }
    }
  }
  
  double signedDistLineSegmentToPoint(const Vec2d v, const Vec2d  w, const Vec2d p){
    Vec2d dx = w-v;
    const double l2 = dx.length2();  // i.e. |w-v|^2 -  avoid a sqrt
    //if (l2 == 0.0) return distance(p, v);   // v == w case
    
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line. 
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    const double t = dot(p - v, w - v) / l2;
    //const double t = (px-vx)*dx + (py-vy)*dy / l2;
    
    double sign = cross(dx, Vec2d(p-v)) > 0 ? 1.0 : -1.0;
    if (t < 0.0) 
      return sign*(p-v).length(); //distance(p, v);       // Beyond the 'v' end of the segment
    else if (t > 1.0)
      return sign*(p-w).length(); //distance(p, w);  // Beyond the 'w' end of the segment
    //const vec2 projection = v + t * (w - v);  // Projection falls on the segment
    //const double prx = (1-t)*vx + t*wx;
    //const double pry = (1-t)*vy + t*wy;
    Vec2d pr = (1-t)*v + t*w;
  
    return sign*(p-pr).length(); //sqrt( (px-prx)*(px-prx) + (py-pry)*(py-pry));
  }

  void bresenhamFillRaster2(const Vec2d &x1, const Vec2d &x2, double w, Array2<double> &pixels) {
    Vec2d normal = (x2-x1).normalized().orthogonal(); // y,-x
    aabb bound(x1, x2);
    bound.grow(x1 + w*normal);
    bound.grow(x1 - w*normal);
    bound.grow(x2 + w*normal);
    bound.grow(x2 - w*normal); 
    
    aabb ibound = aabb(Vec2d(floor(bound.pMin.x), floor(bound.pMin.y)),
		       Vec2d(ceil(bound.pMax.x), ceil(bound.pMax.y))).intersection(aabb(Vec2d(0,0),Vec2d(pixels.dim1(), pixels.dim2())));
    if(!ibound.isValid())
      return;

    RectPixelVisitor qp(x1 + w*normal, x1 - w*normal, x2 - w*normal, x2 + w*normal); // oriented quad, counter clockwise
    while(qp.hasNext()) {
      std::pair<int, int> pos = qp.next();
      double d = signedDistLineSegmentToPoint(x1, x2, Vec2d(pos.first, pos.second));
      double oldd = pixels(pos.first,pos.second);
      if(fabs(d) + 1.0e-6 < fabs(oldd))
	pixels(pos.first, pos.second) = d;
    }
  }

  void wideBBoxDistanceRaster(const Vec2d & x1, const Vec2d& x2, double w, Array2<double> &pixels){
    Vec2d normal = (x2-x1).normalized().orthogonal(); // y,-x
    aabb bound(x1, x2);
    bound.grow(x1 + w*normal);
    bound.grow(x1 - w*normal);
    bound.grow(x2 + w*normal);
    bound.grow(x2 - w*normal);
    
    aabb ibound = aabb(Vec2d(floor(bound.pMin.x), floor(bound.pMin.y)),
		       Vec2d(ceil(bound.pMax.x), ceil(bound.pMax.y))).intersection(aabb(Vec2d(0,0),Vec2d(pixels.dim1(), pixels.dim2())));
    if(!ibound.isValid())
      return;
    
    for(int x = ibound.pMin.x; x<ibound.pMax.x; x++)
      for(int y = ibound.pMin.y; y<ibound.pMax.y; y++) {
	  if(!pixels.bound(Index2(x,y)))
	    continue;
	  //double d = distRayToPoint(x1, y1, x2, y2, x, y);
	  //double d = distLineSegmentToPoint(x1, y1, x2, y2, x, y);
	  double d = signedDistLineSegmentToPoint(x1, x2, Vec2d(x, y));
	  double oldd = pixels(x,y);
	  if(fabs(d) + 1.0e-6 < fabs(oldd))
	    pixels(x, y) = d;
	}
  }


  void wideBBoxSegmentedDistanceRaster(const Vec2d &x1, const Vec2d &x2, double w, Array2<double> &pixels){
    if(! aabb(Vec2d(0,0), Vec2d(pixels.dim1(), pixels.dim2())).intersection(aabb(x1, x2)).isValid())
      return;
    
    std::pair<double, double> tinout = aabb(Vec2d(0,0), Vec2d(pixels.dim1(), pixels.dim2())).intersection(x1, x2);

    if (tinout.second < tinout.first || tinout.second < 0.0) // no intersection, or "behind"
      return;
    
    Vec2d dir = (x2-x1); 
    Vec2d X1 = tinout.first > 0 ? x1 + tinout.first*dir : x1;
    Vec2d X2 = tinout.second < 1 ? x1 + tinout.second*dir : x2;
    
    double dist2 = (X2-X1).length2();
    int steps = dist2 / (w*w*3);
    for(int i=0; i==0 || i<steps; i++){
      double t1 = double(i) / steps;
      double t2 = double(i+1) / steps;
      Vec2d xx1 = (1-t1)*X1 + t1*X2;
      Vec2d xx2 = (1-t2)*X1 + t2*X2;
      wideBBoxDistanceRaster(xx1, xx2, w, pixels);
    }
  }

  void wedgeDistanceRaster(const Vec2d &p, const Vec2d &n1, const Vec2d &n2,
			   double w, Array2<double> &pixels){

    assert(w>0);
    double sign = -1.0;
    // find out on what side the wedge is located
    if(cross(n1, n2) < 0){
      w *= -1;
      sign = 1.0;
    }
    
    aabb bound(p, p + w*n1);
    bound.grow(p + w*n2);
    //  bound.grow(p + (.5*n1 + .5*n2));
    
    aabb ibound = aabb(Vec2d(floor(bound.pMin.x), floor(bound.pMin.y)),
		       Vec2d(ceil(bound.pMax.x), ceil(bound.pMax.y))).intersection(aabb(Vec2d(0,0),Vec2d(pixels.dim1(), pixels.dim2())));
    std::cout << ibound << std::endl;
    if(!ibound.isValid())
      return;
    for(int x = ibound.pMin.x; x<ibound.pMax.x; x++)
      for(int y = ibound.pMin.y; y<ibound.pMax.y; y++)
	{
	  double d = sign*(Vec2d(x,y) - p).length();
	  if(pixels.bound(Index2(x,y)) && fabs(d) < fabs(pixels(x,y)))
	    pixels(x,y) = d;
	}
  }

  void rasterizePoint(const Vec2d &p, double w, Array2<double> &pixels) {
    assert(w>0);
    aabb bound = aabb(p-Vec2d(w, w), p+Vec2d(w,w)).intersection(aabb(Vec2d(0,0), Vec2d(pixels.dim1(), pixels.dim2())));
    if(!bound.isValid())
      return;

    for(int x = bound.pMin.x; x<bound.pMax.x; x++)
      for(int y = bound.pMin.y; y<bound.pMax.y; y++)
	{
	  if (!pixels.bound(Index2(x,y)))
	    continue;
	  //double d = distRayToPoint(x1, y1, x2, y2, x, y);
	  //double d = distLineSegmentToPoint(x1, y1, x2, y2, x, y);
	  double d = (p - Vec2d(x, y)).length();
	  double oldd = pixels(x,y);
	  if(fabs(d) < fabs(oldd))
	    pixels(x, y) = d;
	}
  }


  
} // namespace distance

#endif
