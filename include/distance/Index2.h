// -*- c++ -*-
#pragma once

#include <utility>  // for pair
#include <tuple>
#if DISTANCE_HAS_IOSTREAM
#include <iostream>
#endif

#pragma push_macro("TIE")
#undef TIE
#define TIE(X) std::tie((X).i, (X).j)

namespace distance {

struct Index2
{
    int i, j;
    Index2() = default;
    Index2(const Index2&) = default;
    Index2(Index2&&) = default;
    Index2& operator=(const Index2&) = default;
    explicit Index2(const std::pair<int, int> &ij) : i(ij.first), j(ij.second) {}
    Index2(int i, int j) : i(i), j(j) {}
    Index2 s() const { return Index2(i - 1, j); }
    Index2 n() const { return Index2(i + 1, j); }
    Index2 w() const { return Index2(i, j - 1); }
    Index2 e() const { return Index2(i, j + 1); }
    Index2 se() const { return Index2(i - 1, j + 1); }
    Index2 sw() const { return Index2(i - 1, j - 1); }
    Index2 ne() const { return Index2(i + 1, j + 1); }
    Index2 nw() const { return Index2(i + 1, j - 1); }
    friend bool operator<(const Index2 &lhs, const Index2 &rhs) { return TIE(lhs)<TIE(rhs); }
    friend bool operator==(const Index2 &lhs, const Index2 &rhs) { return TIE(lhs)==TIE(rhs); }
};
#pragma pop_macro("TIE")

#if DISTANCE_HAS_IOSTREAM
inline std::ostream& operator<<(std::ostream& os, const Index2 s)
{
    return os << "Index2( " << s.i << ", " << s.j << ")";
}
#endif
}  // namespace distance
