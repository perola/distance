// -*- c++ -*-
#pragma once

#include <algorithm>  // for max, min
#include <cassert>    // for assert
#include <cmath>      // for floor
#include <utility>    // for pair, make_pair, operator!=, operator==
#include "Vec2d.h"    // for Vec2d, operator<<

#if DISTANCE_HAS_IOSTREAM
#include <iostream>   // for operator<<, basic_ostream::operator<<, ostream
#endif

namespace distance {

namespace inner {
inline int iabs(const int x) { return x >= 0 ? x : -x; }
}  // namespace inner

/* A pixel visitor is created from a geometric description
   of an area to visit. Typically a line, ie PixelVisitor(pStart, pEnd)

   It starts in a pre-accessible state. To access data:
   while(it.hasNext()) doSomething(it.next());

   An "empty" (default constructed) pixel visitor reports hasNext=false

*/
class PixelVisitor {
  public:
    // Advance the iterator and get the next pixel
    virtual std::pair<int, int> next() = 0;

    // Check if advancement can be done
    virtual bool hasNext() const = 0;
};

class BresenhamVisitor : PixelVisitor {
    std::pair<int, int> cur;  // avoid creating pairs all the time
    int x1, y1;
    int dx, dy;
    int sx, sy;
    int err;
    void step()
    {
        int err2 = 2 * err;
        if (err2 >= dy) {
            err += dy;
            cur.first += sx;
        }
        if (err2 <= dx) {
            err += dx;
            cur.second += sy;
        }
    }

  public:
    BresenhamVisitor() : cur(std::make_pair(0, 0)), x1(0), y1(0)
    {
        assert(!BresenhamVisitor::hasNext() &&
               !BresenhamVisitor::hasNextLine());
    }
    BresenhamVisitor(double x0, double y0, double x1, double y1)
        : BresenhamVisitor(static_cast<int>(std::floor(x0)),
                           static_cast<int>(std::floor(y0)),
                           static_cast<int>(std::floor(x1)),
                           static_cast<int>(std::floor(y1)))
    {
    }
    BresenhamVisitor(int x0, int y0, int x1, int y1)
        : cur(std::make_pair(x0, y0)),
          x1(x1),
          y1(y1),
          dx(inner::iabs(x1 - x0)),
          dy(-inner::iabs(y1 - y0)),
          sx(x0 < x1 ? 1 : -1),
          sy(y0 < y1 ? 1 : -1),
          err(dx + dy)
    {
        // no boundary handling at INT_MIN/MAX

        // backwards step, works also for singular input
        int err2 = 2 * err;
        if (err2 >= dy) {
            err -= dy;
            cur.first -= sx;
        }
        if (err2 <= dx) {
            err -= dx;
            cur.second -= sy;
        }

        // now a forward step should put us back up on p0
        assert(BresenhamVisitor::hasNext());
    }
    bool hasNext() const override
    {
        return !(cur.first == x1 && cur.second == y1);
    }
    bool hasNextLine() const { return cur.second != y1; }
    std::pair<int, int> curr() const
    {
        // assert(sx*cur.first >= sx*x0 && sx*cur.first <= sx*x1);
        return cur;
    }
    std::pair<int, int> next() override
    {
        step();
        return curr();
    }
    std::pair<int, int> nextLine()
    {
        assert(hasNextLine() &&
               "invalid to call nextLine on a BresenhamVisitor "
               "for which hasNextLine is false");
        // advance until we reach a new y
        int y = cur.second;
        while (cur.second == y)  // infinite loop for horizontal lines
            step();
        return curr();
    }
    // the last point on the current line (in a forward direction)
    std::pair<int, int> endOfLine()
    {
        // step as far as we can on this line without going onto next
        while (true) {
            int err2 = 2 * err;
            if (err2 <= dx) break;
            // break before reaching the next line
            if (err2 >= dy) {
                err += dy;
                cur.first += sx;
            }
        }
        return curr();
    }
    // the gradient at the current pos, such that curr+grad = next
    std::pair<int, int> grad() const
    {
        int err2 = 2 * err;
        return std::make_pair(err2 >= dy ? sx : 0, err2 <= dx ? sy : 0);
    }
    // the quadrant
    std::pair<int, int> quadrant() const { return std::make_pair(sx, sy); }
#if DISTANCE_HAS_IOSTREAM
  friend std::ostream& operator<<(std::ostream& os, const BresenhamVisitor& l)
    {
        return (os << l.curr() << "-" << std::make_pair(l.x1, l.y1) << " @"
                   << l.curr() << (l.hasNext() ? "+ g" : " g") << l.grad());
    }
#endif
};

class ScanLineVisitor {
    int x0, x1;
    std::pair<int, int> cur;

  public:
    ScanLineVisitor() : x1(0), cur(std::make_pair(0, 0)) { assert(!hasNext()); }
    ScanLineVisitor(int x0, int x1, int y)
        : x0(x0), x1(x1), cur(std::make_pair(x0 - 1, y))
    {
        assert(x0 <= x1);
        assert(hasNext());
    }
    bool hasNext() const { return cur.first != x1; }
    std::pair<int, int> curr() const { return cur; }
    std::pair<int, int> next()
    {
        ++cur.first;
        return curr();
    }
    int line() const { return cur.second; }
#if DISTANCE_HAS_IOSTREAM
  friend std::ostream& operator<<(std::ostream& os, const ScanLineVisitor& l)
    {
        return (os << l.line() << ": " << l.x0 << "-" << l.x1 << " @"
                   << l.cur.first << (l.hasNext() ? '+' : ' '));
    }
#endif
};

/** A visitor for triangles with flat top or base
 *
 *  Such a triangle can be confined inside two BresenhamVisitors
 *  only
 *  Usage:
 *         ftv = FlatTriVisitor(p0, p1, p2);
 *         while(ftv.hasNext()) {
 *             <x,y> = ftv.next();
 *             workIt(x,y);
 *         }
 */
class FlatTriVisitor {
    BresenhamVisitor left;
    BresenhamVisitor right;
    ScanLineVisitor h;

  public:
    // the pointy vert must be p0, and p1.y == p2.y
    FlatTriVisitor(const Vec2d& p0, const Vec2d& p1, const Vec2d& p2)
        : left(p0.x, p0.y, std::min(p1.x, p2.x), p1.y),
          right(p0.x, p0.y, std::max(p1.x, p2.x), p2.y)
    {
        assert(p1.y == p2.y);  // the flat side is p1-p2
        h = ScanLineVisitor(left.curr().first, right.curr().first, p0.y);
    }
    bool hasNext() const
    {
        assert(left.hasNextLine() == right.hasNextLine());
        return h.hasNext() || (left.hasNext() && right.hasNext());
    }
    std::pair<int, int> next()
    {
        if (!h.hasNext()) {
            left.nextLine();
            if (left.quadrant().first == -1)  // left slope
                left.endOfLine();             // advance to left edge
            right.nextLine();
            if (right.quadrant().first == 1)  // right slope
                right.endOfLine();            // advance to right edge
            h = ScanLineVisitor(left.curr().first, right.curr().first,
                                right.curr().second);
        }
        assert(left.curr().second == right.curr().second);  // same line
        return h.curr();
    }
};

class RectPixelVisitor {
    ScanLineVisitor h;
    BresenhamVisitor left[2];
    BresenhamVisitor right[2];

  public:
    RectPixelVisitor(const Vec2d& p0, const Vec2d& p1, const Vec2d& p2,
                     const Vec2d& p3)
    {
        // assume counter clockwise orientation
        int index = 0;
        const Vec2d arr[] = {p0, p1, p2, p3};

        double y = p0.y;
        if (y < p1.y) {
            index = 1;
            y = p1.y;
        }
        if (y < p2.y) {
            index = 2;
            y = p2.y;
        }
        if (y < p3.y) {
            index = 3;
            // y = p3.y;
        }

        // set up Visitors
        left[0] =
            BresenhamVisitor(arr[index % 4].x, arr[index % 4].y,
                             arr[(1 + index) % 4].x, arr[(1 + index) % 4].y);
        left[1] =
            BresenhamVisitor(arr[(1 + index) % 4].x, arr[(1 + index) % 4].y,
                             arr[(2 + index) % 4].x, arr[(2 + index) % 4].y);
        right[0] =
            BresenhamVisitor(arr[index % 4].x, arr[index % 4].y,
                             arr[(3 + index) % 4].x, arr[(3 + index) % 4].y);
        right[1] =
            BresenhamVisitor(arr[(3 + index) % 4].x, arr[(3 + index) % 4].y,
                             arr[(2 + index) % 4].x, arr[(2 + index) % 4].y);

        int y0 = arr[index % 4].y;           // current scanline
        left[0].next();                      // just start
        if (left[0].quadrant().first == -1)  // left slope
            left[0].endOfLine();             // advance to left edge
        int x0 = left[0].curr().first;

        right[0].next();                     // just start
        if (right[0].quadrant().first == 1)  // right slope
            right[0].endOfLine();            // advance to right edge
        int x1 = right[0].curr().first;

        h = ScanLineVisitor(x0, x1, y0);
    }
    bool hasNext() const
    {
        return h.hasNext() || ((left[0].hasNext() || left[1].hasNext()) &&
                               (right[0].hasNext() || right[1].hasNext()));
    }
    std::pair<int, int> curr() const { return h.curr(); }
    std::pair<int, int> next()
    {
        // first continue with current scanline
        if (h.hasNext()) {
            return h.next();
        }

        // find the bounds of the next scanline
        std::pair<int, int> X0 = nextLineLeft();
        std::pair<int, int> X1 = nextLineRight();

        assert(X0.first <= X1.first);              // left before right
        assert(X0.second == X1.second);            // same y
        assert(X1.second == h.curr().second - 1);  // next line

        // reassign scanline
        h = ScanLineVisitor(X0.first, X1.first, X1.second);  // x x y
        return h.curr();
    }

    std::pair<int, int> nextLineLeft()
    {
        assert(h.curr().second == left[0].curr().second ||
               h.curr().second ==
                   left[1].curr().second);  // one of the lefts should
                                            // be at the same pos as h
        assert(left[0].hasNext() || left[1].hasNext());

        std::pair<int, int> X0;
        while (left[0].curr().second == h.curr().second && left[0].hasNext()) {
            X0 = left[0].next();
        }
        // can we move more left?
        while (left[0].hasNext() && left[0].grad() == std::make_pair(-1, 0)) {
            X0 = left[0].next();
        }

        while (left[1].curr().second == h.curr().second && left[1].hasNext()) {
            X0 = left[1].next();
        }
        // can we move more left? (on the current line)
        while (left[1].curr().second == X0.second && left[1].hasNext() &&
               left[1].grad() == std::make_pair(-1, 0)) {
            X0 = left[1].next();
        }

        assert(X0.second == h.curr().second -
                   1);  // no matter what we should have the next y
        if (left[0].hasNext()) assert(left[0].grad() != std::make_pair(-1, 0));
        if (!left[0].hasNext() && left[1].hasNext())
            assert(left[1].grad() != std::make_pair(-1, 0));
        return X0;
    }

    std::pair<int, int> nextLineRight()
    {
        assert(h.curr().second == right[0].curr().second ||
               h.curr().second == right[1].curr().second);  // one of the rights
        // should be at the same
        // pos as h
        assert(right[0].hasNext() || right[1].hasNext());

        std::pair<int, int> X1;
        while (right[0].curr().second == h.curr().second &&
               right[0].hasNext()) {
            X1 = right[0].next();
        }
        // can we move more right?
        while (right[0].hasNext() && right[0].grad() == std::make_pair(1, 0)) {
            X1 = right[0].next();
        }

        while (right[1].curr().second == h.curr().second &&
               right[1].hasNext()) {
            X1 = right[1].next();
        }
        // can we move more right? (on the current line)
        while (right[1].curr().second == X1.second && right[1].hasNext() &&
               right[1].grad() == std::make_pair(1, 0)) {
            X1 = right[1].next();
        }

        assert(X1.second == h.curr().second -
                   1);  // no matter what we should have the next y
        if (right[0].hasNext()) assert(right[0].grad() != std::make_pair(1, 0));
        if (!right[0].hasNext() && right[1].hasNext())
            assert(right[1].grad() != std::make_pair(1, 0));

        return X1;
    }
};

}  // namespace distance
