// -*- c++ -*-
#pragma once

#include <cstddef>

namespace distance {

/** An integer partial factorial. Computes N!/P! for N,P >= 0
 * Nb empty product is 1.
 * Nb integer division is performed, eg N!/P!=0 for P>N
 *
 * Eg: PartialFactorial<3,0> == 3*2*1/1 = 6
 * Eg: PartialFactorial<3,2> == 3*2*1/2*1 = 3
 * Eg: PartialFactorial<3,4> == 3*2*1/4*3*2*1 = 0
 */
template <size_t N, size_t P>
struct PartialFactorial;

namespace inner {

template <int N, int C>
struct PartialFactorialImpl
{
    static_assert(N >= C && N > 0 && C > 0,
                  "PartialFactorial must be called with positive input, where  "
                  "number of products is smaller than start value.");
    static constexpr int value()
    {
        return N * PartialFactorialImpl<N - 1, C - 1>::value();
    }
};

// terminating case
template <int N>
struct PartialFactorialImpl<N, 0>
{
    static constexpr int value() { return 1; }
};

// terminating case for <0,0>
template <>
struct PartialFactorialImpl<0, 0>
{
    static constexpr int value() { return 1; }
};

// dispatcher to handle cases that would recurse endlessly
template <int N, int P, bool Inc = (N >= P), bool PosP = (P >= 0)>
struct PartialFactorialHelper
{
    static constexpr int value() { return 0; }
};

template <int N, int P>
struct PartialFactorialHelper<N, P, true, true>
{
    static constexpr int value()
    {
        return PartialFactorialImpl<N, N - P>::value();
    }
};

}  // ns inner

template <size_t N, size_t P>
struct PartialFactorial
{
    static constexpr size_t value()
    {
        return inner::PartialFactorialHelper<N, P>::value();
    };
};

}  // ns distance
