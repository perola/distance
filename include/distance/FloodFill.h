// -*- c++ -*-
#pragma once

#include <deque>
//#include <cassert>
#include <algorithm>

#include "Array2.h"
#include "Index2.h"

#ifdef MATLAB_MEX_FILE
#define printf mexPrintf
#endif

namespace distance {
template <typename tag_type, typename iter_type, typename unary_pred,
          typename unary_func>
void FloodFill(distance::Array2<tag_type>& map, iter_type pos_start,
               iter_type pos_end, unary_pred predicate,
               unary_func replacement_func)
{
    // given a set of values which may have neighbors that can be start points
    for (iter_type it = pos_start; it != pos_end; ++it) {
        Index2 current = *it;
        // check neighbors
        Index2 nbs[] = {
            current, current.n(),  // current.ne(),
            current.e(),           // current.se(),
            current.s(),           // current.sw(),
            current.w()            //,  current.nw()
        };
        for (const Index2& i : nbs) {
            if (map.bound(i) && predicate(map(i))) {
                FloodFill(map, i, predicate, replacement_func);
            }
        }
    }
}

template <typename tag_type, typename iter_type, typename start_pred,
          typename unary_pred, typename unary_func>
void FloodFill(distance::Array2<tag_type>& map, iter_type pos_start,
               iter_type pos_end, start_pred start_predicate,
               unary_pred predicate, unary_func replacement_func)
{
    // given a set of values for which some (start_predicate) may have neighbors
    // (predicate) that can be start points
    for (iter_type it = pos_start; it != pos_end; ++it) {
        Index2 current = Index2(*it);
        if (!start_predicate(map(current))) continue;

        // check neighbors
        Index2 nbs[] = {
            current, current.n(),  // current.ne(),
            current.e(),           // current.se(),
            current.s(),           // current.sw(),
            current.w()            //,  current.nw()
        };
        for (const Index2& i : nbs) {
            if (map.bound(i) && predicate(map(i))) {
                FloodFill(map, i, predicate, replacement_func);
            }
        }
    }
}

template <typename tag_type, typename unary_pred, typename unary_func>
void FloodFill(distance::Array2<tag_type>& map, Index2 start_pos,
               unary_pred predicate, unary_func replacement_func)
{
#ifdef MATLAB_MEX_FILE
    if (!predicate(map(start_pos))) {
        printf("data at (%d, %d): %f.\n", start_pos.i, start_pos.j,
                  map(start_pos));
        mexErrMsgTxt("bad start pos. data doesn't match predicate.\n");
    }
#endif
#ifndef NDEBUG
    int i = 0;
#endif
    std::deque<Index2> queue;
    queue.push_back(start_pos);
    while (!queue.empty()) {
#ifndef NDEBUG
        const int maxIter = map.dim1() * map.dim2() * 10;
        if (++i > maxIter) {
            printf("max iter reached. aborting.\n");
            break;
        }
#endif
        Index2 current = queue.back();
        queue.pop_back();

#if 1
        // scan line optimization
        {
            int lineStart = current.i;
            while(predicate(map(lineStart, current.j)) && lineStart-1 >= 0) { --lineStart; }

            int lineEnd   = current.i; // 1 past last coord (end) 
            while(predicate(map(lineEnd, current.j))
                  && ++lineEnd < static_cast<int>(map.dim1())) {}
            
            auto liFirst = map.coord2li(lineStart, current.j);
            auto liEnd = liFirst + (lineEnd - lineStart); // 1 past last
            // is transform smart enough to unroll to memset if r_func is constexpr?
            // it seems run time is dominated by queue handling at this point
            std::transform(map.begin() + liFirst, map.begin() + liEnd,
                           map.begin() + liFirst,
                           replacement_func);
        }
        
        // check east/west neighbors (north/current/south already visited)
        if (map.bound(current.e()) && predicate(map(current.e())))
            queue.push_back(current.e());
        if (map.bound(current.w()) && predicate(map(current.w())))
            queue.push_back(current.w());

#else
        // update value at current position
        map(current) = replacement_func(map(current));

        // check neighbors
        Index2 nbs[] = {
            current.n(), /*current.ne(),*/ current.e(), /* current.se(),*/
            current.s(), /*current.sw(),*/ current.w() /*, current.nw()*/};
        for (const Index2& nb : nbs) {
            if (map.bound(nb) && predicate(map(nb))) queue.push_back(nb);
        }
#endif
    }
}
}
