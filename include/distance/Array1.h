// -*- c++ -*-
#pragma once

#include <stdio.h>      // for printf, size_t
#include <cassert>      // for assert
#include <cmath>        // for floor
#include <cstddef>      // for size_t
#include <type_traits>  // for enable_if, is_floating_point
#include <vector>       // for vector
#include "clamp.h"      // for clamp
#include "lerp.h"       // for lerp

namespace distance {

template <typename T>
using Array1 = std::vector<T>;

template <typename T, typename T2>
inline std::enable_if_t<std::is_floating_point<T2>::value, T>
interp1_inner(const T* arr, T2 t, T2 oneLess)
{
    T2 tscaled = t * oneLess;
    T2 tlow = std::floor(tscaled);
    T2 thigh = tlow < oneLess ? tlow + 1 : tlow;
    T2 p = tscaled - tlow;  // local param between tlow and thigh
    return lerp(arr[static_cast<size_t>(tlow)], arr[static_cast<size_t>(thigh)],
                p);  // delayed floating point->integer conversion 
}

// interp1 with uniform parameterization
// we need the enable_if since T2 is a template parameter
template <typename T, typename T2>
inline std::enable_if_t<std::is_floating_point<T2>::value, T>
interp1(const Array1<T>& arr, T2 t)
{
    return interp1_inner(arr.data(), clamp<T2>(t, T2{0}, T2{1}),
                         static_cast<T2>(arr.size() - 1));
}

// interp1 pre-alloced, returns interpolated data so expr can be chained
template <typename T, typename Container>
inline std::enable_if_t<
    std::is_floating_point<typename Container::value_type>::value,
    Array1<T>&>
interp1(const Array1<T>& arr, Container ts, Array1<T>& result)
{
    using T2 = typename Container::value_type;
    std::size_t i = 0;
    T2 last = static_cast<T2>(arr.size() - 1);
    for (T2 t : ts) {
        result[i++] =
            interp1_inner(arr.data(), clamp<T2>(t, T2{0}, T2{1}), last);
    }

    return result;
}

// interp1 with uniform parameterization, iterable parameters
template <typename T, typename Container>
inline std::enable_if_t<
    std::is_floating_point<typename Container::value_type>::value,
    Array1<T>>
interp1(const Array1<T>& arr, Container ts)
{
    Array1<T> result(ts.size());
    return interp1(arr, ts, result);
}

// filter input Array1 with 1D filter kernel 
template<typename T>
inline Array1<T>& filter(const Array1<T>& arr, const Array1<T>& filter, Array1<T> &result)
{
    assert(result.size() == arr.size());
    int fsize = filter.size();
    int mid = fsize / 2 + (fsize % 2); // add one if filter size is odd

    // start pad
    int i; // keep i running
    for(i=0; i<mid-1; i++){
        
        T sum = 0;
        T first = arr[0];
        int j = 0;   
        for(; (i+j) < mid; j++){
            sum += first*filter[j];
        }
        for(int jend=fsize; j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
        }
        result[i] =  sum;
    }

    // mid
    for(int iend=arr.size()-mid; i<iend; i++){
        T sum = 0;
        for(int j=0, jend = fsize; j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
        }
        result[i] = sum;
    }

    // end pad
    for(int iend=arr.size(); i<iend; i++){
        T sum = 0;

        int j;
        for(j=0; j+i < iend; j++){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
        }

        T last = arr[arr.size()-1];
        for(int jend = fsize; j<jend; j++){
            sum += last*filter[j];
        }
        result[i] = sum;
    }
    return result;
}

// filter input Array1 with 1D filter kernel 
template<typename T>
inline Array1<T> filter(const Array1<T>& arr, const Array1<T>& filt)
{
    Array1<T> result(arr.size());
    return filter(arr, filt, result);
}

// average filter input Array1  
template<typename T>
inline Array1<T>& aafilter(const Array1<T>& arr, int width, Array1<T> &result)
{
    assert(result.size() == arr.size());
    int fsize = width;
    int mid = fsize / 2 + (fsize % 2); // add one if filter size is odd

    // start pad
    int i=0; // keep i running
    for(; i<mid-1; i++){
        T sum = 0;
        T first = arr[0];
        int j;        
        for(j=0; (i+j) < mid; j++){
            sum += first;
        }
        for(int jend=fsize; j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii];
        }
        result[i] =  sum / width;
    }

    // mid
    for(int iend=arr.size()-mid; i<iend; i++){
        T sum = 0;
        for(int j=0, jend = width; j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii];
        }
        result[i] = sum/width;
    }

    // end pad
    for(int iend=arr.size(); i<iend; i++){
        T sum = 0;
        int j;
        for(j=0; j+i < iend; j++){
            int ii = i-mid+j+1;
            sum += arr[ii];
        }

        T last = arr[arr.size()-1];
        for(int jend = width; j<jend; j++){
            sum += last;
        }
        result[i] = sum/width;
    }
    return result;
}

// filter input Array1 with 1D filter kernel 
template<typename T>
inline Array1<T> aafilter(const Array1<T>& arr, int width)
{
    Array1<T> result(arr.size());
    return aafilter(arr, width, result);
}

// filter input Array1 with 1D filter kernel 
template<typename T>
inline Array1<T> filterreduce(const Array1<T>& arr, const Array1<T>& filter)
{
    int fsize = filter.size();
    Array1<T> result(arr.size() / filter.size() + (fsize % 2));
    int mid = fsize / 2 + (fsize % 2); // add one if filter size is odd
    
    // start pad
    int i; // keep i running
    int out = 0;
    for(i=0; i<mid-1; i+=fsize){
        T sum = 0;
        T first = arr[0];
        int j;        
        for(j=0; (i+j) < mid; j++){
            sum += first*filter[j];
        }
        for(int jend=filter.size(); j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
        }
        result[out++] =  sum;
    }

    // mid
    for(int iend=arr.size()-mid; i<iend; i+=fsize){
        T sum = 0;
        for(int j=0, jend = filter.size(); j<jend; j++ ){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
            printf("m: ii:%d j:%d\n", ii, j);
        }
        result[out++] = sum;
    }

    // end pad
    for(int iend=arr.size(); i<iend; i+=fsize){
        T sum = 0;

        int j;
        for(j=0; j+i-1 < iend; j++){
            int ii = i-mid+j+1;
            sum += arr[ii]*filter[j];
        }

        T last = arr[arr.size()-1];
        for(int jend = filter.size(); j<jend; j++){
            sum += last*filter[j];
        }
        result[out++] = sum;
    }
    return result;
}

}  // namespace distance
