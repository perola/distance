// -*- c++ -*-
#pragma once

#include <algorithm>      // for min
#include <deque>          // for deque
#include <tuple>          // for tie
#include <utility>        // for pair
#include "CubicBezier.h"  // for CubicBezier
#include "aabb.h"         // for aabb

namespace distance {

template <typename Vec2, typename Scalar>
bool intersects(const CubicBezier<Vec2>& b1, const CubicBezier<Vec2>& b2,
                Scalar eps)
{
    using Bezier = CubicBezier<Vec2>;
    std::deque<std::pair<Bezier, Bezier>> bezierPairs = {{b1, b2}};

    int iter = 0, maxIter = 1e5;
    while (bezierPairs.size()) {
	assert(++iter < maxIter);

        Bezier a, b;
        std::tie(a, b) = bezierPairs.back();
        bezierPairs.pop_back();

        aabb aBox(a.c0());
        aBox.grow(a.c1()).grow(a.c2()).grow(a.c3());

        aabb bBox(b.c0());
        bBox.grow(b.c1()).grow(b.c2()).grow(b.c3());

        if (aBox.intersection(bBox).isEmpty())
            continue;  // no intersection here go to next

        // check convergence
        if (std::min(aBox.width(), aBox.height()) < eps ||
            std::min(bBox.width(), bBox.height()) < eps) {
            return true;
        }

        Bezier a1, a2;
        a.split(.5, &a1, &a2);

        Bezier b1, b2;
        b.split(.5, &b1, &b2);

        bezierPairs.push_back({a1, b1});
        bezierPairs.push_back({a2, b2});
        bezierPairs.push_back({a1, b2});
        bezierPairs.push_back({a2, b1});
    }
    return false;
}
}  // namespace distance
