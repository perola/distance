// -*- c++ -*-
#pragma once


#include <assert.h>   // for assert
#include <algorithm>  // for lower_bound
#include <array>      // for array
#include <iterator>   // for distance
#include <vector>     // for vector

namespace distance {

/**
 * Computes Spline values using DeBoer's algorithm, see eg 'Curves
 * and Surfaces for CAGD', G. Farin, pp 165-168.
 *
 * Implemented with recursion on auxiliary template parameter K
 */
template <typename T, int N, int K = N>
struct DeBoerEvaluator
{
    using value_type = T;
    static value_type eval(const value_type* data, const double* knots,
                           double t)
    {
        // we just assume that we've been given the right knot interval and
        // associated coefficients
        // ie that data points to N+1 values, and knots to N+K non-decreasing
        // values
        constexpr int i = K;
        return (knots[i + N - K] - t) / (knots[i + N - K] - knots[i - 1]) *
                   DeBoerEvaluator<value_type, N, K - 1>::eval(data, knots, t) +
               (t - knots[i - 1]) / (knots[i + N - K] - knots[i - 1]) *
                   DeBoerEvaluator<value_type, N, K - 1>::eval(data + 1,
                                                               knots + 1, t);
    }
};

/** The terminating case for recursive DeBoer evaluations */
template <typename T, int N>
struct DeBoerEvaluator<T, N, 0>
{
    using value_type = T;
    static value_type eval(const value_type* data, const double*, double)
    {
        return data[0];
    }
};

template <typename C, int N, typename R = double>
std::array<C, N> insert_impl(const C* coeffs, const R* knots, R new_knot)
{
    // assume correct interval, that is N+1 coefficients (4 for degree 3) and
    // N+2
    // (6) non-decreasing knots
    std::array<C, N> new_coeffs;
    for (int i = 0; i < N; i++) {
        C c =
            (knots[i + N] - new_knot) / (knots[i + N] - knots[i]) * coeffs[i] +
            (new_knot - knots[i]) / (knots[i + N] - knots[i]) * coeffs[i + 1];
        new_coeffs[i] = c;
    }
    return new_coeffs;
}

/**
 * Inserts a knot into a spline knot vector and compute the
 * correspoing new coeffients, see eg 'Curves and Surfaces for CAGD',
 * G. Farin, pp 159-165.
 */
template <typename C, int N, typename R = double>
void insert(std::vector<C>& coeffs, std::vector<R>& knots, R new_knot)
{
    // invariants
    assert(new_knot < 1.0);  // assuming the knot vector is normalized in [0,1]

    // find the first position that is not less than new_knot
    auto it = std::lower_bound(knots.begin(), knots.end(), new_knot);
    assert(it != knots.end());
    assert(*it >= new_knot);

    // go back to the start position for this segment
    auto offset = std::distance(knots.begin(), it) - N;

    // find new coefficients
    std::array<C, N> new_coeffs =
        insert_impl<C, N, R>(&coeffs.at(offset), &knots.at(offset), new_knot);

    // insert new knot
    knots.insert(it, new_knot);

    // replace the affected coefficients, eg for N=3, 2 coefficients become 3
    // here's probably room for improvement, especiall when called repeatedly
    coeffs.erase(coeffs.begin() + offset + 1, coeffs.begin() + offset + N);
    coeffs.insert(coeffs.begin() + offset + 1, new_coeffs.begin(),
                  new_coeffs.end());
}

/** Convert a spline into Bezier form
 *
 * A spline with uniform knot multiplicity = degree N can be trivially
 * converted into bezier form.
 */
template <typename C, int N, typename R = double>
std::vector<std::array<C, N + 1>> to_bezier(std::vector<C> newc,
                                            std::vector<R> newknots)
{
    newknots.reserve(newknots.size() * (N - 1));  // maximum growth
    for (unsigned int i = 0; i < newknots.size(); i++) {
        // find current multiplicity
        int mult = 0;
        while ((i + 1) < newknots.size() &&
               newknots.at(i) == newknots.at(i + 1)) {
            mult++;
            i++;
        }
        // insert the knot mult times
        for (int j = 0; j < (N - mult - 1); j++) {
            insert<C, N, R>(newc, newknots, newknots.at(i));
            ++i;  // each insert just advances the knot vector
        }
    }

    // reap the beziers and store into an array
    std::vector<std::array<C, N + 1>> beziers;
    for (int i = 0, iend = newc.size() - N; i < iend; i += N) {
        std::array<C, N + 1> bz;
        for (int j = 0; j < (N + 1); j++) {
            bz.at(j) = newc.at(i + j);
        }
        beziers.push_back(bz);
    }
    return beziers;
}

}  // namespace distance

