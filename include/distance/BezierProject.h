// -*- c++ -*-
#pragma once

#include <algorithm>  // for copy_n
#include <array>      // for array, array<>::value_type
#include <cmath>      // for fabs, isfinite
#include <cstddef>    // for size_t
#include <limits>     // for numeric_limits
#include <stack>      // for stack
#include <utility>    // for pair

#include "Bezier.h"             // for BezierEvaluator, BezierSplitter
#include "DebugAssert.h"        // for ASSERT, DebugAssert
#include "Vec2d.h"              // for Vec2d, operator-, dot, operator*, vec2
#include "clamp.h"              // for clamp
#include "gems/NearestPoint.h"  // for ConvertToBezierForm2
#include "ignore.h"             // for ignore
#include "lerp.h"               // for lerp

namespace distance {

inline int sign(double d) { return d < 0 ? -1 : d > 0 ? +1 : 0; }

template <int D>
struct root
{
    int type = 0;
    double roots[D]{};
};

template <typename T, std::size_t N>
inline double nrsolve_bracketed(const std::array<T, N>& bezier, double eps)
{
    // finds a solution in a *bracketed* interval containing a *single* root
    ASSERT(sign(bezier[0]) != sign(bezier[N - 1]));

    double t_low = 0;
    double t_high = 1;

    // linearized initial guess, NR solver is dependent on good starting point
    // and it is not very expensive to compute
    // todo, find out when this is bad
    double t =
        distance::clamp(bezier[0] / (bezier[0] - bezier[N - 1]), 0.0, 1.0);
    //    printf("* %.6g\n", t);
    double f, df;

    double dt = t;
    double dt1 = 1, dt2;
    for (int count = 0; count < 64; count++) {
        dt2 = dt1;
        dt1 = dt;
        f = BezierEvaluator<T, 0, N - 1>::eval(bezier.data(), t);
        assert(sign(f) != sign(bezier[0]) || sign(f) != sign(bezier[N - 1]));
        df = BezierEvaluator<T, 1, N - 1>::eval(bezier.data(), t);
        if (f == 0.0) return t;
        dt = f / df;  // gradient, allowed to be inf

        // if the sign at t is same as original t_low, then we
        // haven't crossed the 0-axis and should examine the left bracket
        bool left_bracket = sign(bezier[0]) != sign(f);

        // advance t with dt to find next pos to evaluate
        double t_next = distance::clamp(t - dt, t_low, t_high);
        bool clamped = (t_next == t_low) || (t_next == t_high);
        if ((fabs(dt * 2) > fabs(dt2)) || df == 0.0 || clamped) {
            // slow convergence, override dt and place next_t halfway between
            // current t and t_high/low to subdivide bracketed interval
            if (left_bracket) {
                // use the lhs: [t_low, t]
                dt = (t - t_low) * .5;
            } else {
                // else use the rhs: [t, t_high]
                dt = (t - t_high) * .5;
            }
            // update t_next
            t_next = t - dt;
        }

        // check for convergence by looking at size of dt
        // will only return roots that are correctly bracketed
        // ie. dt must point into the bracketed interval
        if (std::abs(dt) < eps) {
            // if (dt < 0 && !left_bracket) {
            //     // in t, t_high
            //     return t_next;
            // }
            // if (dt > 0 && left_bracket) {
            //     // in t_low, t
            //     return t_next;
            // }
            if ((dt > 0) == left_bracket) {
                return t_next;
            }
        }

        // update brackets for next iteration
        if (left_bracket)
            t_high = t;
        else
            t_low = t;
        t = t_next;
    }

    ASSERT(false && "unreachable");
    return t;
}

template <typename T, std::size_t N>
inline std::size_t numSignChanges(const std::array<T, N>& b)
{
    std::size_t numCross = 0;
    for (std::size_t i = 0; i < N - 1; i++)
        if (sign(b[i]) != sign(b[i + 1]) && sign(b[i + 1]) != 0) {
            ++numCross;
        }
    return numCross;
}

/** Root finding algorithm inspired by * "An unconditionally
 * convergent method for computing zeros of splines and polynomials",
 * by Mørken and Reimers. See SplineZero.h
 *
 * Rename to something more descriptive.
 *
 */
template <typename T, std::size_t N>
inline root<N - 1> solve(std::array<T, N> bezier, double eps)
{
    // Preconditions:
    for (const auto& b : bezier) 
        ASSERT(std::isfinite(b) && "finite numbers only"), ignore(b);

#if 1
    // normalize coeffs
    double biggest = 0;
    for (double d : bezier) {
        if (std::abs(d) > biggest) biggest = std::abs(d);
    }
    for (double& d : bezier) d /= biggest;
#endif
    std::stack<std::array<T, N>> bstack;
    bstack.push(bezier);
    root<N - 1> r;
    std::stack<std::pair<double, double>> ranges;
    ranges.push({0, 1});
    while (!bstack.empty()) {
        std::array<T, N> b = bstack.top();
        bstack.pop();
        std::pair<double, double> range = ranges.top();
        ranges.pop();

        int numCross = numSignChanges(b);  // includes zeros (except rightmost)
                                           //#undef DOPRINT
#ifdef DOPRINT
        printf("%ld [%.9g %.9g] %d : %.9g (%s)\n", bstack.size(), range.first,
               range.second, numCross, (range.second - range.first),
               (b[0] * b[N - 1] <= 0 ? "-" : "+"));
        printf("\tB: %.3g %.3g %.3g %.3g\n", b[0], b[1], b[2], b[3]);
#endif
        if (numCross == 0) {
            // no solution in bezier interval, drop segment
            continue;
        } else if (numCross == 1 && sign(b[0]) != sign(b[N - 1]) &&
                   b[N - 1] != 0) {
            // we bracketed a *single* root!, use NR-solver
#ifdef DOPRINT
            printf("\t[B]: %.4g %.4g %.4g %.4g, %.4g, %.4g\n", b[0], b[1], b[2],
                   b[3], b[4], b[5]);
#endif
            double t = nrsolve_bracketed(b, eps);  // t in 0,1
            // double t = ridder(
            //     [b](T x) {
            //         printf("b(%.16g) = %.16g\n", x,
            //                BezierEvaluator<T, 0, N - 1>::eval(b.data(), x));
            //         return BezierEvaluator<T, 0, N - 1>::eval(b.data(), x);
            //     },
            //     0, 1);
            r.roots[r.type] = lerp(range.first, range.second, t);
            r.type += 1;
            // printf("nr solved a root: [%.9g]\n", r.roots[r.type - 1]);
        } else if (sign(b[0]) != sign(b[N - 1]) &&
                   (range.second - range.first) < eps) {
            // we have at least one root, and it is/they are
            // bracketed in an interval small enough to be acceptable
            // as a single solution
            //            printf("bracket at least one: [%.9g %.9g]\n",
            //            range.first,
            //       range.second);
            r.roots[r.type] = lerp(range.first, range.second,
                                   .5);  // return midpoint of interval
            r.type += 1;
        } else {
            // split and return to top
            std::array<T, N> leftright[2];

            // just split along the middle
            BezierSplitter<double, N - 1>::split(b.data(), .5,
                                                 leftright[0].data());
            // we want to work left to right, therefore push the right segment
            // before the left
            if (numSignChanges(leftright[1])) {
                bstack.push(leftright[1]);
                ranges.push(
                    {lerp(range.first, range.second, .5), range.second});
            }
            bstack.push(leftright[0]);
            ranges.push({range.first, lerp(range.first, range.second, .5)});
            ASSERT(bstack.size() < 32);
        }
    }
    // visit last coefficient
    if (bezier[N - 1] == 0) {
        r.roots[r.type] = 1.0;
        r.type += 1;
    }

    // Postconditions:
    ASSERT(r.type < static_cast<int>(N));
    return r;
}

/* Finds the ordered roots of a Bezier polynomial using interval
 * splitting until a unique root interval is found. The root is then
 * polished with Newton Raphson that uses the bracket.
 *
 * This version uses a fixed size externally allocated
 * stack. Extensive fuzzing and testing suggests that the stack needs
 * not be very big. In the majority of time, an iteration pops one
 * element and only pushes one. Only rarely are both subintervals
 * pushed.
 *
 */

template <typename T, std::size_t N, std::size_t S>
inline root<N - 1> solve_fixed(std::array<T, N> bezier, double eps,
                               std::array<T, N> (&bstack)[S],
                               std::pair<double, double> (&ranges)[S])
{
    static_assert(S >= 4, "use a bigger stack");
    // Preconditions:
    for (const auto& b : bezier)
        ASSERT(std::isfinite(b) && "finite numbers only");

    std::size_t S1 = S;
    std::size_t Ptr = S - 1;
    root<N - 1> r;
    ranges[Ptr] = {0, 1};
    bstack[Ptr] = bezier;

    // work right-to-left
    while (Ptr < S1) {
        const std::array<T, N>& b = bstack[Ptr];
        const std::pair<double, double>& range = ranges[Ptr];

        int numCross = numSignChanges(b);
        bool bracket = sign(b[0]) != sign(b[N - 1]) && sign(b[N - 1]) != 0;
        //#undef DOPRINT
#ifdef DOPRINT
        printf("%ld [%.9g %.9g] %d : %.9g (%s)\n", (S1 - Ptr), range.first,
               range.second, numCross, (range.second - range.first),
               (b[0] * b[N - 1] <= 0 ? "-" : "+"));
        printf("\t%.3g %.3g %.3g %.3g\n", b[0], b[1], b[2], b[3]);
#endif
        if (numCross == 0) {
            // no solution in bezier interval, drop segment
            ++Ptr;
            continue;
        }

        if (numCross == 1 && bracket) {
            // we bracketed a root!, use NR-solver
            double t = nrsolve_bracketed(b, eps);  // t in 0,1
            r.roots[r.type] = lerp(range.first, range.second, t);
            r.type += 1;
        } else if (bracket && (range.second - range.first) < eps) {
            // we have at least one root, and it is/they are
            // bracketed in an interval small enough to be acceptable
            // as a single solution
            //            printf("bracket at least one: [%.9g %.9g]\n",
            //            range.first,
            //       range.second);
            r.roots[r.type] = lerp(range.first, range.second,
                                   .5);  // return midpoint of interval
            r.type += 1;
        } else {
            // split and return to top

            if (Ptr == 0) {
                ASSERT(false);
            }

            // just split along the middle (t=0.5)
            // this works because our stack goes "backwards" or right to left
            BezierSplitter<double, N - 1>::split(bstack[Ptr].data(), .5,
                                                 bstack[Ptr - 1].data());

            // if the right hand side of the interval has no sign
            // changes "skip" it by shifting the queue and *NOT*
            // decrementing Ptr
            if (!numSignChanges(bstack[Ptr])) {
                std::copy_n(&bstack[Ptr - 1], 1, &bstack[Ptr]);
                ranges[Ptr] = {range.first,
                               lerp(range.first, range.second, .5)};
            } else {
                ranges[Ptr - 1] = {range.first,
                                   lerp(range.first, range.second, .5)};
                ranges[Ptr] = {lerp(range.first, range.second, .5),
                               range.second};
                --Ptr;
            }
            --Ptr;
        }
        ++Ptr;
    }
    // visit last coefficient
    if (bezier[N - 1] == 0) {
        r.roots[r.type] = 1.0;
        r.type += 1;
    }

    // Postconditions:
    ASSERT(r.type < static_cast<int>(N));
    return r;
}

/** Finds the closest point on a cubic bezier
 *
 *  Given point P, and cubic bezier Q(t):
 *  Dot(Q'(t), Q(t) - P) == 0
 *
 */
inline std::pair<double, Vec2d> ProjectOnBezier(
    const Vec2d& pos, const std::array<Vec2d, 4>& cubbezier, const double eps)
{
#if MPRINT
    printf("START. eps: %.6g\n", eps);
#endif
    std::array<double, 5 + 1> fifth = ConvertToBezierForm2(pos, cubbezier);
    int numCross = numSignChanges(fifth);

    // initialize to segment start
    std::pair<double, Vec2d> closest{0.0, cubbezier[0]};

    if (numCross > 0) {
        root<5> roots = solve(fifth, eps);

        {
            // compare squared distances to avoid some sqrt calls
            double closest_dist2 = std::numeric_limits<double>::max();
#if MPRINT
            printf("%.3g, N=%d\n ", std::sqrt(closest_dist2), roots.type);
#endif
            for (int i = 0; i < roots.type; i++) {
                double p = roots.roots[i];
                Vec2d trypos =
                    BezierEvaluator<Vec2d, 0, 3>::eval(cubbezier.data(), p);
                double trydist2 = (pos - trypos).length2();
#if MPRINT
                printf("R: %.3g => D: %.3g\n", p, std::sqrt(trydist2));
#endif

                if (trydist2 < closest_dist2) {
                    closest_dist2 = trydist2;
                    closest.second = trypos;
                    closest.first = p;
                }
            }
        }
    }

    // visit segment endpoint explicitly
    {
        double p = 1.0;
        Vec2d trypoint = cubbezier[3];
        if ((pos - trypoint).length2() < (pos - closest.second).length2()) {
            closest.first = p;
            closest.second = trypoint;
        }
    }

    return closest;
}

/** Projects a point along a given vector on a cubic bezier
 *
 *  Given point P, vector W = (-V.y,V.x) and cubic bezier Q(t):
 *  Dot(Q(t) - P, W) == 0
 *
 */
inline std::pair<double, Vec2d> ProjectOnBezier(const Vec2d& P, const Vec2d& V,
                                                const std::array<Vec2d, 4>& Q,
                                                const double eps)
{
    Vec2d W = {-V.y, V.x};
    std::array<double, 4> bpoly = {{
        dot(Q[0] - P, W),
        dot(Q[1] - P, W),
        dot(Q[2] - P, W),
        dot(Q[3] - P, W),
    }};

    // initialize to 'invalid' state
    std::pair<double, Vec2d> closest{-1.0, {}};

    int numCross = numSignChanges(bpoly);

    if (numCross > 0) {
        root<3> roots = solve(bpoly, eps);

        for (int i = 0; i < roots.type; i++) {
            double t = roots.roots[i];
            if (t >= 0.0 && t <= 1.0) {
                Vec2d trypoint =
                    BezierEvaluator<Vec2d, 0, 3>::eval(Q.data(), t);
                if ((closest.first < 0) /* uninitialized */ ||
                    (P - trypoint).length() < (P - closest.second).length()) {
                    closest.first = t;
                    closest.second = trypoint;
                }
            }
        }
    }

    // explicitly visit segment endpoints
    for (auto& tvec : {std::pair<double, Vec2d>(0, Q[0]),
                       std::pair<double, Vec2d>(1, Q[3])}) {
        double t = tvec.first;
        Vec2d trypoint = tvec.second;
        if (std::abs(dot(Vec2d(trypoint - P), W)) < eps) {
            if ((closest.first < 0) /* uninitialized */ ||
                (P - trypoint).length() < (P - closest.second).length()) {
                closest.first = t;
                closest.second = trypoint;
            }
        }
    }

    return closest;
}
}  // namespace distance
