// -*- c++ -*-
#pragma once

#include <algorithm>  // for move, generate_n, transform, upper_bound
#include <cassert>    // for assert
#include <numeric>
#if DISTANCE_HAS_IOSTREAM
#include <iostream>  // for operator<<, cerr, endl, ostream, size_t
#endif
#include <iterator>  // for back_inserter, distance
#include <stack>     // for stack
#include <utility>   // for pair, make_pair
#include <vector>    // for vector

#include "CubicBezier.h"  // for CubicBezier
#include "Vec2d.h"        // for operator/, Vec2d, operator*, operator-, vec2
#include "aabb.h"         // for aabb
#include "clamp.h"        // for clamp
#include "lerp.h"         // for lerp

namespace distance {

/** A cubic Bezier curve
 *
 */
template <typename T>
class PolyBezier3 {
    // probably better to subdivide to be approximately arc-length
    // parameterized with uniform
    std::vector<CubicBezier<T>> beziers_;
    std::vector<double> params_;

  public:
    using point_type = T;
    PolyBezier3 extrapolated(const aabb& bound,
                             typename T::value_type eps) const
    {
        if (beziers_.empty())
            return PolyBezier3<T>(std::vector<CubicBezier<T>>{});

        std::vector<CubicBezier<T>> newBeziers;
        newBeziers.reserve(beziers_.size() + 2);
        std::vector<double> newParams;
        newParams.reserve(params_.size() + 2);

        {
            // extrapolate at start
            const CubicBezier<T>& first = beziers_.front();
            if (bound.contains(first.c0(), eps)) {
                // we are inside the bound
                // need to prepend a bezier
                T dir = first.c0() - first.c1();
                std::pair<double, double> tminmax =
                    bound.intersection(first.c0(), dir);
                T intersection(first.c0() + dir * tminmax.second);
                CubicBezier<T> toPrepend(
                    intersection, lerp(intersection, first.c0(), 1.0 / 3),
                    lerp(intersection, first.c0(), 2.0 / 3), first.c0());
                newBeziers.push_back(toPrepend);
                newParams.push_back(0);
            }
        }

        // copy all beziers in the middle
        std::copy(beziers_.begin(), beziers_.end(),
                  std::back_inserter(newBeziers));  // we have reserved
        std::copy(params_.begin(), params_.end(),
                  std::back_inserter(newParams));  // we have reserved

        {
            // extrapolate at end
            const CubicBezier<T>& last = newBeziers.back();
            if (bound.contains(last.c3(), eps)) {
                // we are inside the grid
                // need to append a bezier
                T dir = last.c3() - last.c2();
                std::pair<double, double> tminmax =
                    bound.intersection(last.c3(), dir);
                Vec2d intersection(last.c3() + dir * tminmax.second);
                CubicBezier<T> toAppend(
                    last.c3(), lerp(last.c3(), intersection, 1.0 / 3),
                    lerp(last.c3(), intersection, 2.0 / 3), intersection);
                newBeziers.push_back(toAppend);
                newParams.push_back(1.0);
            }
        }
        return PolyBezier3<T>(newBeziers, newParams);
    }

    aabb bound() const
    {
        auto fromBezier = [](const CubicBezier<T>& b) -> aabb {
            return aabb(b.c0(), b.c1(), b.c2(), b.c3());
        };
        aabb bbox = fromBezier(bezier(0));
        for (int i = 1; i < count(); i++) {
            bbox.grow(fromBezier(bezier(i)));
        }
        return bbox;
    }

    PolyBezier3 refine() const
    {
        std::vector<CubicBezier<T>> newBeziers(beziers_.size() * 2);
        for (int i = 0; i < count(); i++) {
            const CubicBezier<T>& b = beziers_.at(i);
            b.split(.5, &newBeziers.at(i * 2),
                    &newBeziers.at(i * 2 + 1));  // construct inplace
        }
        return PolyBezier3<T>(newBeziers);
    }
    // a naive implementation that refines to a specific maximum length
    PolyBezier3 refine(double length) const
    {
        std::vector<CubicBezier<T>> newBeziers;
        std::stack<CubicBezier<T>> rhs;
        auto nextBezier = beziers_.begin();
        while (nextBezier != beziers_.end()) {
            assert(nextBezier < beziers_.end());
            // generate next bezier
            rhs.push(*nextBezier);  // push onto stack
            while (!rhs.empty()) {  // work on stack
                CubicBezier<T> lhs = rhs.top();
                rhs.pop();
                if (lhs.length() < 2 * length) {
                    if (lhs.length() < length * 1.25) {
                        newBeziers.push_back(lhs);
                    } else {
                        CubicBezier<T> b1, b2;
                        lhs.split(.5, &b1, &b2);
                        newBeziers.push_back(b2);
                        newBeziers.push_back(b1);
                    }
                } else {
                    CubicBezier<T> b1, b2;
                    double r = length /
                               lhs.length();  // assume uniform parametrization
                    lhs.split(r, &b1, &b2);
                    double r2 =
                        b1.length() /
                        length;  // how good is b1? closer to one is better

                    if (r2 < .75 || r2 > 1.25) {
                        // resplit, if there's enough room for improvements
                        double f =
                            clamp(r / clamp(r2, .33333, 3.0), 0.01, 0.99);
                        lhs.split(f, &b1, &b2);
                    }

                    rhs.push(b2);
                    rhs.push(b1);
                }
            }
            nextBezier++;  // add next segment to stack
        }
        return PolyBezier3<T>(newBeziers);
    }
    PolyBezier3() = default;
    explicit PolyBezier3(std::vector<CubicBezier<T>> beziers)
        : beziers_(std::move(beziers))
    {
        // uniform parametrization
        // suboptimal
        int p = 0;
        int N = beziers_.size() + 1;
        std::generate_n(std::back_inserter(params_), N,
                        [&p, N]() -> double { return (p++) / (N - 1.0); });
        assert(params_.front() == 0);
        assert(params_.back() == 1);
    }
    PolyBezier3(std::pair<std::vector<CubicBezier<T>>, std::vector<double>> bp)
        : PolyBezier3(std::move(bp.first), std::move(bp.second))
    {
        // delegate
    }
    PolyBezier3(std::vector<CubicBezier<T>> beziers, std::vector<double> params)
        : beziers_(std::move(beziers)), params_(std::move(params))
    {
        assert(beziers_.size() + 1 == params_.size());
        assert(params.empty() || params_.front() == 0);
        assert(params_.size() > 1 ? params_.back() > 0 : params_.back() == 0);
    }

    T eval(double t) const
    {
        // clamp input to interpolating region
        double tc = distance::clamp(t, 0.0, 1.0);

        // find interval
        auto it = std::upper_bound(params_.begin(), params_.end(), tc);
        if (it == params_.end()) --it;
        assert(it < params_.end());
        assert(it > params_.begin());
        double t1 = *it;
        --it;
        double t0 = *it;
        auto offset = std::distance(params_.begin(), it);

        // map to inner parameter
        double u = (tc - t0) / (t1 - t0);  // the fraction along t0-t1

        // evaluate local Bezier
        assert(offset >= 0);
        assert(!beziers_.empty());
        return beziers_.at(offset).eval(u);
    }

    int count() const { return beziers_.size(); }
    const CubicBezier<T>& bezier(int i) const { return beziers_.at(i); }
    const std::vector<CubicBezier<T>>& beziers() const { return beziers_; }
    double param(int i) const { return params_.at(i); }
    const std::vector<double>& params() const { return params_; }
    bool is_closed() const
    {
        return !beziers_.empty() &&
               beziers_.front().c0() == beziers_.back().c3();
    }
};

/** \short
 *    http://en.wikipedia.org/wiki/Cubic_Hermite_spline
 */
template <class T>
class HermiteInterpolant {
  public:
    template <typename RandomAccessIter>
    static PolyBezier3<T> interpolate(RandomAccessIter begin,
                                      RandomAccessIter end)
    {
        return converter(begin, end);
    }
    static PolyBezier3<T> interpolate(const std::vector<T>& dataPoints)
    {
        return converter(dataPoints.begin(), dataPoints.end());
    }

  private:
    // convert data points to bezier coefficients
    template <typename RandomAccessIter>
    static std::pair<std::vector<CubicBezier<T>>, std::vector<double>>
    converter(RandomAccessIter begin, RandomAccessIter end)
    {
        using std::distance;
        std::size_t count = distance(begin, end);

        RandomAccessIter pos = begin;
        using PairType =
            std::pair<std::vector<CubicBezier<T>>, std::vector<double>>;

        if (count == 0)
            return PairType({}, {0});
        else if (count == 1) {  // single point -> 1 bezier
            return PairType({{{*pos}, {*pos}, {*pos}, {*pos}}}, {{0}, {1}});
        } else if (count == 2) {  // two points, straight line -> 1 bezier
            return PairType(
                {{*pos, 2.0 / 3 * (*pos) + 1.0 / 3 * *(pos + 1),
                  1.0 / 3 * *pos + 2.0 / 3 * *(pos + 1), *(pos + 1)}},
                {0, 1});
        }

        // three or more points, normal case
        std::vector<CubicBezier<T>> beziers;
        bool is_closed = *begin == *(end - 1);

        // compute chord lengths for parametrization
        std::vector<double> chords;
        chords.reserve(count);

        // std::transform takes InputIterators (only != no <), so need
        // early exits above (can't test if "end+1 != end")
        std::transform(begin + 1, end, begin, std::back_inserter(chords),
                       [](const Vec2d& p2, const Vec2d& p1) -> double {
                           return ((p2 - p1).length());
                       });

        beziers.reserve((count - 1));

        std::vector<T> diff2;
        diff2.reserve(count - 2);
        for (std::size_t i = 1, num = count - 1; i < num; i++) {
            // central difference
            diff2.push_back(*(begin + i + 1) - *(begin + i - 1));
        }
        auto fduIt = chords.begin();

        typename std::vector<T>::const_iterator diffIt = diff2.begin();
        auto pointsIt = begin;
        auto secondLastPoint = end - 2;  // random access iterator

        // 1. handle first Bezier
        {
            CubicBezier<T> b;
            if (!is_closed) {
                double fdu0 = *fduIt;
                double fdu1 = *(fduIt + 1);

                // FMILL tangent
                double uRatio = fdu0 / (fdu0 + fdu1);
                Vec2d Tm = uRatio * (*diffIt);

                // Bessel tangent, see Farin, Curves for surf. and CAGD, p 128.
                // slightly modified to use the FMILL tangent instead of m1
                // double alpha1 = fdu0 / (fdu0 + fdu1);
                Vec2d fdPoint0 = *(pointsIt + 1) - *pointsIt;
                Vec2d m1 = Tm / fdu0;  //( (1-alpha1) / fdu0 * fdPoint0) +
                                       //(alpha1 / fdu1 * fdPoint1);
                Vec2d m0 = (2 * fdPoint0 / fdu0 - m1);
                b = CubicBezier<T>(*pointsIt,
                                   *pointsIt + m0 * fdu0 / 3,  // Bessel
                                   *(pointsIt + 1) - Tm / 3,   // FMILL
                                   *(pointsIt + 1));
            } else {
                // closed case is just like inner beziers
                // parameter forward differences
                double fdum = chords.back();  // last segment
                double fdui = *(fduIt);       // current segment
                double fdup = *(fduIt + 1);   // next segment
                // the FMILL tangents use the central difference direction
                // vector scaled to to match the
                // relative lengths of the parameter differences (fdu)

                Vec2d Tp = fdui / (fdum + fdui) * (*(begin + 1) - *(end - 2));
                Vec2d Tm = fdui / (fdui + fdup) * (*diffIt);

                // converting between a tangent in a Cubic Hermite spline and a
                // Bezier coefficient is
                // simply B = B* +- T/3
                b = CubicBezier<T>(*pointsIt, *pointsIt + Tp / 3.0,
                                   *(pointsIt + 1) - Tm / 3.0, *(pointsIt + 1));
            }
            beziers.emplace_back(b);
        }
        // 2. handle middle Beziers
        while (++pointsIt != secondLastPoint) {
            assert(pointsIt < secondLastPoint);
            assert(diffIt != diff2.end());
            assert(pointsIt != end);

            // parameter forward differences
            double fdum = *fduIt;        // last segment
            double fdui = *(fduIt + 1);  // current segment
            double fdup = *(fduIt + 2);  // next segment
            // the FMILL tangents use the central difference direction
            // vector scaled to to match the
            // relative lengths of the parameter differences (fdu)

            Vec2d Tp = fdui / (fdum + fdui) * (*diffIt);
            Vec2d Tm = fdui / (fdui + fdup) * *(diffIt + 1);

            // converting between a tangent in a Cubic Hermite spline and a
            // Bezier coefficient is
            // simply B = B* +- T/3
            beziers.emplace_back(*pointsIt, *pointsIt + Tp / 3.0,
                                 *(pointsIt + 1) - Tm / 3.0, *(pointsIt + 1));
            ++diffIt;
            ++fduIt;
        }

        // 3. handle last Bezier
        {
            assert(pointsIt == secondLastPoint);
            assert((diffIt + 1) == diff2.end());  // should be last difference
            CubicBezier<T> b;

            if (!is_closed) {
                double fdum = *fduIt;        // last segment
                double fdui = *(fduIt + 1);  // current segment

                // FMILL tangent
                double uRatio = fdui / (fdum + fdui);
                Vec2d Tp = uRatio * (*diffIt);

                // Bessel tangent, see Farin, Curves for surf. and CAGD, p 128.
                // slightly modified to use the FMILL tangent instead of mLm
                // double alpha1 = fdum / (fdum + fdui);
                // Vec2d fdPoint0 = *(pointsIt) - *(pointsIt-1);
                Vec2d fdPoint1 = *(pointsIt + 1) - *(pointsIt + 0);
                Vec2d mLm = Tp / fdui;  //( (1-alpha1) / fdu0 * fdPoint0) +
                                        //(alpha1 / fdu1 * fdPoint1);
                Vec2d mL = 2 * fdPoint1 / fdui - mLm;

                b = CubicBezier<T>(*pointsIt,
                                   *pointsIt + Tp / 3.0,  // FMILL
                                   // Bessel tangent, see Farin, Curves for
                                   // surf. and CAGD, p 129.
                                   *(pointsIt + 1) - mL * fdui / 3,
                                   *(pointsIt + 1));
                assert((pointsIt + 2) == end);
            } else {
                // closed case is just like inner beziers
                // parameter forward differences
                double fdum = *(fduIt);        // last segment
                double fdui = *(fduIt + 1);    // current segment
                double fdup = chords.front();  // next segment
                // the FMILL tangents use the central difference direction
                // vector scaled to to match the
                // relative lengths of the parameter differences (fdu)

                Vec2d Tp = fdui / (fdum + fdui) * (*diffIt);
                Vec2d Tm = fdui / (fdui + fdup) * (*(begin + 1) - *(end - 2));

                // converting between a tangent in a Cubic Hermite spline and a
                // Bezier coefficient is
                // simply B = B* +- T/3
                b = CubicBezier<T>(*pointsIt, *pointsIt + Tp / 3.0,
                                   *(pointsIt + 1) - Tm / 3.0, *(pointsIt + 1));
            }

            beziers.emplace_back(b);
        }

        double sum_inv =
            1.0 / std::accumulate(chords.begin(), chords.end(), 0.0);
        std::vector<double> params(count, 0.);
        for (std::size_t i = 1; i < count; i++) {
            params.at(i) = params.at(i - 1) + chords.at(i - 1) * sum_inv;
        }
        params.back() = 1.0;  // make sure
        return std::make_pair(std::move(beziers), std::move(params));
    }
};

}  // namespace distance
