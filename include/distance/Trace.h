// -*- c++ -*-
#pragma once

#if DISTANCE_HAS_IOSTREAM

#include <stddef.h>          // for size_t
#include <algorithm>         // for forward
#include <initializer_list>  // for initializer_list
#include <iostream>          // for operator<<, ostream, basic_ostream, ost...
#include <memory>            // for unique_ptr
#include <sstream>
#include <string>       // for string
#include <type_traits>  // for is_const
#include <typeinfo>     // for type_info
#include "ignore.h"

namespace distance {

// The base concatenation implementation uses op<<(ostream, T)
template <typename T>
struct Con
{
    static void cat(std::ostream& os, const T& t) { os << t; }
};

// The base case for inserting types into the stream for concatenation
// Don't specialize this, specialize Con<T>::cat, see
// http://www.gotw.ca/publications/mill17.htm for info of the pitfalls
// of function specialization. Class specialization is safer and
// clearer
template <typename T>
void concat(std::ostream& os, const T& t)
{
    Con<T>::cat(os, t);
}

// a struct for wrapping pointer types for special concat/stringification
// behavior
template <typename T>
struct PtrInfo
{
    T* t;
    PtrInfo(T* t) : t(t) {}
};
// the maker to simplify deduction
template <typename T>
PtrInfo<T> make_ptrinfo(T* t)
{
    return PtrInfo<T>(t);
}
// a struct for wrapping array types for special concat/stringification
// behavior
template <typename T>
struct ArrayInfo
{
    // T (&t)[N];
    T* t;  // store pointer instead of array ref to allow ArrayInfo from pointer
    size_t N;  // and size
    ArrayInfo(T* t, size_t N) : t(t), N(N) {}
};
// the maker to simplify deduction, only accepts array refs
template <typename T, size_t N>
ArrayInfo<T> make_arrayinfo(T (&t)[N])
{
    return ArrayInfo<T>(t, N);
}

// output extra info for the PtrInfo types
template <typename T>
struct Con<PtrInfo<T>>
{
    static void cat(std::ostream& os, const PtrInfo<T>& ptr)
    {
        os << ptr.t << (std::is_const<T>::value ? " const" : "");
        os << " (";
        if (ptr.t != nullptr)
            os << *ptr.t;
        else
            os << "nullptr";
        os << ")";  //<" << typeid(T).name() << ">";
    }
};

// output extra info for the ArrayInfo types
template <typename T>
struct Con<ArrayInfo<T>>
{
    static void cat(std::ostream& os, const ArrayInfo<T>& ptr)
    {
        os << ptr.t << (std::is_const<T>::value ? " const" : "");
        os << " [";
        if (ptr.t != nullptr) {
            os << *ptr.t;
            for (std::size_t i = 1; i < ptr.N; i++) os << ", " << ptr.t[i];
        } else
            os << "nullptr ...";
        os << "]";  //<" << typeid(T).name() << ">";
    }
};

// unwrap shared pointers
template <typename T>
struct Con<std::shared_ptr<T>>
{
    static void cat(std::ostream& os, const std::shared_ptr<T>& ptr)
    {
        os << "shared_ptr<" << typeid(T).name() << "> ";
        concat(os, make_ptrinfo(ptr.get()));  // delegate to PtrInfo
        os << " use_count: " << ptr.use_count();
    }
};

// unwrap unique pointers
template <typename T>
struct Con<std::unique_ptr<T>>
{
    static void cat(std::ostream& os, const std::unique_ptr<T>& ptr)
    {
        os << "unique_ptr<" << typeid(T).name() << "> ";
        concat(os, make_ptrinfo(ptr.get()));  // delegate to PtrInfo
    }
};

// Variadic concatenation, overrideable stringification behavior for
// types. To tune the behavior of your type you have two options:
//  a) specialize the class template Con<T> with static member void
//  cat(std::ostream &os, const T& t)
//  b) implement operator<< for your type
//
// Option a) is less intrusive wrt normal streaming, but the base case
// for a) dispatches to b)
template <typename... Args>
std::string concatenate(Args&&... args)
{
    std::ostringstream ss;
    bool noComma = true;
    ignore(noComma);
    (void)std::initializer_list<bool>{
        false, (concat(ss, noComma ? "" : ", "),
                concat(ss, std::forward<Args>(args)), noComma = false)...};
    return ss.str();
}
}  // namespace distance
#endif // DISTANCE_HAS_IOSTREAM
