// -*- c++ -*-
#pragma once

#include <stdio.h>    // for printf
#include <algorithm>  // for max
#include <cmath>      // for floor, ceil, isinf
#include <utility>    // for pair, make_pair, operator!=
#include "DebugAssert.h"
#include "Vec2d.h"  // for Vec2d

namespace distance {

class dda {
    /*
      A Fast Voxel Traversal Algorithm for Ray Tracing
      John Amanatides and Andrew Woo
     */

    double tMaxX_{}, tMaxY_{};
    double tDeltaX_{}, tDeltaY_{};
    std::pair<int, int> pos_;
    std::pair<int, int> end_;
    int stepX_, stepY_;

    mutable int counter_;
    int maxCount_;

  public:
    dda() : pos_{0, 0}, end_{0, 0}, counter_(1), maxCount_(1) {}
    dda(double x1, double y1, double x2, double y2)
    {
        pos_ = std::make_pair(std::floor(x1), std::floor(y1));
        end_ = std::make_pair(std::floor(x2), std::floor(y2));

        ASSERT((x1 == x2 && y1 == y2) || std::abs(x1 - x2) > 1e-6 ||
               std::abs(y1 - y2) > 1e-6);

        ASSERT(std::isfinite(x1) && std::isfinite(y1) && std::isfinite(x2) &&
               std::isfinite(y2));

        counter_ = 0;
        // set the max count to a heuristic value based on manhattan distance
        // could the heuristic fail for some "scales"?
        maxCount_ = 2 + (std::abs(pos_.first - end_.first) +
                         std::abs(pos_.second - end_.second));
        // std::cerr << "maxCount " << maxCount_ << std::endl;

        if (pos_ == end_) {
            // only one pixel to visit
            // the step counter will make sure the pos_ pixel is visited
            // before the visitor is exhausted
            stepX_ = stepY_ = 0;
        } else {
            stepX_ = x2 >= x1 ? +1 : -1;
            stepY_ = y2 >= y1 ? +1 : -1;

            Vec2d dir = Vec2d(x2 - x1, y2 - y1).normalized();

            tMaxX_ = tDeltaX_ = stepX_ / dir.x;  // dir component may be inf
            tMaxY_ = tDeltaY_ = stepY_ / dir.y;  //

            double txfrac =
                (stepX_ > 0 ? (std::ceil(x1) - x1) : (x1 - std::floor(x1)));
            if (!std::isinf(tDeltaX_)) tMaxX_ *= txfrac;

            double tyfrac =
                (stepY_ > 0 ? (std::ceil(y1) - y1) : (y1 - std::floor(y1)));
            if (!std::isinf(tDeltaY_)) tMaxY_ *= tyfrac;

            if (tMaxX_ == 0 && stepX_ > 0) tMaxX_ = tDeltaX_;
            if (tMaxY_ == 0 && stepY_ > 0) tMaxY_ = tDeltaY_;
        }
    }

    bool hasNext() const
    {
        ASSERT(counter_ <= maxCount_);
        return pos_ != end_ || counter_ == 0;
    }
    std::pair<int, int> next()
    {
        //      std::cerr << "next()" << std::endl;
        if (counter_++ == 0) {
            // the first step returns start pos
            return pos_;
        }

        // std::cerr << "<tmax: " << tMaxX_ << ", " << tMaxY_ << std::endl;
        if (tMaxX_ <= tMaxY_) {
            tMaxX_ += tDeltaX_;
            pos_.first += stepX_;
            //    last_ = 0; // x-dir
        } else {
            tMaxY_ += tDeltaY_;
            pos_.second += stepY_;
            // last_ = 1; // y-dir
        }
        //  std::cerr << ">tmax: " << tMaxX_ << ", " << tMaxY_ << std::endl;
        return pos_;
    }
};

}  // namespace distance
