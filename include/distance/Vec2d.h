// -*- c++ -*-
#pragma once

#include <stdlib.h>  // for abs

#include <cassert>
#include <cmath>             // for sqrt
#include <initializer_list>  // for initializer_list
#include <tuple>             // for tie
#include <type_traits>       // for integral_constant<>::value, alignment_of
#include <utility>           // for make_pair, pair

#if DISTANCE_HAS_IOSTREAM
#include <iostream>  // for ostream, operator<<
#endif

#pragma push_macro("TIE")
#undef TIE
#define TIE(X) std::tie((X).x, (X).y)
#pragma push_macro("FRIENDOP")
#undef FRIENDOP
#define FRIENDOP(T, op)                               \
    bool friend operator op(const T& v1, const T& v2) \
    {                                                 \
        return TIE(v1) op TIE(v2);                    \
    }

namespace distance {

// free function extractors to make the vector/point type interchangeable
template <typename V>
auto first(const V& v) -> decltype(v.x)
{
    return v.x;
}
template <typename V>
auto second(const V& v) -> decltype(v.y)
{
    return v.y;
}

template <typename T>
struct vec2
{
    using value_type = T;
    value_type x, y;
    vec2() = default;
    vec2(const vec2&) = default;
    vec2(vec2&&) = default;
    vec2& operator=(const vec2&) = default;
    vec2(value_type a, value_type b) : x(a), y(b) {}
    vec2(std::initializer_list<value_type> data)
        : x(*data.begin()), y(*(data.begin() + 1))
    {
    }
    value_type length2() const { return x * x + y * y; }
    value_type manhattan() const { return std::abs(x) + std::abs(y); }
    value_type length() const { return sqrt(length2()); }
    friend value_type length(const vec2& v) { return v.length(); }
    vec2 friend operator-(const vec2& v) { return vec2(-v.x, -v.y); }
    vec2 friend operator-(const vec2& v1, const vec2& v2)
    {
        return vec2(v1.x - v2.x, v1.y - v2.y);
    }
    vec2 friend operator+(const vec2& v1, const vec2& v2)
    {
        return vec2(v1.x + v2.x, v1.y + v2.y);
    }
    vec2 friend operator*(value_type s, const vec2& v)
    {
        return vec2(s * v.x, s * v.y);
    }
    vec2 friend operator*(const vec2& v, value_type s)
    {
        return vec2(v.x * s, v.y * s);
    }
    // Pairwise mult, just like GLSL, see dot
    vec2 friend operator*(const vec2& v, const vec2& v2)
    {
        return vec2(v.x * v2.x, v.y * v2.y);
    }
    vec2 friend operator/(const vec2& v, value_type s)
    {
        return vec2(v.x / s, v.y / s);
    }
    // Pairwise div, just like GLSL
    vec2 friend operator/(const vec2& v, const vec2& v2)
    {
        return vec2(v.x / v2.x, v.y / v2.y);
    }
    vec2 normalized() const
    {
        value_type l = length();
        return vec2(x / l, y / l);
    }
    friend vec2 normalized(const vec2& v) { return v.normalized(); }
    vec2 orthogonal() const { return vec2(-y, x); }
    friend vec2 orthogonal(const vec2& v) { return v.orthogonal(); }

    value_type friend cross(const vec2& v1, const vec2& v2)
    {
        return v1.x * v2.y - v1.y * v2.x;
    }
    value_type friend dot(const vec2& v1, const vec2& v2)
    {
        return v1.x * v2.x + v1.y * v2.y;
    }
    FRIENDOP(vec2, <);
    FRIENDOP(vec2, >);
    FRIENDOP(vec2, <=);
    FRIENDOP(vec2, >=);
    FRIENDOP(vec2, !=);
    FRIENDOP(vec2, ==);
};

#if DISTANCE_HAS_IOSTREAM
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::pair<T, T>& p)
{
#ifdef MATHEMATICA
    return (os << '{' << p.first << ", " << p.second << '}');
#else
    return (os << '(' << p.first << ", " << p.second << ')');
#endif
}
template <typename T>
std::ostream& operator<<(std::ostream& os, const vec2<T>& v)
{
    return (os << std::make_pair(v.x, v.y));
}
#endif

using Vec2d = vec2<double>;
static_assert(std::is_standard_layout<Vec2d>::value,
              "Standard layout required");
static_assert(sizeof(Vec2d) == sizeof(Vec2d::value_type) * 2,
              "Size must match two doubles");
static_assert(std::alignment_of<Vec2d>::value ==
                  std::alignment_of<Vec2d::value_type>::value,
              "Alignment must match double*");
}  // namespace distance

#pragma pop_macro("TIE")
#pragma pop_macro("FRIENDOP")
