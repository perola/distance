// -*- c++ -*-
#pragma once

#include <array>
#include <stddef.h>

namespace distance {

template <int n, int k, bool valid = (n >= k)>
struct Binomial
{
    constexpr static int value()
    {
        return (Binomial<n - 1, k - 1>::value() + Binomial<n - 1, k>::value());
    }
};
template <int n, int k>
struct Binomial<n, k, false>
{
    constexpr static int value() { return 0; }
};

template <>
struct Binomial<0, 0, true>
{
    constexpr static int value() { return 1; };
};

template <int n>
struct Binomial<n, 0, true>
{
    constexpr static int value() { return 1; };
};

template <int n>
struct Binomial<n, n, true>
{
    constexpr static int value() { return 1; };
};

template <int n, int k>
inline constexpr int binomial()
{
    return Binomial<n, k>::value();
}

#if __cplusplus < 201400
template <size_t... Indices>
struct integer_sequence
{
    using type = integer_sequence;
};

template <size_t N, size_t... II>
struct make_index_sequence;

template <size_t... II>
struct make_index_sequence<0, II...> : integer_sequence<II...>
{
};

template <size_t N, size_t... II>
struct make_index_sequence : make_index_sequence<N - 1, N - 1, II...>
{
};
template <typename T, size_t... i>
inline constexpr std::array<T, sizeof...(i)> binomialtable(
    integer_sequence<i...>)
{
    return {{static_cast<T>(Binomial<sizeof...(i) - 1, i>::value())...}};
}

template <int N, typename T = double>
struct BinomialTable
{
    using Seq = typename make_index_sequence<N + 1>::type;
    // VS2015 (update 1) fails to compile the next line, update 3 passes as
    // expected
    static constexpr const std::array<T, N + 1> table = binomialtable<T>(Seq{});
};
#else
#include <utility>  // the C++14 way of doing things
using std::integer_sequence;
using std::index_sequence;
using std::make_index_sequence;
template <typename T, size_t... i>
inline constexpr std::array<T, sizeof...(i)> binomialtable(
    std::index_sequence<i...>)
{
    return {{static_cast<T>(Binomial<sizeof...(i) - 1, i>::value())...}};
}

template <int N, typename T = double>
struct BinomialTable
{
    // using Seq = typename make_index_sequence<N + 1>::type;
    // VS2015 (update 1) fails to compile the next line, update 3 passes as
    // expected
    static constexpr const std::array<T, N + 1> table =
        binomialtable<T>(make_index_sequence<N + 1>{});
};

#endif

// provide an address to allow taking const-ref of member
// this avoids undefined reference linking errors:
//   ... BinomialTable<...>::table referenced from ...
template <int N, typename T>
const std::array<T, N + 1> BinomialTable<N, T>::table;

}  // ns distance
