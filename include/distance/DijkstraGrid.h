// -*- c++ -*-
#pragma once

#include <assert.h>        // for assert
#include <algorithm>       // for fill, move, for_each
#include <cmath>           // for sqrt
#include <cstddef>         // for size_t
#include <cstdlib>        
#include <limits>          // for numeric_limits
#include <queue>           // for pop_heap, push_heap
#include <tuple>           // for get, make_tuple, tuple
#include <vector>          // for vector
#include "Array2.h"        // for Array2
#include "DistanceTags.h"  // for State, State::Initial, State::Far, State::...
#include "Index2.h"        // for Index2

namespace distance {

class DijkstraGrid {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx_, dy_, hyp_;

    std::vector<Index2> heapData_;

  public:
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
        double dy)
    {
        DijkstraGrid dg_pos(begin, end, sx, sy, dx, dy);
        dg_pos.run();

        // keep the data grids, but negate distances
        DijkstraGrid dg_neg(-dg_pos.distanceGrid_, dg_pos.stateGrid_,
                            dg_pos.updateGrid_, dx, dy);
        dg_neg.run();

        Array2d d = dg_pos.distanceGrid_;
        for (int li = 0, liend = d.count(); li < liend; ++li) {
            if (std::abs(dg_neg.distanceGrid_(li)) < std::abs(d(li)))
                d(li) = -dg_neg.d(li);
        }
        return std::make_tuple(d, dg_neg.stateGrid_, dg_neg.updateGrid_);
    }

    DijkstraGrid(Array2d d, Array2<State> s, Array2<int> u, double dx,
                 double dy)
        : distanceGrid_(std::move(d)),
          stateGrid_(std::move(s)),
          updateGrid_(std::move(u)),
          dx_(dx),
          dy_(dy),
          hyp_(std::sqrt(dx * dx + dy * dy))
    {
        for (int li = 0, liend = stateGrid_.count(); li < liend; li++) {
            if (stateGrid_(li) == State::Far) {
                distanceGrid_(li) = std::numeric_limits<double>::max();
            }
        }
        for (int li = 0, liend = stateGrid_.count(); li < liend; li++) {
            if (stateGrid_(li) == State::Initial) {
                if (distanceGrid_(li) >= 0) {
                    // only do positive side
                    visit(stateGrid_.li2coord(li));
                }
            }
        }
    }

    template <typename ForwardIter>
    DijkstraGrid(ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
                 double dy)
        : distanceGrid_(sx, sy),
          stateGrid_(sx, sy),
          updateGrid_(sx, sy),
          dx_(dx),
          dy_(dy),
          hyp_(std::sqrt(dx * dx + dy * dy))
    {
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        initialize(begin, end);
    }

    template <typename ForwardIter>
    void initialize(ForwardIter begin, ForwardIter end)
    {
        // initialize narrow band
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                if (std::abs(std::get<1>(p)) <
                    std::abs(d(std::get<0>(p))))  // the smallest initializer
                                                  // no matter sign should
                                                  // "win"
                    touch(std::get<0>(p), std::get<1>(p), State::Initial);
            });

        // bootstrap algorithm by visiting the points in the narrow band and
        // thus updating
        // and adding their neighbors
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                const double epsilon = 0;  //  Increase epsilon if the algorithm
                //  starts picking up initializers on the
                //  wrong side of the interface, eg.
                //  std::min(dx_/2, dy_/2)
                if (std::get<1>(p) > epsilon) {
                    visit(std::get<0>(p));
                }
            });
    }

    void visit(const Index2& pt)
    {
        // DON'T VISIT POINTS ON THE NEGATIVE SIDE
        // THEY MIGHT HAVE NEIGHBORS WHICH ARE NOT YET
        // SET AND IF ADDED TO THE QUEUE VIOLATES THE
        // POSITIVE REQUIREMENTS
        if (distanceGrid_(pt) <= 0) return;

        // UPDATE (AND RESTART) NEIGHBORS
        Index2 inds[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                         pt.s(), pt.sw(), pt.w(), pt.nw()};
        for (const Index2& ind : inds) {
            if (!BOUNDS(ind)) continue;
            if (stateGrid_(ind) == State::Initial) continue;
            if (distanceGrid_(ind) < 0) {
                continue;  // don't go negative
            }

            double d = computeDistance(ind, pt);
            assert(d >= 0);
            updateGrid_(ind)++;
            if (d < distanceGrid_(ind)) {
                assert(stateGrid_(ind) != State::Initial);
                touch(ind, d);
                add(ind);
            }
        }
    }

    void run()
    {
#ifndef NDEBUG
        std::size_t i = 0;
#endif
        while (hasNext()) {
#ifndef NDEBUG
            // upper bound num iterations, normally 8
            int maxVisitsPerPoint = 12;
            if (++i > distanceGrid_.dim1() * distanceGrid_.dim2() *
                          maxVisitsPerPoint) {
                // mexPrintf("early exit\n");
                break;
            }
#endif
            Index2 pt = next();
            if (distanceGrid_(pt) < 0)
                // mexPrintf("%d,%d, %.3g NEGATIVE\n", pt.i, pt.j,
                //          distanceGrid_(pt));

                if (stateGrid_(pt) == State::Final) continue;
            stateGrid_(pt) = State::Final;
            visit(pt);
        }
    }

    const Array2d& getDistanceGrid() const { return distanceGrid_; }
    const Array2<State>& getStateGrid() const { return stateGrid_; }
    const Array2<int>& getUpdateGrid() const { return updateGrid_; }

  protected:
    double d(const Index2& p) const { return distanceGrid_(p); }
    double d(std::size_t li) const { return distanceGrid_(li); }
    bool BOUNDS(const Index2& p) const
    {
        return (p.i >= 0 && p.j >= 0 &&
                p.i < static_cast<int>(distanceGrid_.dim1()) &&
                p.j < static_cast<int>(distanceGrid_.dim2()));
    }
    double computeDistance(const Index2& pt, const Index2& /*from*/) const
    {
        const Index2 inds[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                               pt.s(), pt.sw(), pt.w(), pt.nw()};

        // use symmetry to reduce the size of the look-up
        const double dij[] = {dy_, hyp_, dx_, hyp_};
        // const double dik[] = {hyp_, dx_, hyp_, dy_}; // use offset instead

        double dnew = std::numeric_limits<double>::max();

        for (int i = 0; i < 8; i++) {
            const Index2& ind1 = inds[i];

            double Dj = std::numeric_limits<double>::max();
            if (BOUNDS(ind1)) Dj = distanceGrid_(ind1);
            double dtentative = dij[i % 4] + Dj;
            assert(dtentative >= 0);
            if (std::abs(dtentative) < std::abs(dnew)) dnew = dtentative;
#if 0
    	    const Index2& ind1 = inds[i];
            const Index2& ind2 =
                inds[(i + 1) % 8];  // wrap around for last entry

            double Dj = std::numeric_limits<double>::max();
            if (BOUNDS(ind1)) Dj = distanceGrid_(ind1);
            double Dk = std::numeric_limits<double>::max();
            if (BOUNDS(ind2)) Dk = distanceGrid_(ind2);

            double Dij = dijk[i % 4];
            double Dik = dijk[(i + 1) % 4];

            dnew = std::min(dnew, std::min(Dij + Dj, Dik + Dk));
#endif
        }
        return dnew;
    }

    void touch(const Index2& index, double val, State state = State::Active)
    {
        // distanceGrid_(index), state);
        if (std::abs(val) >
            std::abs(distanceGrid_(
                index))) {  // mexErrMsgTxt("new val is not lower");
        }
        distanceGrid_(index) = val;
        stateGrid_(index) = state;
    }

    void add(const Index2& index)
    {
        heapData_.push_back(index);
        // push_heap requires that (first, last-1) is already heap.
        std::push_heap(heapData_.begin(), heapData_.end(),
                       [&](const Index2& l, const Index2& r) {
                           return distanceGrid_(l) > distanceGrid_(r);
                       });
    }
    bool hasNext() const { return !heapData_.empty(); }
    Index2 next()
    {
        // calling next on a heap for which hasNext is false will
        // throw
        std::pop_heap(heapData_.begin(), heapData_.end(),
                      [&](const Index2& l, const Index2& r) {
                          return distanceGrid_(l) > distanceGrid_(r);
                      });
        Index2 ind = heapData_.back();  // throws if empty
        heapData_.pop_back();
        return ind;
    }
};

}  // namespace distance
