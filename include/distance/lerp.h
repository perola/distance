// -*- c++ -*-
#pragma once

#include <type_traits>

namespace distance {

/** Linear interpolation
 * Pretty catch-all but requires floating point parameter 't' to prohibit
 * bad matches like lerp(double, vec, vec), lerp(float, class, float), etc...
 */
template <typename T1, typename T2>
constexpr std::enable_if_t<std::is_floating_point<T2>::value, T1> lerp(
    const T1& x0, const T1& x1, T2 t)
{
    return x0 * (1 - t) + x1 * t;
}
}  // namespace distance
