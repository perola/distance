#pragma once

#include <algorithm>
#include <deque>

#if DISTANCE_HAS_IOSTREAM
#include <iostream>
#endif
#include <cmath>
#include <set>
#include <type_traits>
#include <utility>
#include <vector>

#include "Array2.h"
#include "Bresenham.h"
#include "Vec2d.h"
#include "aabb.h"
#include "clamp.h"
#include "dda.h"
#include "detectors.h"
#include "lerp.h"

namespace distance {

template <typename Point_t>
bool isfinite(const Point_t& p)
{
    return std::isfinite(first(p)) && std::isfinite(second(p));
}

enum class Side
{
    Left,
    Interface,
    Right
};

template <typename Point_t>
struct ProjectionPoint
{
    Point_t projection;
    typename Point_t::value_type parameter;
};
template <typename Point_t>
struct SignedProjectionPoint
{
    Point_t projection;
    typename Point_t::value_type parameter;
    Side side;
};
#if DISTANCE_HAS_IOSTREAM
inline std::ostream& operator<<(std::ostream& os, const Side s)
{
    switch (s) {
        case Side::Left:
            return os << "Left";
        case Side::Right:
            return os << "Right";
        case Side::Interface:
            return os << "Interface";
    }
    assert(false && "unreachable");
    return os;
}
#endif


// find the 'side' of vec1 with respect to vec2 (in a right handed coordinate
// system)
template <typename Point_t>
Side halfspace(const Point_t& vec1, const Point_t& vec2)
{
    auto hs = cross(vec1, vec2);
    if (hs < 0)
        return Side::Left;
    else if (hs > 0)
        return Side::Right;
    else
        return Side::Interface;
}

// classify point other with respect to point p with tangent pT
// analogous to halfspace for an infinitisimal linesegment from around p
template <typename Point_t>
Side halfspace(const Point_t& p, const Point_t pT, const Point_t& other)
{
    /* return LineSegment(p, p+pT).halfspace(other);
     */

    return halfspace(other - p, pT);
}

namespace ls {
template <typename Point_t, typename Scalar_t = typename Point_t::value_type>
struct LineSegment
{
    Point_t p0_, p1_;

  public:
    using value_type = Scalar_t;
    LineSegment(Point_t p0, Point_t p1) : p0_(p0), p1_(p1) {}
    Point_t eval(Scalar_t t) const
    {
        return distance::lerp<Point_t>(p0_, p1_, t);
    }
    Point_t dir() const { return p1_ - p0_; }
    Point_t n() const { return orthogonal(dir()); }

    // find the 'side' of p wrt to the line segment in a right handed coordinate
    // system
    Side halfspace(const Point_t& p) const
    {
        return ::distance::halfspace(p - p0_, dir());
    }

    value_type project(const Point_t& p) const;
    value_type project(const Point_t& p, const Point_t& dir) const;
    ProjectionPoint<Point_t> closest(const Point_t& p) const;
    ProjectionPoint<Point_t> closest(const Point_t& p,
                                     const Point_t& dir) const;
    SignedProjectionPoint<Point_t> signed_closest(const Point_t& p) const;
    SignedProjectionPoint<Point_t> signed_closest(const Point_t& p,
                                                  const Point_t& dir) const;
};

template <typename Point_t, typename Scalar_t = typename Point_t::value_type>
struct ParameterizedLineSegment : public LineSegment<Point_t, Scalar_t>
{
    using value_type = typename LineSegment<Point_t, Scalar_t>::value_type;

    ParameterizedLineSegment(Point_t p0, Point_t p1, Scalar_t t0, Scalar_t t1)
        : LineSegment<Point_t, Scalar_t>(p0, p1), t0_(t0), t1_(t1)
    {
    }

    Scalar_t t0_, t1_;
    value_type evalp(Scalar_t t) const { return lerp(t0_, t1_, t); }
};

}  // namespace ls

template <typename Point_t, typename Scalar_t>
Scalar_t ls::LineSegment<Point_t, Scalar_t>::project(const Point_t& p) const
{
    // Vector projection A proj B = dot(a,b) / dot(b,b) * b
    // A = pos - l.p0
    // B = l.p1 - l.p0

    Point_t p0Pos = p - p0_;
    Point_t p0p1 = dir();
    assert(dot(p0p1, p0p1) >= 0.);
    assert(std::isfinite(dot(p0Pos, p0p1)));
    double t = dot(p0Pos, p0p1) / dot(p0p1, p0p1);
    return t;
}

template <typename Point_t, typename Scalar_t>
Scalar_t ls::LineSegment<Point_t, Scalar_t>::project(const Point_t& p,
                                                     const Point_t& dir) const
{
    //  Given point pos, vector W = (-V.y,V.x)
    //  and line segment Q(t) = lerp(p0,p1, t):
    //
    //          Dot(Q(t) - p, W) == 0
    //
    //          Dot((1-t)p0 + tp1 - p, W) == 0
    //          Dot(1-t(p0-p) + t(p1-p), W) == 0)
    //
    //
    Point_t W = orthogonal(dir);
    ls::LineSegment<Point_t> l2{p0_ - p, p1_ - p};
    double num = dot(l2.p0_, W);
    double denom = dot(l2.p0_, W) - dot(l2.p1_, W);
    double t = num / denom;
    return t;
}

template <typename Point_t, typename Scalar_t>
ProjectionPoint<Point_t> ls::LineSegment<Point_t, Scalar_t>::closest(
    const Point_t& p) const
{
    assert(isfinite(p));
    double t = clamp(project(p), 0., 1.);
    return {eval(t), t};
}

template <typename Point_t, typename Scalar_t>
ProjectionPoint<Point_t> ls::LineSegment<Point_t, Scalar_t>::closest(
    const Point_t& p, const Point_t& dir) const
{
    assert(isfinite(p));
    double t = clamp(project(p, dir), 0., 1.);
    return {eval(t), t};
}

template <typename Point_t, typename Scalar_t>
SignedProjectionPoint<Point_t>
ls::LineSegment<Point_t, Scalar_t>::signed_closest(const Point_t& p) const
{
    assert(isfinite(p));
    double t = clamp(project(p), 0., 1.);
    return {eval(t), t, halfspace(p)};
}

template <typename Point_t, typename Scalar_t>
SignedProjectionPoint<Point_t>
ls::LineSegment<Point_t, Scalar_t>::signed_closest(const Point_t& p,
                                                   const Point_t& dir) const
{
    assert(isfinite(p));
    double t = clamp(project(p, dir), 0., 1.);
    return {eval(t), t, halfspace(p)};
}

// allow check for existence of free frunctions named 'first' and 'second'
// instantiated below in the static_assert with the Point_t in question
DISTANCE_HAS_FREE_FUNC(PointT, first)
DISTANCE_HAS_FREE_FUNC(PointT, second)

template <typename Point_t>
class PolyLine {
    std::vector<Point_t> points_;
    std::vector<typename Point_t::value_type> params_;
    mutable aabb box_;

  public:
    using point_type = Point_t;
    using value_type = typename Point_t::value_type;

    bool is_closed() const
    {
        return !points_.empty() && (points_.front() == points_.back());
    }

    PolyLine(std::vector<Point_t> poly = {},
             std::vector<typename Point_t::value_type> params = {})
        : points_(std::move(poly)), params_(std::move(params))
    {
        // see macro instantiation above for the has_XXX checks
        static_assert(
            detail::has_first(Point_t{}) && detail::has_second(Point_t{}),
            "provide x/y member extractors as free functions first/second in "
            "the same ns as Point_t or in the global ns");
        if (params_.empty()) {
            int p = 0;
            int N = points_.size();
            if (N == 1)
                params_ = {0.};
            else {
                params_.reserve(N);
                std::generate_n(
                    std::back_inserter(params_), N,
                    [&p, N]() -> double { return (p++) / (N - 1.0); });
            }
        }
        assert(params_.size() == points_.size());

        if (points_.size() == 1) {
            assert(params_.front() == 0);
            assert(params_.back() == 0);
        } else if (points_.size() > 1) {
            assert(params_.front() == 0);
            assert(params_.back() == 1);
        }
    }

    const aabb& bbox() const
    {
        if (!box_.isValid() && !points_.empty()) {
            box_ = aabb::from_range(points_.begin(), points_.end());
        }
        return box_;
    }

    PolyLine extrapolated(const aabb& box)
    {
        if (is_closed() || points_.size() < 2) return *this;

        std::vector<point_type> newpts;
        newpts.reserve(points_.size() + 2);

        // test if startpoint is inside box
        // if so prepend a point on the bbox border
        const distance::Vec2d first(points_.front()[0], points_.front()[1]);
        if (box.contains(first)) {
            // 1. Find intersection plus some epsilon
            point_type extrapDir =
                -ls::LineSegment<point_type>(points_.at(0), points_.at(1))
                     .dir();  // negate direction
            auto minmax =
                box.intersection(first, {extrapDir.z, extrapDir.trace});
            point_type newpoint =
                points_.front() + extrapDir * (minmax.first > minmax.second
                                                   ? minmax.first
                                                   : minmax.second);
            newpts.push_back(newpoint);
        }

        // copy all the given points
        std::copy(points_.begin(), points_.end(), std::back_inserter(newpts));

        // test if end-point is inside box
        // if so append a point on the bbox border
        distance::Vec2d last(points_.back().z, points_.back().trace);
        if (box.contains(last)) {
            // 1. Find intersection plus some epsilon
            point_type extrapDir =
                ls::LineSegment<point_type>(points_.at(points_.size() - 2),
                                            points_.at(points_.size() - 1))
                    .dir();  // same direction
            auto minmax =
                box.intersection(last, {extrapDir.z, extrapDir.trace});
            point_type newpoint =
                points_.back() + extrapDir * (minmax.first > minmax.second
                                                  ? minmax.first
                                                  : minmax.second);
            newpts.push_back(newpoint);
        }
        return PolyLine(newpts);
    }
    // returns the closest point (with parameter) on the polyline wrt to input
    // point p
    ProjectionPoint<Point_t> closest(const Point_t& p) const
    {
        assert(isfinite(p));
        double d_tentative = std::numeric_limits<double>::max();
        double u_tentative = 0;
        Point_t p_tentative;
        for (unsigned int i = 0; i < points_.size() - 1; i++) {
            const ls::LineSegment<Point_t> l(points_.at(i), points_.at(i + 1));
            auto projection = l.closest(p);
            double d = length(projection.projection - p);
            if (d < std::abs(d_tentative)) {
                d_tentative = d;
                u_tentative =
                    lerp(params_[i], params_[i + 1], projection.parameter);
                p_tentative = projection.projection;
            }
        }
        return ProjectionPoint<Point_t>{p_tentative, u_tentative};
    }

    // returns the closest point (with parameter) along 'dir' on the polyline
    // wrt to input point p
    ProjectionPoint<Point_t> closest(const Point_t& p, const Point_t& dir) const
    {
        assert(isfinite(p));
        double d_tentative = std::numeric_limits<double>::max();
        double u_tentative = std::numeric_limits<double>::max();
        Point_t p_tentative{std::numeric_limits<double>::max(),
                            std::numeric_limits<double>::max()};
        for (unsigned int i = 0; i < points_.size() - 1; i++) {
            const ls::LineSegment<Point_t> l(points_.at(i), points_.at(i + 1));
            auto projection = l.closest(p, dir);
            double d = length(projection.projection - p);
            if (projection.parameter >= 0. && projection.parameter < 1.0 &&
                d < std::abs(d_tentative)) {
                d_tentative = d;
                u_tentative =
                    lerp(params_[i], params_[i + 1], projection.parameter);
                p_tentative = projection.projection;
            }
        }
        return ProjectionPoint<Point_t>{p_tentative, u_tentative};
    }

    // returns the closest point (with parameter) on the polyline wrt to input
    // point p
    SignedProjectionPoint<Point_t> signed_closest(const Point_t& p) const
    {
        assert(isfinite(p));
        double d_tentative = std::numeric_limits<double>::max();
        double u_tentative = 0;
        Point_t p_tentative{};
        Side side_tentative{};
        for (unsigned int i = 0; i < points_.size() - 1; i++) {
            const ls::LineSegment<Point_t> line(points_.at(i),
                                                points_.at(i + 1));
            auto projection = line.closest(p);
            assert(isfinite(projection.projection));
            Point_t vec = (projection.projection - p);

            double d = length(vec);
            if (d >= d_tentative) continue;

            // assume project on segment, overridden below
            Point_t T = line.dir();
            if (projection.parameter <= 0) {
                // projects on 'before' start of line segment
                if (i > 0) {
                    // use previous line segment to form a weighted "vertex
                    // tangent"
                    T = normalized(T) +
                        normalized(ls::LineSegment<Point_t>(points_.at(i - 1),
                                                            points_.at(i))
                                       .dir());
                } else if (is_closed()) {
                    assert(i == 0);
                    T = normalized(T) +
                        normalized(ls::LineSegment<Point_t>(
                                       points_.at(points_.size() - 2),
                                       points_.at(points_.size() - 1))
                                       .dir());
                }
                side_tentative = halfspace(p - points_.at(i), T);
            } else if (projection.parameter >= 1.) {
                // projects on 'after' line segment
                if (i < points_.size() - 2) {
                    T = normalized(T) +
                        normalized(ls::LineSegment<Point_t>(points_.at(i + 1),
                                                            points_.at(i + 2))
                                       .dir());
                } else if (is_closed()) {
                    assert(i == points_.size() - 1);
                    T = normalized(T) +
                        normalized(ls::LineSegment<Point_t>(points_.at(0),
                                                            points_.at(1))
                                       .dir());
                }
                side_tentative = halfspace(p - points_.at(i + 1), T);

            } else {
                // projects on line
                side_tentative = line.halfspace(p);
            }
            d_tentative = d;
            u_tentative =
                lerp(params_[i], params_[i + 1], projection.parameter);
            p_tentative = projection.projection;
        }

        return SignedProjectionPoint<Point_t>{p_tentative, u_tentative,
                                              side_tentative};
    }

    // returns the closest point (with parameter) along 'dir' on the polyline
    // wrt to input point p
    SignedProjectionPoint<Point_t> signed_closest(const Point_t& p,
                                                  const Point_t& dir) const
    {
        assert(isfinite(p));
        double d_tentative = std::numeric_limits<double>::max();
        double u_tentative = std::numeric_limits<double>::max();
        Point_t p_tentative{std::numeric_limits<double>::max(),
                            std::numeric_limits<double>::max()};
        Side side_tentative{};
        for (unsigned int i = 0; i < points_.size() - 1; i++) {
            const ls::LineSegment<Point_t> line(points_.at(i),
                                                points_.at(i + 1));
            auto parameter = line.project(p, dir);
            if (parameter < 0. || parameter > 1.) continue;

            Point_t proj_pos = line.eval(parameter);
            double d = length(proj_pos - p);
            if (d >= d_tentative) continue;

            side_tentative = line.halfspace(p);

            d_tentative = d;
            u_tentative = lerp(params_[i], params_[i + 1], parameter);
            p_tentative = proj_pos;
        }

        return SignedProjectionPoint<Point_t>{p_tentative, u_tentative,
                                              side_tentative};
    }

    Point_t eval(double t) const
    {
        // clamp input to interpolating region
        double tc = distance::clamp(t, 0.0, 1.0);

        // find interval
        auto it = std::upper_bound(params_.begin(), params_.end(), tc);
        if (it == params_.end()) --it;
        assert(it < params_.end());
        assert(it > params_.begin());
        double t1 = *it;
        --it;
        double t0 = *it;
        auto offset = std::distance(params_.begin(), it);

        // map to inner parameter
        double u = (tc - t0) / (t1 - t0);  // the fraction along t0-t1

        // evaluate local Bezier
        assert(offset >= 0);
        assert(!points_.empty());
        return ls::LineSegment<Point_t>(points_.at(offset),
                                        points_.at(offset + 1))
            .eval(u);
    }

    std::size_t count() const { return points_.size(); }
    const std::vector<Point_t>& points() const { return points_; }
    const std::vector<typename Point_t::value_type>& params() const
    {
        return params_;
    }
};


#if DISTANCE_HAS_IOSTREAM
template <typename V>
inline std::ostream& operator<<(std::ostream& os, const ls::LineSegment<V>& s)
{
    return os << "LineSegment(" << s.p0_ << ", " << s.p1_ << ")";
}
template <typename V>
inline std::ostream& operator<<(std::ostream& os,
                                const ls::ParameterizedLineSegment<V>& s)
{
    return os << "ParameterizedLineSegment( (" << s.p0_ << ", " << s.p1_
              << "), (" << s.t0_ << ", " << s.t1_ << ")";
}

template<typename V>
std::ostream& operator<<(std::ostream& os, const PolyLine<V>& pl)
{
    os << "PolyLine( Points( ";
    for (const auto p : pl.points()) {
        os << p << ", ";
    }
    os << ") ";
    os << "Params( ";
    for (auto d : pl.params()) {
        os << d << ", ";
    }
    os << ")";
    return os;
}

#endif


}  // namespace distance
