// -*- c++ -*-
#pragma once

#include <array>
#include <cmath>
#include "Bezier.h"
#include "DebugAssert.h"

namespace distance {

enum class bezier_type
{
    Unknown,
    Point,
    Line,
    Quadratic,
    Arch,
    Single_inflection,
    Double_inflection,
    Self_intersecting,
    Cusp,
    Loop
};


namespace classify {
inline constexpr double epsilon() { return 1e-9; }
inline bool zero(double a) { return std::abs(a) < epsilon(); }

//   inline double sign(double a)
// {
//     if (a > epsilon())
//         return +1;
//     else if (a < -epsilon())
//         return -1;
//     else
//         return 0;
// }
}

inline const char* bezier_type_name(bezier_type beztype)
{
    switch (beztype) {
        case bezier_type::Unknown:
            return "Unknown";
        case bezier_type::Line:
            return "Line";
        case bezier_type::Quadratic:
            return "Quadratic";
        case bezier_type::Arch:
            return "Arch";
        case bezier_type::Single_inflection:
            return "Single_inflection";
        case bezier_type::Double_inflection:
            return "Double_inflection";
        case bezier_type::Self_intersecting:
            return "Self_intersecting";
        case bezier_type::Cusp:
            return "Cusp";
        case bezier_type::Loop:
            return "Loop";
        case bezier_type::Point:
            return "Point";
            //default:
            // don't provide default
    }
    ASSERT(false && "unreachable");
}

struct bezier_type_inflections
{
    bezier_type type;
    double s0 = -1, s1 = -1;
    bezier_type_inflections(bezier_type intype, double t) : type(intype), s0(t)
    {
        ASSERT(type == bezier_type::Single_inflection ||
               type == bezier_type::Cusp);
        if (t < 0 || t > 1) {
            type = bezier_type::Arch;
        }
    }
    bezier_type_inflections(bezier_type intype, double t1, double t2)
        : type(intype), s0(t1), s1(t2)
    {
        ASSERT(type == bezier_type::Double_inflection ||
               type == bezier_type::Loop);
        int numSol = (t1 >= 0 && t1 <= 1) + (t2 >= 0 && t2 <= 1);
        switch (numSol) {
            case 2:
                break;  // do nothing, everything is just dandy
            case 1:
                // degrade deending on input type
	      type = (intype == bezier_type::Double_inflection)
                    ? bezier_type::Single_inflection
                    : bezier_type::Arch;  // loop requires two inflection points
                break;
            case 0:
                type = bezier_type::Arch;
                break;
        }
    }
};

template <class vec2>
inline bezier_type classify_loopblinn(const std::array<vec2, 4>& bezier)
{
    // See "Resolution Independent Curve Rendering using Programmable Graphics
    // Hardware" by Loop & Blinn 2005
    // also nicely outlined in GPU Gems 3, Chapter 25. Vector art on GPU
    const vec2& p0 = bezier[0];
    const vec2& p1 = bezier[1];
    const vec2& p2 = bezier[2];
    const vec2& p3 = bezier[3];

#if 0
    /*
      Article version
      1. Convert the bezier coefficents to monomial coeffs (0,1,2,3)
      2. Compute determinants:
        d0 = Det[3,2,1], d1 = -Det[3,2,0], d2 = Det[3,1,0], d3 = -Det[2,1,0]
     */
    // this expands to:
    double d0 = 0;
    double d1 =
        3 * (-3 * p1.y * p2x + 3 * p1.x * p2.y + 2 * p1.y * p3.x - p2.y * p3.x -
             p0.y * (p1.x - 2 * p2.x + p3.x) - 2 * p1.x * p3.y + p2.x * p3.y +
             p0.x * (p1.y - 2 * p2.y + p3.y));
    double d2 = 3 * (-3 * p1.y * p2.x + 3 * p1.x * p2.y + p1.y * p3.x -
                     p0.y * (2 * p1.x - 3 * p2.x + p3.x) - p1.x * p3.y +
                     p0.x * (2 * p1.y - 3 * p2.y + p3.y));
    double d3 = 9 * (-(p1.y * p2.x) + p0.y * (-p1.x + p2.x) +
                     p0.x * (p1.y - p2.y) + p1.x * p2.y);

#else
    // GPU Gems version
    // b0 = (p0.x, p0.y, 1) ...
    // double a1 = dot(b0, cross(p3, b2));
    // double a2 = dot(b1, cross(p0, b3));
    // double a3 = dot(b2, cross(b1, b0));

    double a1 =
        p0.x * (p3.y - p2.y) + p0.y * (p2.x - p3.x) + p3.x * p2.y - p3.y * p2.x;

    double a2 =
        p1.x * (p0.y - p3.y) + p1.y * (p3.x - p0.x) + p0.x * p3.y - p0.y * p3.x;

    double a3 =
        p2.x * (p1.y - p0.y) + p2.y * (p0.x - p1.x) + p1.x * p0.y - p1.y * p0.x;

    double d3 = 3 * a3;
    double d2 = d3 - a2;
    double d1 = d2 - a2 + a1;
/* These "d"s (The GPU Gems version) are related to the article version d's like
   so:
   3*3*a3 == - article-d1
   3*(3*a3 - a2) == - article-d2
   3*((3*a3 - a2) - a2 + a1) == - article-d1
 */
#endif

// Ii(t,s) = s(-3d1 t^2 + 3d2 ts - d3s^2)
// discr(I) = d1^2(3d2^2 - 4d3d1)

#if 0
    // normalize size of ds
    double scale =
        1.0 / std::max(std::max(std::abs(d1), std::abs(d2)), std::abs(d3));
    d1 *= scale;
    d2 *= scale;
    d3 *= scale;
#endif

    if (classify::zero(d1)) {
        return classify::zero(d2)
                   ? (classify::zero(d3) ? bezier_type::Line
                                         : bezier_type::Arch)
                   : bezier_type_inflections(bezier_type::Single_inflection,
                                             d3 / (3 * d2))
                         .type;  // might degrade to arch
    }
    // double discr = d1*d1*( 3* d2*d2  - 4*d1*d3);
    // sign is independent on d1*d1, strip
    double d = 3 * d2 * d2 - 4 * d1 * d3;

    if (classify::zero(d))
        return bezier_type_inflections(bezier_type::Cusp, d2 / (2 * d1)).type;
    double f1 = d > 0 ? std::sqrt(d / 3) : std::sqrt(-d);
    double f2 = 2 * d1;
    return bezier_type_inflections(
               d > 0 ? bezier_type::Double_inflection : bezier_type::Loop,
               (d2 + f1) / f2, (d2 - f1) / f2)
        .type;
}


}  // namespace distance
