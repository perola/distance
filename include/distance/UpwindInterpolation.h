// -*- c++ -*-
#pragma once

#include <algorithm>  // for max, min
#include <cmath>      // for sqrt
#include <utility>    // for make_pair, pair

#include "DebugAssert.h"  // for ASSERT, DebugAssert
#include "lerp.h"         // for lerp

namespace distance {

/** Compute geodesic distance for a vertex over a triangle.
 *
 * The minimal distance (Di) for vertex Vi inside a triangle {Vi,
 * Vj, Vk} is computed. The triangle is ordered clockwise and has
 * inter-vertex distances Dij, Djk, and Dik. Current distance values
 * at vertices Vj and Vk are Dj and Dk
 *
 * We use a local coordinate system to simplify solution: Vj is
 * located at 0,0 and Vk is located at 0,Djk; Vi is implicitly
 * placed using distances to Vj and Vk.
 *
 * Numerical issues:
 *  1. if Djk is small the division might enlarge error (not an
 *     issue on a grid)
 *  2. square root of negative number -> NaN. clamp to zero for
 *     (beyond) border case
 */
inline double upwind_interpolate(double dij, double djk, double dik, double dj,
                                 double dk)

{
    //    mexPrintf("%f %f %f, %.3g %.3g -> %.4g\n", v.Dij, v.Djk, v.Dik, v.Dj,
    //    v.Dk);

    // Find normal vector of an approximated linear wave front, N =
    // nx,ny.
    // Imagine a triangle { Vj Vk Vs} where Vs is (Dk-Dj) units along
    // the projected line from Vk towards the wave front. If (Dk < Dj)
    // Vs will end up 'below' the line VjVk.
    // The triangle {Vj, Vk, Vs} has hypotenuse Djk and a right angle
    // at Vs. Now scale this triangle by by dividing by Djk.
    // The line Vk-Vs is parallel to N, and due to symmetry, N can be
    // represented by the catheti lengths of the triangle.
    const double nx = (dk - dj) / djk;  // the distance from Vk to Vs, 'signed'
    // the other catheti, distance from Vj to Vs, is found with the
    // Pythagorean theorem
    const double ny =
        -std::sqrt(std::max<double>(0, 1 - nx * nx));  // Djk/Djk=1
    // Clamp to zero to avoid Domain error yielding NaN, this would be
    // a numerical error (breaking triangle inequality)

    // find coordinates of Vi
    // use the derivation of the law of cosines to find the x- coord
    const double Vix = (djk * djk + dij * dij - dik * dik) / (2 * djk);
    const double Viy = -std::sqrt(dij * dij - Vix * Vix);

    const double di = nx * Vix + ny * Viy + dj;

    // Test convexity of solution
    // i.e. if the line (Vi, ViP) crosses (Vj, Vk)
    // by dotting v = {-ny, nx} with Vi, Vj, and Vk
    const double tj = 0;          //-ny*0 + nx*0;
    const double tk = -ny * djk;  // y-component of Vk is zero
    const double ti = -ny * Vix + nx * Viy;
    if (tj <= ti && ti <= tk) {
        return di;
    }

    // Dijkstra version
    return std::min(dij + dj, dik + dk);
}

// Also interpolate an auxiliary value defined at grid points
template <typename T>
inline std::pair<double, T> upwind_dijkstra(double dij, double dik, double dj,
                                            double dk, T aj, T ak)
{
    // Dijkstra version
    if (dij + dj < dik + dk)
        return std::make_pair(dij + dj, aj);
    else
        return std::make_pair(dik + dk, ak);
}

// Also interpolate an auxiliary value defined at grid points
template <typename T>
inline std::pair<double, T> upwind_interpolate(double dij, double djk,
                                               double dik, double dj, double dk,
                                               T aj, T ak)
{
    ASSERT(dij > 0 && djk > 0 && dik > 0 && dj >= 0 && dk >= 0, "invariants");
    double nx = (dk - dj) / djk;
    double ny = -std::sqrt(std::max(0.0, 1 - nx * nx));

    double vix = (djk * djk + dij * dij - dik * dik) / (2 * djk);
    ASSERT(dij * dij - vix * vix > 0., "invariant");
    double viy = -std::sqrt(dij * dij - vix * vix);

    double di = nx * vix + ny * viy + dj;

    // {-ny,nx} . vj/vk/vi
    const double tj = 0;          //-ny*0 + nx*0;
    const double tk = -ny * djk;  // y-component of Vk is zero
    const double ti = -ny * vix + nx * viy;
    if (tj <= ti && ti <= tk) {
      const double tju = (ti-tj)/(tj+tk);
      return std::make_pair(di, lerp(aj, ak, tju));
    }

    return upwind_dijkstra(dij, dik, dj, dk, aj, ak);
}
}  // namespace distance
