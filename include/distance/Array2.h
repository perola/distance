// -*- c++ -*-
#pragma once

#include <algorithm>  // for copy
#include <cassert>
#include <cstddef>
#include <functional>
#include <vector>  // manages data

#include "Index2.h"

namespace distance {

template <typename T>
struct Array2
{
    using value_type = T;
    using size_type = std::size_t;

    size_type dim1_ = 0;
    size_type dim2_ = 0;
    //  size_type dim12_;
    // data is stored column major to interface with matlab
    std::vector<T> data_;

  public:
    const T* data() const { return data_.data(); }
    T* data() { return data_.data(); }
    bool bound(const Index2& coord) const
    {
        return coord.i >= 0 && static_cast<size_type>(coord.i) < dim1_ &&
               coord.j >= 0 && static_cast<size_type>(coord.j) < dim2_;
    }
    size_type dim1() const { return dim1_; }
    size_type dim2() const { return dim2_; }
    size_type count() const { return dim1_ * dim2_; }
    //  const size_type * dims() const { return &dim1_; } // for matlab

    typename std::vector<T>::iterator begin() { return std::begin(data_); }
    typename std::vector<T>::const_iterator begin() const
    {
        return data_.cbegin();
    }
    typename std::vector<T>::iterator end() { return std::end(data_); }
    typename std::vector<T>::const_iterator end() const { return data_.cend(); }
    /*
    Array2(Array2&&) = default;
    Array2(const Array2&) = default;
    Array2& operator=(const Array2&) = default;
    Array2& operator=(Array2&&) = default;
    */
    Array2() = default;
    Array2(size_type dim1, size_type dim2, std::vector<T> data)
        : dim1_(dim1), dim2_(dim2), data_(std::move(data))
    {
        assert(dim1_ * dim2_ == data_.size());
    }
    Array2(size_type dim1, size_type dim2, const T* data = nullptr)
        : dim1_(dim1), dim2_(dim2), data_(dim1 * dim2)
    {
        if (data != nullptr) {
            std::copy(data, data + (dim1 * dim2), begin());
        }
    }

    size_type coord2li(size_type i, size_type j) const
    {
        // Matlab column major order
        // d1 X d2, at (i,j) -> linear index
        // (j-1) * d1 + i
        return j * dim1_ + i;
    }
    size_type coord2li(Index2 c) const { return coord2li(c.i, c.j); }
    Index2 li2coord(size_type li) const
    {
        // nb col major
        size_type j = li / dim1_;
        size_type i = li % dim1_;
        return Index2(i, j);
    }
    T& operator()(size_type i, size_type j) { return data_[coord2li(i, j)]; }
    const T& operator()(size_type i, size_type j) const
    {
        return data_[coord2li(i, j)];
    }
    T& operator()(const Index2& ind) { return data_[coord2li(ind.i, ind.j)]; }
    const T& operator()(const Index2& ind) const
    {
        return data_[coord2li(ind.i, ind.j)];
    }
    T& operator()(size_type li) { return data_[li]; }
    const T& operator()(size_type li) const { return data_[li]; }
    Array2 operator-()
    {
        Array2 result(dim1(), dim2());
        std::transform(begin(), end(), result.begin(), std::negate<T>());
        return result;
    }
};

}  // namespace distance
