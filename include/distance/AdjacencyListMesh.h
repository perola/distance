//===-- include/AdjacencyListMesh.h - A mesh data structure -----*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#pragma once

#include <deque>   // for _Deque_iterator, deque, operator-
#include <memory>  // for allocator_traits<>::value_type, unique...
#if DISTANCE_HAS_IOSTREAM
#include <iterator>  // for istream_iterator
#include <sstream>   // for basic_istream, istringstream, istream
#endif
#include <stdexcept>  // for runtime_error
#include <string>     // for operator+, string, to_string, getline
#include <utility>    // for pair, make_pair, move
#include <vector>     // for vector

namespace distance {

struct Triangle
{
    int V1, V2, V3;
    Triangle(int v1, int v2, int v3) : V1(v1), V2(v2), V3(v3) {}
    bool operator==(const Triangle& t) const
    {
        return V1 == t.V1 && V2 == t.V2 && V3 == t.V3;
    }
    static Triangle read_obj_tri(std::istream& is)
    {
        int ids[3];
        for (auto& i : ids) {
            is >> i;
            if (!is) throw std::runtime_error("ReadTri failure");
        }
        return Triangle{ids[0] - 1, ids[1] - 1, ids[2] - 1};
    }
};

inline std::deque<std::vector<int>> findNeighbors(
    const std::vector<Triangle>& tris, int numvertices);

template <typename VecT>
class AdjacencyListMesh {
  public:
    using AdjacencyList = std::vector<int>;

    //! construct a mesh from a set of vertices and triangles
    template <typename VIterator, typename MIterator>
    AdjacencyListMesh(VIterator verts_begin, VIterator verts_end,
                      MIterator tris_begin, MIterator tris_end)
        : vertices_(verts_begin, verts_end),
          triangles_(tris_begin, tris_end),
          adjacencyLists_(findNeighbors(triangles_, vertices_.size()))
    {
    }

    //! construct a mesh from a set of vertices and triangles
    AdjacencyListMesh(std::vector<VecT> verts, std::vector<Triangle> tris)
        : vertices_(verts),
          triangles_(tris),
          adjacencyLists_(findNeighbors(triangles_, vertices_.size()))
    {
    }
#if DISTANCE_HAS_IOSTREAM
    static std::unique_ptr<AdjacencyListMesh> readOBJ(std::istream& is);
#endif

    //! data is immutable
    const std::vector<VecT>& getVertices() const { return vertices_; }
    const std::vector<Triangle>& getTriangles() const { return triangles_; }
    const std::deque<AdjacencyList>& getAdjacencyLists() const
    {
        return adjacencyLists_;
    }

  private:
    std::vector<VecT> vertices_;
    std::vector<Triangle> triangles_;
    std::deque<AdjacencyList> adjacencyLists_;
};

inline std::deque<int> topologicalSort(std::deque<std::pair<int, int>>& edges)
{
    if (!edges.size()) return std::deque<int>();

    std::deque<int> ring;
    // insert first element
    ring.push_back(edges.back().first);
    ring.push_back(edges.back().second);
    edges.pop_back();

    // probably better to have a tag list (bool[edges.size()]) with merged edges
    // to avoid erasing in loop. size should be small anyway
    while (edges.size()) {
        unsigned int k = 0;
        for (; k < edges.size(); k++) {
            if (edges.at(k).first == ring.back()) {
                ring.push_back(edges.at(k).second);
                break;
            }
            if (edges.at(k).second == ring.front()) {
                ring.push_front(edges.at(k).first);
                break;
            }
        }
        if (k >= edges.size())  // found no match
            throw std::runtime_error(
                "AdjacencyListMesh: Non-manifold input to topologicalSort.");
        edges.erase(edges.begin() + k);
    }
    return ring;
}

///  \brief Compute adjacency lists or 'one-rings' for each vertex in the
///  triangle list
///
///  Two cases are distinguished:
///    - A vertex is interior, then the first and last vertex of the
///  one-ring are the same (duplicate)
///   - A vertex is on the boundary, then the first and last vertex
///  are different

std::deque<std::vector<int>> findNeighbors(const std::vector<Triangle>& tris,
                                           int size)
{
    // 1. Build a map of all tris that share each vertex using a
    // vector<vector> for (vert -> tris)
    std::deque<std::vector<int>> vert2tri(size);
    // 2. For each vertex 'current'
    //    2.a Collect all neighboring tris from the vector<vector>:
    //    vert2tri.at(i)
    //    2.b For each of the tris, take the three constituating
    //    vertices, cull 'current' and push a directed
    //    edge of the two other on a temporary edge data structure
    //    'edges'
    // 3. Pick a starting edge from 'edges' (any one will do)
    // 4. Add the end edge vertices (first, second) to the new
    //    sorted set ('ring')
    // 5. While 'edges' is not empty
    //    5.a Loop around 'edges'
    //    5.b Try to fit the current edge at either start or end of
    //    'ring'
    //    5.c If it fits, add it to 'ring' and remove it from 'edges'
    std::deque<std::vector<int>> adjacencyList(size);
    // 6. Push the one-ring on the adjacencyList data structure
    //    (linearly indexed)

    // make map
    for (unsigned int i = 0; i < tris.size(); i++) {
        const Triangle& tri = tris.at(i);
        vert2tri.at(tri.V1).push_back(i);
        vert2tri.at(tri.V2).push_back(i);
        vert2tri.at(tri.V3).push_back(i);
    }

    // for each vertex ...
    for (int i = 0; i < size; i++) {
        const std::vector<int>& unsorted = vert2tri.at(i);
        if (unsorted.empty()) continue;
        using Edge = std::pair<int, int>;
        std::deque<Edge> edges;
        for (unsigned int j = 0; j < unsorted.size(); j++) {
            const Triangle& t = tris.at(unsorted.at(j));
            if (t.V1 == i) {
                edges.push_back(std::make_pair(t.V2, t.V3));
            } else if (t.V2 == i) {
                edges.push_back(std::make_pair(t.V3, t.V1));
            } else if (t.V3 == i) {
                edges.push_back(std::make_pair(t.V1, t.V2));
            }
        }

        // Now 'edges' make up the one-ring, but it needs to be
        // sorted and culled for duplicates
        std::deque<int> ring = topologicalSort(edges);
        if (ring.empty())
            throw std::runtime_error("AdjacencyListMesh: Non-manifold input.");
        // memory is pre-allocated, as length is known at init time
        adjacencyList.at(i) = std::vector<int>(ring.begin(), ring.end());
    }
    return adjacencyList;
}

#if DISTANCE_HAS_IOSTREAM

namespace detail {
template <class T>
constexpr auto has_read_obj_vert(T* obj)
    -> decltype(read_obj_vert(std::cin, *obj), std::true_type{})
{
    return std::true_type{};
}
constexpr auto has_read_obj_vert(...) -> std::false_type
{
    return std::false_type{};
}
}  // namespace detail
template <typename VecT>
std::unique_ptr<AdjacencyListMesh<VecT>> AdjacencyListMesh<VecT>::readOBJ(
    std::istream& is)
{
    // tries to read a mesh from an OBJ stream
    // only reads simplest triangle faces, ie 'f 1 2 3',
    // no slashes, no quads or larger polygons
    // consult http://en.wikipedia.org/wiki/Wavefront_.obj_file

    std::vector<VecT> verts;
    std::vector<Triangle> tris;

    std::string line;
    int lineNo = -1;
    while (std::getline(is, line)) {
        ++lineNo;
        std::istringstream iss(line);
        std::string type;
        if (iss >> type)  // eat first word
        {
            if (type.at(0) == '#')  // comment
                continue;
            else if (type.at(0) == 'v' && type.size() == 1)  // vertex
            {
                VecT v;
                static_assert(detail::has_read_obj_vert(&v),
                              "requires a free function: 'VecT "
                              "read_obj_vert(std::istream&, VecT&)' in the "
                              "same ns as VecT");
                verts.push_back(read_obj_vert(iss, v));
            } else if (type.at(0) == 'f' && type.size() == 1)  // face
            {
                tris.push_back(
                    Triangle::read_obj_tri(iss));  // reader handles one-offset
            }
        }
    }
    return std::make_unique<AdjacencyListMesh<VecT>>(std::move(verts),
                                                     std::move(tris));
}
#endif
}  // namespace distance
