// -*- c++ -*-
#pragma once

#include <type_traits>

namespace distance {

// since c++17 so remove at some point
template <class T>
constexpr const T& clamp(const T& t, const T& lower, const T& upper)
{
    // will fail for types that don't have operator less
    return t < lower ? lower : t > upper ? upper : t;
}

// clamp to unit interval (only generated for standard numeric types)
template <class T>
constexpr std::enable_if_t<std::is_arithmetic<T>::value, T> clampu(const T& t)
{
    return clamp<T>(t, T(0), T(1));
}

}  // namespace distance
