// -*- c++ -*-
#pragma once

#include <stdio.h>  // for printf
#include <array>    // for array

namespace distance {

template <typename C>
struct afd
{
    std::array<C, 4> B;
    double t = 0;
    double dt = 1;

    template <typename C2>
    static std::array<C2, 4> to_bezier(std::array<C2, 4> afd_basis)
    {
        /*
          {e,
          1/18 (18 e +  6 f - 3 g + 2 h),
          1/18 (18 e + 12 f - 3 g + h),
          e + f}
        */
        auto h = afd_basis[0];
        auto g = afd_basis[1];
        auto f = afd_basis[2];
        auto e = afd_basis[3];
        std::array<C2, 4> bezier = {
            {e, 1. / 18 * (18 * e + 6 * f - 3 * g + 2 * h),
             1. / 18 * (18 * e + 12 * f - 3 * g + h), e + f}};
        return bezier;
    }
    std::array<C, 4> to_bezier() const { return to_bezier(B); }
    afd(std::array<C, 4> bezier)
    {
        /* Incoming curve is in the Bezier basis:
         *
         * a0(1-t)^3 + 3b0(1-t)^2t + 3c0(1-t)t^2 + d0(t)^3
         *
         */

        /* make tranformation into faster basis
         *
         * a1 + b1t + c1*1/2*t(1-t) + d1*1/6*t(t-1)(t-2)
         * {a, -a + d, 6 (b - 2 c + d), -6 (a - 3 b + 3 c - d)}
         *
         */
        auto a = bezier[0];
        auto b = bezier[1];
        auto c = bezier[2];
        auto d = bezier[3];

        B[3] = a;
        B[2] = -a + d;
        B[1] = 6 * (b - 2 * c + d);
        B[0] = -6 * (a - 3 * b + 3 * c - d);
    }
    C curr() const { return B[3]; }
    bool hasNext() { return t < 1; }
    C next()
    {
        E();
        return curr();
    }

    /** Refines the Bezier by shifting it into the interval (.5, 1)
     */
    afd L()
    {
        // B[3] = B[3];
        B[2] = B[2] / 2 - B[1] / 8 + B[0] / 16;
        B[1] = B[1] / 4 - B[0] / 8;
        B[0] = B[0] / 8;
        dt /= 2;  // half step size
        return *this;
    }
    /** Coarsen the Bezier by shifting into the interval (0, 2)
     */
    void l()
    {
        // the inverse of L
        // B[3] = B[3];
        B[2] = 2 * B[2] + B[1];
        B[1] = 4 * B[1] + 4 * B[0];
        B[0] = 8 * B[0];
        dt *= 2;
    }
    /** 'Increments' the Bezier by shifting the interval to (1, 2)
     */
    afd& E()
    {
        // no re-ordering allowed
        B[3] = B[3] + B[2];
        B[2] = B[2] + B[1];
        B[1] = B[1] + B[0];
        //      B[0] += 0;
        t += dt;
        return *this;
    }
};

#if 0
namespace inner {
template <>
struct LengthConcept<Vec2d>
{
    static double length(const Vec2d& d) { return d.length(); }
};
}  // namespace inner
#endif

template <typename C>
void printf(const std::array<C, 4>& a, const char* pre = "")
{
#ifdef mex
    mexPrintf("%s(%.3g %.3g) (%.3g %.3g) (%.3g %.3g) (%.3g %.3g)\n", pre,
              a[0].x, a[0].y, a[1].x, a[1].y, a[2].x, a[2].y, a[3].x, a[3].y);
#else
    ::printf("%s{%.3g, %.3g},\n", pre, a[3].x, a[3].y);
#endif
}

template <typename C>
void printf(const afd<C>& a, const char* pre = "")
{
#ifdef mex
    mexPrintf(
        "%s(%.3g %.3g) (%.3g %.3g) (%.3g %.3g) (%.3g %.3g) : (t: %.3g, dt: "
        "%.3g)\n",
        pre, a.B[0].x, a.B[0].y, a.B[1].x, a.B[1].y, a.B[2].x, a.B[2].y,
        a.B[3].x, a.B[3].y, a.t, a.dt);
#else
    ::printf("%s{%.3g, %.3g},\n", pre, a.B[3].x, a.B[3].y);
#endif
}

template <typename C>
struct variable_afd : public afd<C>
{
    static double LENGTH(const C& v)
    {
        constexpr const int t = 0;
        switch (t) {
            case 0:
                return v.manhattan();
            case 1:
                return v.length2();
            case 2:
                return v.length();
        }
    }
    variable_afd(std::array<C, 4> bz) : afd<C>(std::move(bz))
    {
        printf(*this, "s: ");
    }
    void refine()
    {
        // first, try to increase step size
        while (afd<C>::dt < .5 &&
               /*B[1].length2() < 1 &&*/ LENGTH(afd<C>::B[2]) < 0.25) {
            // printf("+%.2g ", B[2].length2());
            afd<C>::l();
        }

        // then, restrict if necessary
        // third derivative
        while (LENGTH(afd<C>::B[0]) > 1) {
            //	printf("|%.2g(%.2g)", B[1].length2(), B[0].length2());
            afd<C>::L();
        }

        // second derivative
        while (LENGTH(afd<C>::B[1]) > 1) {
            //	printf("=%.2g(%.2g)", B[2].length2(), B[1].length2());
            afd<C>::L();
        }

        // first derivative
        while (LENGTH(afd<C>::B[2]) > .95) {
            //	printf("-%.2g", B[2].length2());
            afd<C>::L();
        }
    }
};

template <typename C>
struct fixed_afd : public afd<C>
{
    static double LENGTH(const C& v)
    {
        constexpr const int t = 0;
        switch (t) {
            case 0:
                return v.manhattan();
            case 1:
                return v.length2();
            case 2:
                return v.length();
        }
    }

    fixed_afd(std::array<C, 4> bz) : afd<C>(bz)
    {
        double lengthControlPoly = LENGTH(bz[1] - bz[0]) +
                                   LENGTH(bz[2] - bz[1]) +
                                   LENGTH(bz[3] - bz[2]);
        unsigned int power = 1, count = 1;

        afd<C>::L();  // force first subdivision
        while (power < (lengthControlPoly * 1.5)) {
            power *= 2;
            count++;
            afd<C>::L();
        }

        // printf("maxl: %d\n", intLength);
        ::printf("maxl: %d\n", power);
        ::printf("maxl: %d\n", count);
    }
    void refine() {}
    C next()
    {
        afd<C>::E();
        // printf("step\t%.4g\n", B[2].length());
        return afd<C>::curr();
    }
};
}  // namespace distance
