// -*- c++ -*-
#pragma once

#include <assert.h>               // for assert
#include <stddef.h>               // for size_t
#include <algorithm>              // for max, min, min_element, transform
#include <cmath>                  // for fabs, isnan, sqrt
#include <deque>                  // for deque, _Deque_iterator, operator!=
#include <limits>                 // for numeric_limits
#include <stdexcept>              // for runtime_error
#include "Array2.h"               // for Array2
#include "Index2.h"               // for Index2
#include "UpwindInterpolation.h"  // for upwind_interpolate

namespace distance {

  // c++11 version has constexpr
  template <typename T, size_t N>
  constexpr size_t countof(T (&)[N]) {
    return N;
  }

  struct TSet {
  TSet(double dij, double djk, double dik, double dj, double dk)
      : Dij(dij), Djk(djk), Dik(dik), Dj(dj), Dk(dk) {}
  double Dij, Djk, Dik; // inter-vertex distances
  double Dj, Dk;        // vertex values
};

class FastMarching {

  enum { INITIAL, DONE, NEAR, FAR };

  Array2<double> &dgrid;
  Array2<int> tags;
  double dx, dy, hyp;
  double epsilon;

  std::deque<Index2> list, list2;

public:
  FastMarching(Array2<double> &grid, double dx, double dy)
      : dgrid(grid), dx(dx), dy(dy), hyp(sqrt(dx * dx + dy * dy)),
        epsilon(1e-9) {
    if (dx <= 0 || dy <= 0)
      throw(std::runtime_error(
          "FastMarching: negative or zero delta prohibited"));
    if (std::max(dx / dy, dy / dx) > 100.0)
      throw(std::runtime_error(
          "FastMarching: dx-dy ratio required to be below 100."));
    if (dgrid.dim1() < 2 || dgrid.dim2() < 2)
      throw(std::runtime_error("FastMarching: singleton dimensions prohibited. "
                               "need at least 2x2 input"));
  }

  void initialize() {
    std::deque<unsigned int> initializers;
    tags = Array2<int>(dgrid.dim1(), dgrid.dim2());
    for (unsigned int li = 0; li < dgrid.dim1() * dgrid.dim2(); ++li) {
      if (dgrid(li) < 0)
        throw(std::runtime_error(
            "FastMarching::initialize: negative initializers prohibited"));
      if (std::isnan(dgrid(li))) {
        tags(li) = FAR;
        dgrid(li) = std::numeric_limits<double>::max();
      } else {
        // don't touch numerical values, assumed to be correctly input
        tags(li) = INITIAL;
        initializers.push_back(li);
        //std::cout << li << ": " << dgrid(li) << std::endl;
      }
    }

    // bootstrap algorithm
    for (std::deque<unsigned int>::const_iterator it = initializers.begin();
         it != initializers.end(); ++it)
      updateNeighbors(dgrid.li2coord(*it));
    std::swap(list, list2); // make sure list is prepped
  }

  void march() {
    //int i = 0;
    while (list.size()) {
      //std::cout << i++ << ": " << list.size() << std::endl;
      for (std::deque<Index2>::const_iterator it = list.begin();
           it != list.end(); ++it)
        updateCoord(*it);

      list.clear();

      // swap lists
      std::swap(list, list2);
    }
  }

  void updateCoord(const Index2 &ind) {
    double oldVal = dgrid(ind.i, ind.j);
    double newVal = recompute(ind);

    newVal = std::min(newVal, oldVal);

    if (tags(ind.i, ind.j) != INITIAL) // don't touch INITIAL values
      dgrid(ind.i, ind.j) = newVal;

    if (fabs(oldVal - newVal) < epsilon) {
      updateNeighbors(ind);
      // indicate solved (at least temporary)
      tags(ind.i, ind.j) = DONE;
    } else {
      // keep working on this one
      list2.push_back(ind);
    }
  }

  bool bound(const Index2 &coord) const { return dgrid.bound(coord); }

  void updateNeighbors(const Index2 &ind) {
    // Todo: what nb-set to use
    std::deque<Index2> nbs;
    if (bound(ind.s()))
      nbs.push_back(ind.s());
    // if(bound(ind.sw()))
    //   nbs.push_back(ind.sw());
    if (bound(ind.w()))
      nbs.push_back(ind.w());
    // if(bound(ind.nw()))
    //   nbs.push_back(ind.nw());
    if (bound(ind.n()))
      nbs.push_back(ind.n());
    // if(bound(ind.ne()))
    //   nbs.push_back(ind.ne());
    if (bound(ind.e()))
      nbs.push_back(ind.e());
    // if(bound(ind.se()))
    //   nbs.push_back(ind.se());

    for (std::deque<Index2>::const_iterator it = nbs.begin(); it != nbs.end();
         ++it) {
      if (tags(it->i, it->j) == NEAR || tags(it->i, it->j) == INITIAL)
        continue; // already in list

      double oldVal = d(*it);
      double newVal = recompute(*it);

      if (newVal < oldVal) {
        // update value
        dgrid(it->i, it->j) = newVal;

        // add to active list
        list2.push_back(*it); // not necessary to push front
        tags(it->i, it->j) = NEAR;
      }
    }
  }

  double d(const Index2 &ind) const { return dgrid(ind.i, ind.j); }


  /**
     nw n ne
     w  i e
     sw s se
   */
  double recompute(const Index2 &ind) {
    std::deque<TSet> tsets;
    // Tset Dij, Djk, Dik, Dj, Dk
    if (bound(ind.w()) && bound(ind.nw()))
      tsets.push_back(TSet(dx, dy, hyp, d(ind.w()), d(ind.nw()))); // i-w-nw
    if (bound(ind.nw()) && bound(ind.n()))
      tsets.push_back(TSet(hyp, dx, dy, d(ind.nw()), d(ind.n()))); // i-nw-n
    if (bound(ind.n()) && bound(ind.ne()))
      tsets.push_back(TSet(dy, dx, hyp, d(ind.n()), d(ind.ne()))); // i-n-ne
    if (bound(ind.ne()) && bound(ind.e()))
      tsets.push_back(TSet(hyp, dy, dx, d(ind.ne()), d(ind.e()))); // i-ne-e
    if (bound(ind.e()) && bound(ind.se()))
      tsets.push_back(TSet(dx, dy, hyp, d(ind.e()), d(ind.se()))); // i-e-se
    if (bound(ind.se()) && bound(ind.s()))
      tsets.push_back(TSet(hyp, dx, dy, d(ind.se()), d(ind.s()))); // i-se-s
    if (bound(ind.s()) && bound(ind.sw()))
      tsets.push_back(TSet(dy, dx, hyp, d(ind.s()), d(ind.sw()))); // i-s-sw
    if (bound(ind.sw()) && bound(ind.w()))
      tsets.push_back(TSet(hyp, dy, dx, d(ind.sw()), d(ind.w()))); // i-sw-w

    assert(!tsets.empty());
    double tdists[8];
    std::transform(tsets.begin(), tsets.end(), tdists, &minDistanceLinear);

    const double *mind = std::min_element(tdists, tdists + tsets.size());
    assert(mind != tdists + tsets.size()); // range should be non-empty
    return *mind;
  }

  static double minDistanceLinear(const TSet &v) {
    return upwind_interpolate(v.Dij, v.Djk, v.Dik, v.Dj, v.Dk);
  }
};

} // namespace distance
