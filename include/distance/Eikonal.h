// -*- c++ -*-
#pragma once

#include <cmath>
#include <algorithm>

namespace distance {

// ------------------------------------------------------------
// ------------------------------------------------------------
// Solves the quadratic equation for the 2D case
//  Precondition: |a| <= |b|, speed == 1, dx == dy = 1, sign = +-1
//
inline double QuadraticEikonal2D(double a, double b, double sign)
{
    // Test first solution
    // (x - a)^2 = 1/speed^2
    // ie the solution is only dependent on one of the neighboring cells
    double update = a + sign;
    if (std::abs(update) <= std::abs(b)) {
        return update;
    }

    // Test next solution
    // (x-a)^2 + (x-b)^2 = 1
    // solution dependent on two of the neighboring cells
    return 0.5 * (a+b + sign*std::sqrt(std::max(0.0, 2 - (a-b)*(a-b))));
}

inline double QuadraticEikonal2D(double xn, double yn, double dx, double dy,
                          double sign)
{
    // Test first solution(s)
    // ((x - xn)/dx)^2 = 1 or ((x - yn)/dy)^2 = 1
    // ie the solution is only dependent on one of the neighboring cells
    double updatex = xn + sign * dx;
    double updatey = yn + sign * dy;
    if (std::abs(updatex) <= std::abs(yn)) {
        return updatex;
    }
    if (std::abs(updatey) <= std::abs(xn)) {
        return updatey;
    }

    // Test next solution
    // ((x-xn)/dx)^2 + ((x-yn)/dy)^2 = 1
    // solution dependent on two of the neighboring cells
#if 0
    const double dx2 = dx*dx;
    const double dy2 = dy*dy;
    return (xn/dx2 + yn/dy2 +
            sign *
            std::sqrt(std::max(0.0, dx2 + dy2 - (xn-yn)*(xn-yn))) / (dx*dy) ) /
        (1/dx2 + 1/dy2);
#else
    // FullSimplify gives, better?
    const double dx2 = dx*dx;
    const double dy2 = dy*dy;
    return (dy2*xn + dx2*yn +
            sign * dx*dy *
            std::sqrt(std::max(0.0, dx2 + dy2 - (xn-yn)*(xn-yn)))) /
        (dx2 + dy2);
#endif
}




}
