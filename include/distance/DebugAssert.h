// c++
#pragma once

#ifdef NDEBUG
//
// optimized (no debug) path
//
#define ASSERT(...) ((void)0)

#else  // #ifdef NDEBUG
#if !(DISTANCE_HAS_IOSTREAM)
#include <cassert>
#define EXPAND_MSVC_BUG_HELPER(x) x
// expands to the first argument: https://stackoverflow.com/a/11172679
#define FIRST_HELPER(first, ...) first
#define FIRST(...) EXPAND_MSVC_BUG_HELPER(FIRST_HELPER(__VA_ARGS__, throwaway))
#define ASSERT(...) assert(FIRST(__VA_ARGS__))
#else // DISTANCE_HAS_IOSTREAM
#include <algorithm>  // for forward
#include <cstdlib>    // for abort
#include <ostream>    // for operator<<, endl, basic_ostream, ostream, basic...
#include <sstream>
#include "Trace.h"    // for concatenate

namespace distance{
struct DebugAssert
{
    std::ostream& os;
    DebugAssert(std::ostream& s, const char* expr, const char* file, int line)
        : os(s)
    {
        os << file << "::" << line << std::endl
           << "  Expression failed: '" << expr << "'" << std::endl;
    }
    ~DebugAssert() { std::abort(); }
    template <typename... Args>
    DebugAssert& message(Args&&... msg)
    {
        os << "  Message: " << concatenate(msg...) << std::endl;
        return *this;
    }

    template <typename... Args>
    DebugAssert& trace(Args&&... rest)
    {
        if (sizeof...(rest))
            os << "  Args: " << concatenate(std::forward<Args>(rest)...)
               << std::endl;
        return *this;
    }
    template <typename Expr, typename... Rest>
    DebugAssert& expr_message_trace(Expr&&, Rest&&... rest)
    {
        // skip/peel first variadic part of __VA_ARGS__, ie the expr
        return message_trace(std::forward<Rest>(rest)...);
    }
    DebugAssert& expr_message_trace()
    {
        // handle empty ASSERT.
        // the macro doesn't support dispatching it but the error
        // message regarding missing internal function is not user
        // friendly
        return *this;
    }
    DebugAssert& message_trace()
    {
        // handle ASSERT without message or args
        return *this;
    }
    template <typename Msg, typename... Args>
    DebugAssert& message_trace(Msg&& msg, Args&&... args)
    {
        // handle message and optional args
        message(std::forward<Msg>(msg));
        trace(std::forward<Args>(args)...);
        return *this;
    }
};
}

//
//
#ifdef MATLAB_MEX_FILE

//#pragma error("not implemented")
#define ASSERT(EX, ...)							\
  if (!(EX)) {								\
    mexErrMsgIdAndTxt("MATLAB:DebugAssert", __VA_ARGS__);		\
  }

#else  // MATLAB_MEX_FILE

#define EXPAND_MSVC_BUG_HELPER(x) x
// expands to the first argument: https://stackoverflow.com/a/11172679
#define FIRST_HELPER(first, ...) first
#define FIRST(...) EXPAND_MSVC_BUG_HELPER(FIRST_HELPER(__VA_ARGS__, throwaway))
// stringify after expansion: https://stackoverflow.com/a/2653351
#define XSTR(...) STR(__VA_ARGS__)
#define STR(...) #__VA_ARGS__

// We always require an expression to assert (EXPR), but to support
// variadics ASSERT(true) as well as ASSERT(false, 'always'), EXPR is
// also present/duplicated the first part of __VA_ARGS__, otherwise
// all sorts of warnings are triggered (empty variadic arg is ill
// formed).  Use c++ variadic peeling to skip it in the
// expr_message_trace call
#define ASSERT_DISPATCH(FILE, LINE, EXPR, ...)                              \
    (void)((EXPR) ||                                                        \
           (distance::DebugAssert(std::cerr, STR(EXPR), __FILE__, __LINE__) \
                .expr_message_trace(__VA_ARGS__),                           \
            true))
// We require an expression, ie the FIRST(__VA_ARGS__), the rest is
// optional hower, to avoid arg-counting or other clumsy workarounds,
// we send __VA_ARGS__ in its entireity to ASSERT_DISPATCH, that way
// the last arg is never empty and we also don't have to do
// non-standard token pasting:
// https://en.wikipedia.org/wiki/Variadic_macro
#define ASSERT(...) \
    ASSERT_DISPATCH(__FILE__, __LINE__, FIRST(__VA_ARGS__), __VA_ARGS__)

#endif
#endif // #ifdef DISTANCE_WITHXXX_IOSTREAM
#endif  // #ifdef NDEBUG
