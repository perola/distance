// -*- c++ -*-
#pragma once

#include <algorithm>
#include <array>
#include <cstddef>
#include "Bezier.h"
#include "DebugAssert.h"
#include "lerp.h"

#if DISTANCE_HAS_IOSTREAM
#include <iostream>  // for ostream, operator<<
#endif

namespace distance {

/** A Cubic Bezier over four templated values
 */
template <typename T>
class CubicBezier {
  public:
    using value_type = T;
    CubicBezier() = default;
    CubicBezier(T c0, T c1, T c2, T c3)
    {
        coeff[0] = c0;
        coeff[1] = c1;
        coeff[2] = c2;
        coeff[3] = c3;
    }
    explicit CubicBezier(const std::array<T, 4>& ds)
        : CubicBezier(std::begin(ds), std::end(ds))
    {
    }
    template <typename ForwardIt>
    CubicBezier(ForwardIt start, ForwardIt end)
    {
        ASSERT(end > start && end - start == 4);
        std::copy(start, end, std::begin(coeff));
    }
    static constexpr std::size_t num_components() { return 4; }
    friend CubicBezier operator+(const CubicBezier& b, T t)
    {
        return CubicBezier(b.c0() + t, b.c1() + t, b.c2() + t, b.c3() + t);
    }
    friend CubicBezier operator*(const CubicBezier& b, T t)
    {
        return CubicBezier(b.c0() * t, b.c1() * t, b.c2() * t, b.c3() * t);
    }
    // evaluate the Bezier
    T eval(double t) const
    {
#if 0
        return BezierEvaluator<T, 0, 3>::eval(coeff.data(), t);
#else
        const double u = 1 - t;
        return (coeff[0] * u + coeff[1] * 3 * t) * u * u +
               (coeff[2] * 3 * u + coeff[3] * t) * t * t;  // 10 mult, 3+1 add
#endif
#if 0
        return coeff[0] * u * u * u + coeff[1] * 3 * u * u * t +
               coeff[2] * 3 * u * t * t +
               coeff[3] * t * t * t;  // 14 mult, 3+1 add

        // in Horner form
        return (coeff[1] * 3 +
                (coeff[1] * -6 + coeff[2] * 3 +
                 (coeff[1] * 3 - coeff[2] * 3 + coeff[3]) * t) *
                    t) *
                   t +
               coeff[0] * (1 + t * (-3 + (3 - t) * t));  // 11 mult, 9 add
#endif
    }
    // evaluate first derivative
    T eval1(double t) const
    {
        return BezierEvaluator<T, 1, 3>::eval(coeff.data(), t);
    }
    // eval second derivative
    T eval2(double t) const
    {
        return BezierEvaluator<T, 2, 3>::eval(coeff.data(), t);
    }
    T eval3(double t) const
    {
        return BezierEvaluator<T, 3, 3>::eval(coeff.data(), t);
    }
    // split the bezier into two
    void split(double t, CubicBezier<T>* a, CubicBezier<T>* b) const
    {
        // split the bezier using deCasteljaus algorithm
        const T c12 = lerp(coeff[0], coeff[1], t);
        const T c23 = lerp(coeff[1], coeff[2], t);
        const T c34 = lerp(coeff[2], coeff[3], t);

        const T c123 = lerp(c12, c23, t);
        const T c234 = lerp(c23, c34, t);

        const T c1234 = lerp(c123, c234, t);
        *a = CubicBezier(coeff[0], c12, c123, c1234);
        *b = CubicBezier(c1234, c234, c34, coeff[3]);
    }
    double length() const
    {
        return BezierEvaluator<T, 0, 3>::length(coeff.data());
    }
    const T& operator[](int i) { return coeff[i]; }
    const T& c0() const { return coeff[0]; }
    const T& c1() const { return coeff[1]; }
    const T& c2() const { return coeff[2]; }
    const T& c3() const { return coeff[3]; }
    const std::array<T, 4>& coeffs() const { return coeff; }

    bool friend operator==(const CubicBezier<T>& b1, const CubicBezier<T>& b2)
    {
        return b1.coeffs() == b2.coeffs();
    }
    friend bool operator!=(const CubicBezier<T>& b1, const CubicBezier<T>& b2)
    {
        return b1.coeffs() != b2.coeffs();
    }

  private:
    std::array<T, 4> coeff;
};
#if DISTANCE_HAS_IOSTREAM
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const CubicBezier<T>& b)
{
    return os << "CubicBezier(" << b.c0() << ", " << b.c1() << ", " << b.c2()
              << ", " << b.c3() << ')';
}
#endif
  
}  // namespace distance
