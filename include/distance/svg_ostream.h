// -*- c++ -*-
#ifndef svg_ostream_h
#define svg_ostream_h

/** An svg document writer
* Facilitates direct streaming of graphic primitives using svg functions.
* \todo Fix styles?
*/
template <class ostream> class svg_ostream {
  ostream &os_;

public:
  // RAII
  svg_ostream(ostream &s, float w, float h) : os_(s) { open(w, h); }
  // in order to allow streaming manipulators like std::endl without bailing out
  svg_ostream &operator<<(ostream &(*F)(ostream &)) {
    F(os_);
    return *this;
  }
  template <typename vec2>
  svg_ostream &circle(vec2 c, typename vec2::value_type r,
                      const char *style = nullptr) {
    os_ << "<circle cx=\"" << c.x << "\" cy=\"" << c.y << "\" r=\"" << r
        << "\"";
    if (style)
      os_ << " "
          << "style=\"" << style << "\"/>";
    os_ << "/>\n";
    return *this;
  }
  template <typename vec2>
  svg_ostream &line(vec2 s, vec2 e, const char *style = nullptr) {
    os_ << "<line "
        << "x1=\"" << s.x << "\" y1=\"" << s.y << "\" "
        << "x2=\"" << e.x << "\" y2=\"" << e.y << "\"";
    if (style)
      os_ << " "
          << "style=\"" << style << "\"";
    os_ << "/>\n";
    return *this;
  }

  template <typename vec2>
  svg_ostream &mc(vec2 s, vec2 c1, vec2 c2, vec2 e, bool debug = false) {
    os_ << "<path d=\"M" << s.x << ',' << s.y << " C" << c1.x << ',' << c1.y
        << ' ' << c2.x << ',' << c2.y << ' ' << e.x << ',' << e.y << "\" />\n";
    if (debug) {
      circle(s, 2).circle(e, 2).circle(c1, 2, "fill:red").circle(c2, 2,
                                                                 "fill:red");
      line(s, c1, "stroke:grey;stroke-dasharray:5,5")
          .line(c1, c2, "stroke:grey;stroke-dasharray:5,5")
          .line(c2, e, "stroke:grey;stroke-dasharray:5,5");
    }
    return *this;
  }
  ~svg_ostream() { close(); }

private:
  void open(float w, float h) {
    os_ << R"(<?xml version="1.0" standalone="no"?>)" << '\n';
    os_ << R"(<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN")" << '\n';
    os_ << R"("http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">)" << '\n';
    os_ << R"(<svg width=")" << w << R"(" height=")" << h << "\">" << '\n';
    os_ << R"(<defs>
  <style type="text/css"><![CDATA[
    path {
      fill:none;
      stroke:black;
      stroke-width:1
    }]]>
  </style>
</defs>)" << '\n';
  }
  void close() { os_ << "</svg>\n"; }
};

#endif
