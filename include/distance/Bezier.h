// -*- c++ -*-
#pragma once

#include <algorithm>  // for copy
#include <array>
#include <cstddef> // size_t

#include "Binomial.h"
#include "PartialFactorial.h"
#include "lerp.h"

namespace distance {

namespace inner {
template <typename T>
struct LengthConcept
{
    static typename T::value_type length(T /*t*/)
    {
        static_assert(sizeof(T) == -1,
                      "Need to implement a lenght concept for this type");
    }
};
template <>
struct LengthConcept<double>
{
    static double length(double d) { return d > 0 ? d : -d; }
};
}  // namespace inner

template <int V, bool pos = (V >= 0)>
struct clamp_positive
{
    static constexpr size_t value() { return 0; }
};

template <int V>
struct clamp_positive<V, true>
{
    static constexpr size_t value() { return V; }
};

/**
 * Computes Bezier value or any derivative based on template parameter input
 *  \see http://en.wikipedia.org/wiki/De_Casteljau's_algorithm
 */
template <typename T, int N, int D>
struct BezierEvaluator
{
    static_assert(D > 0, "positive degree required");
    using value_type = T;
    // start case needed to compute multiplier
    static value_type eval(const value_type* data, double t)
    {
        if (N > D) return value_type{};

        return eval_impl(data, t) *
               PartialFactorial<D, clamp_positive<D - N>::value()>::value();
    }
    /// transforms input of size D+1 into a hodograph at preallocated output of
    /// size D
    static void hodograph(const value_type* data, value_type* output)
    {
        for (std::size_t i = 0; i < D; i++) {
            output[i] = D * (data[i + 1] - data[i]);
        }
    }
    static value_type eval_impl(const value_type* data, double t)
    {
        value_type table[D + 1];
        std::copy(data, data + D + 1, table);

        for (std::size_t row = 0; row < N; row++) {
            for (std::size_t col = 0; col < D - row; col++) {
                table[col] = -table[col] + table[col + 1];
            }
        }
        for (std::size_t row = N; row < D; row++) {
            for (std::size_t col = 0; col < D - row; col++) {
                table[col] = lerp(table[col], table[col + 1], t);
            }
        }
        return table[0];
    }

    /** Approximate arc length using function evaluations,
     * "Point-based methods for estimating the length of a parametric curve" by
     * Floater and Rasmussen
     * \see http://dx.doi.org/10.1016/j.cam.2005.10.001
     */
    static double length(const value_type* data)
    {
#if 1
        value_type f0 = data[0];  // eval(0.0)
        value_type f1 = BezierEvaluator<T, N, D>::eval(data, .5);
        value_type f2 = data[D];  // eval(1.0)
        static constexpr double sqrt3 =
            1.732050807568877193176604123436845839023590087890625;
        value_type r =
            .5 * (f0 + f2) + 1.0 / 3.0 * sqrt3 * (-f0 + 2.0 * f1 - f2);
        return inner::LengthConcept<T>::length(r - f0) +
               inner::LengthConcept<T>::length(f2 - r);
#endif
        // chord length approx
        return inner::LengthConcept<T>::length(data[D] - data[0]);
    }
};

/** A generic Bezier splitter, splits in place
 *  \see http://en.wikipedia.org/wiki/De_Casteljau's_algorithm
 */
template <typename T, int D>
struct BezierSplitter
{
    using value_type = T;
    static void split(const value_type* data, double t, value_type* output)
    {
        // start fd table by copying input
        std::copy_n(data, D + 1, output);
        T* lastPtr = output + 2 * (D + 1) - 1;
        *lastPtr-- = data[D];

        for (std::size_t row = 1; row < D + 1; row++) {
            for (std::size_t col = D; col >= row; col--) {
                output[col] = lerp(output[col - 1], output[col], t);
            }
            *lastPtr-- = output[D];
        }
    }
};

template <typename T, int D>
struct BezierConverter
{
    static std::array<T, D + 1> toMonomial(const std::array<T, D + 1>& bezier)
    {
        // the monomial coefficients are the left side of the difference
        // table multiplied with the respective binomial coefficient
        std::array<T, D + 1> monomial = BinomialTable<D, T>::table;
        T table[D + 1];

        std::copy(bezier.begin(), bezier.end(), table);
        for (int row = 0; row < D + 1; row++) {
            monomial[row] *= table[0];
            for (int col = 0; col < D - row; col++) {
                table[col] = table[col + 1] - table[col];
            }
        }
        return monomial;
    }
    static std::array<T, D + 1> fromMonomial(
        const std::array<T, D + 1>& monomial)
    {
        std::array<T, D + 1> coeffs = BinomialTable<D, T>::table;
        std::array<T, D + 1> bezier;

        T table[D + 1];

        std::transform(monomial.begin(), monomial.end(), coeffs.begin(), table,
                       [](T p, T b) { return p / b; });
        for (int row = 0; row < D + 1; row++) {
            bezier[row] = table[0];
            for (int col = 0; col < D - row; col++) {
                table[col] = table[col + 1] + table[col];
            }
        }
        return bezier;
    }
};

}  // namespace distance
