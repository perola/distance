#pragma once

#include <type_traits>
#if DISTANCE_HAS_IOSTREAM
#include <iostream>
#endif

#include "Vec2d.h"  // for first/second extractors

namespace distance {

template <typename Point_t, typename T = typename Point_t::value_type,
          typename Size_t = std::size_t>
struct Lattice
{
    Point_t origo_;
    Point_t step_;
    Size_t size1_, size2_;
    Lattice(const Point_t& o, const Point_t& d, Size_t dim1, Size_t dim2)
        : origo_(o), step_(d), size1_(dim1), size2_(dim2)
    {
    }
    Lattice() = default;

    using value_type = T;
    using size_type = Size_t;
    using point_type = Point_t;

    /// transform grid coordinate to world floating point coordinate
    template <typename Scalar>
    std::enable_if_t<std::is_arithmetic<Scalar>::value, Point_t> l2w(
        Scalar i, Scalar j) const
    {
        return {i * first(step_) + first(origo_),
                j * second(step_) + second(origo_)};
    }
    /// transform grid (floating point) coordinate to world floating point
    /// coordinate
    Point_t l2w(const Point_t& p) const { return l2w(first(p), second(p)); }

    /// transform world coordinate to grid (floating point) coordinate
    template <typename Scalar>
    std::enable_if_t<std::is_arithmetic<Scalar>::value, Point_t> w2l(
        Scalar x, Scalar y) const
    {
        return {(x - first(origo_)) / first(step_),
                (y - second(origo_)) / second(step_)};
    }
    /// transform world coordinate to grid (floating point) coordinate
    Point_t w2l(const Point_t& p) const { return w2l(first(p), second(p)); }
};

#if DISTANCE_HAS_IOSTREAM
template <typename P>
std::ostream& operator<<(std::ostream& os, const Lattice<P>& l)
{
    os << "Lattice(" << l.origo_ << " -> "
       << (l.origo_ + typename Lattice<P>::point_type(l.step_[0] * l.size1_,
                                                      l.step_[1] * l.size2_))
       << ", " << l.step_ << ", (" << l.size1_ << ", " << l.size2_ << "))";
    return os;
}
#endif

}  // namespace distance
