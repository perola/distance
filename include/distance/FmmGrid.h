// -*- c++ -*-
#pragma once

#include "Array2.h"
#include "DistanceTags.h"
#include "UpwindInterpolation.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>
#include <mutex>
#include <vector>
#include <deque>

#ifdef MATLAB_MEX_FILE
#pragma push_macro("assert")
#ifndef NDEBUG
#undef assert
#define assert(x, txt) \
    if (!(x)) mexErrMsgTxt(#txt)
#endif
#endif

namespace distance {

template <typename T>
class FmmGrid {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<T> auxGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx_, dy_, hyp_;

    std::deque<Index2> workList_, nextWorkList_;
    std::mutex mutex_;

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<T>, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
        double dy)
    {
        FmmGrid pos(begin, end, sx, sy, dx, dy);
        pos.run();

        FmmGrid neg(-pos.distanceGrid_, pos.auxGrid_, pos.stateGrid_,
                    pos.updateGrid_, dx, dy);
        neg.run();

        Array2d d = pos.distanceGrid_;
        for (int li = 0, liend = d.count(); li < liend; ++li) {
            if (std::abs(neg.distanceGrid_(li)) < std::abs(d(li)))
                d(li) = -neg.d(li);
        }
        return std::make_tuple(d, neg.auxGrid_, neg.stateGrid_,
                               neg.updateGrid_);
    }

    // ------------------------------------------------------------
    FmmGrid(Array2d d, Array2<T> a, Array2<State> s, Array2<int> u, double dx,
            double dy)
        : distanceGrid_(std::move(d)),
          auxGrid_(std::move(a)),
          stateGrid_(std::move(s)),
          updateGrid_(std::move(u)),
          dx_(dx),
          dy_(dy),
          hyp_(std::sqrt(dx * dx + dy * dy))
    {
        // make sure unknown are +max
        for (int li = 0, liend = stateGrid_.count(); li < liend; li++) {
            if (stateGrid_(li) == State::Far) {
                distanceGrid_(li) = std::numeric_limits<double>::max();
            }
        }
        // bootstrap
        for (int li = 0, liend = stateGrid_.count(); li < liend; li++) {
            if (stateGrid_(li) == State::Initial) {
                if (distanceGrid_(li) >= 0) {
                    // only do positive side
                    Index2 pt = stateGrid_.li2coord(li);

                    const Index2 nbs[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                                          pt.s(), pt.sw(), pt.w(), pt.nw()};

                    for (const Index2& nb : nbs) {
                        if (!BOUNDS(nb)) continue;
                        if (stateGrid_(nb) == State::Initial) continue;

                        add(nb);
                    }
                }
            }
        }
        std::swap(workList_, nextWorkList_);  // add puts inds into nextWorklist
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    FmmGrid(ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
            double dy)
        : distanceGrid_(sx, sy),
          auxGrid_(sx, sy),
          stateGrid_(sx, sy),
          updateGrid_(sx, sy),
          dx_(dx),
          dy_(dy),
          hyp_(std::sqrt(dx * dx + dy * dy))
    {
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // initialize narrow band
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                if (std::abs(std::get<1>(p)) <
                    std::abs(d(std::get<0>(p))))  // the smallest initializer
                                                  // no matter sign should
                                                  // "win"
                    touch(std::get<0>(p), std::get<1>(p), std::get<2>(p),
                          State::Initial);
            });

        // bootstrap algorithm by visiting neighbors to points in the
        // narrow band (on the positive side)
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                Index2 pt = std::get<0>(p);

                // we can't rely on the distance value in the pair here
                // need to check on the grid
                if (distanceGrid_(std::get<0>(p)) < 0) return;

                const Index2 nbs[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                                      pt.s(), pt.sw(), pt.w(), pt.nw()};

                for (const Index2& nb : nbs) {
                    if (!BOUNDS(nb)) continue;
                    if (stateGrid_(nb) == State::Initial) continue;

                    add(nb);
                }

            });
        std::swap(workList_, nextWorkList_);  // add puts inds into nextWorklist
    }

    // ------------------------------------------------------------
    void visit(const Index2& pt)
    {
        //        assert(BOUNDS(pt), "visit out of bounds");
        //        assert(stateGrid_(pt) != State::INITIAL, "visit to initial");

        // DON'T VISIT POINTS ON THE NEGATIVE SIDE
        // THEY MIGHT HAVE NEIGHBORS WHICH ARE NOT YET
        // SET AND IF ADDED TO THE QUEUE VIOLATES THE
        // POSITIVE REQUIREMENTS
        if (distanceGrid_(pt) <= 0) return;

        double old_d = distanceGrid_(pt);
        double new_d = std::min(computeDistance(pt, pt), old_d);
        ++updateGrid_(pt);

        //        assert(new_d >= 0, "cd pos");

        touch(pt, new_d, new_d, State::Active);  // save smallest

        // test convergence
        if (std::abs(old_d - new_d) < 1e-9) {
            touch(pt, new_d, new_d, State::Final);
            visitnbs(pt);
        } else {
            // the point did not yet converge
            add(pt);  // add to next worklist to keep working
        }
    }

    // ------------------------------------------------------------
    void visitnbs(const Index2& pt)
    {
#if 0
        const Index2 nbs[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                              pt.s(), pt.sw(), pt.w(), pt.nw()};
#else
        const Index2 nbs[] = {pt.e(), pt.s(), pt.n(), pt.w()};
#endif

        for (const Index2& nb : nbs) {
            if (!BOUNDS(nb)) continue;
            // don't touch INITIALS
            if (stateGrid_(nb) == State::Initial) continue;
            // already active, skip
            if (stateGrid_(nb) == State::Active) continue;
            if (distanceGrid_(nb) < 0) {
                continue;  // don't go negative
            }

            double old_d = distanceGrid_(nb);
            double new_d = computeDistance(nb, pt);
            ++updateGrid_(nb);
            //                assert(new_d >= 0, "posit");
            if (new_d < old_d) {
                touch(nb, new_d, new_d, State::Active);
                add(nb);
            }
        }
    }

    // ------------------------------------------------------------
    void run()
    {
#ifndef NDEBUG
        std::size_t i = 0;
#endif
        while (!workList_.empty()) {
            for (Index2 pt : workList_) {
//#pragma omp parallel for
// for (int i = 0; i<workList_.size(); i++) {
//   Index2 pt = workList_.at(i);
#ifndef NDEBUG
                // upper bound num iterations, normally 8
                int maxVisitsPerPoint = 5;
                if (++i > distanceGrid_.dim1() * distanceGrid_.dim2() *
                              maxVisitsPerPoint) {
                    printf("fmmgrid early exit %lu (%lu)\n", i,
                           distanceGrid_.dim1() * distanceGrid_.dim2() *
                               maxVisitsPerPoint);
                    break;
                }
#endif
                visit(pt);  // visit may update distance, and if so it
                            // will also update neighbors
            }

            workList_.clear();
            std::swap(workList_, nextWorkList_);
        }
    }

    const Array2d& getDistanceGrid() const { return distanceGrid_; }
    const Array2<State>& getStateGrid() const { return stateGrid_; }
    const Array2<int>& getUpdateGrid() const { return updateGrid_; }

  protected:
    // ------------------------------------------------------------
    struct TSet
    {
        TSet() = default;
        TSet(double dij, double djk, double dik, double dj, double dk)
            : Dij(dij), Djk(djk), Dik(dik), Dj(dj), Dk(dk)
        {
        }
        double Dij, Djk, Dik;  // inter-vertex distances
        double Dj, Dk;         // vertex values
    };
    // ------------------------------------------------------------
    bool BOUNDS(const Index2& p) const { return distanceGrid_.bound(p); }
    // ------------------------------------------------------------
    double d(const Index2& p) const { return distanceGrid_(p); }
    // ------------------------------------------------------------
    double d(std::size_t li) const { return distanceGrid_(li); }
    // ------------------------------------------------------------
    double computeDistance(const Index2& pt, const Index2& /*from*/) const
    {
        const Index2 nbs[] = {pt.w(), pt.nw(), pt.n(), pt.ne(),
                              pt.e(), pt.se(), pt.s(), pt.sw()};

// use symmetry to reduce the size of the look-up
#if 1
        const double dij[] = {dx_, hyp_, dy_, hyp_};
        const double djk[] = {dy_, dx_, dx_, dy_};
// const double dki[] = {hyp_, dx_, hyp_, dy_}; // use offset instead
#else
        const double dij[] = {dx_, hyp_, dy_, hyp_, dx_, hyp_, dy_, hyp_};
        const double djk[] = {dy_, dx_, dx_, dy_, dx_, dy_, dx_, dy_};
        const double dki[] = {hyp_, dy_, hyp_, dx_,
                              hyp_, dy_, hyp_, dx_};  // use offset instead
#endif

        double dnew = std::numeric_limits<double>::max();

        for (int i = 0; i < 8; i++) {
            const Index2& ind_j = nbs[i];
            const Index2& ind_k = nbs[(i + 1) % 8];

            // the value of the neighbour j
            double d_j = std::numeric_limits<double>::max();
            if (BOUNDS(ind_j)) {
                d_j = distanceGrid_(ind_j);
            }

            double d_k = std::numeric_limits<double>::max();
            if (BOUNDS(ind_k)) {
                d_k = distanceGrid_(ind_k);
            }
            // if(d_j == std::numeric_limits<double>::max()
            //    && d_k == std::numeric_limits<double>::max())
            //     continue;

            double d_tentative = upwind_interpolate(
#if 1
                dij[i % 4],        // distance i->j
                djk[i % 4],        // distance j->k
                dij[(i + 1) % 4],  // distance k->i
#else
                dij[i],                               // distance i->j
                djk[i],                               // distance j->k
                dki[i],                               // distance k->i
#endif
                d_j,  // distance at j
                d_k   // distance at k
                );
            //            assert(d_tentative >= 0, "positive required");

            if (d_tentative < dnew) dnew = d_tentative;
        }
        return dnew;
    }

    // ------------------------------------------------------------
    void touch(const Index2& index, double val, T t,
               State state = State::Active)
    {
        //        std::lock_guard<std::mutex> lock(mutex_);
        //        assert(BOUNDS(index), "tbound");
        //        assert(val >= 0 || state == State::INITIAL, "touch pos");
        distanceGrid_(index) = val;
        auxGrid_(index) = t;
        stateGrid_(index) = state;
    }
    // ------------------------------------------------------------
    void add(const Index2& index)
    {
        //        std::lock_guard<std::mutex> lock(mutex_);
        //        assert(BOUNDS(index), "add bound");
        nextWorkList_.push_back(index);
    }

    // ------------------------------------------------------------
};

}  // namespace distance

#ifdef MATLAB_MEX_FILE
#pragma pop_macro("assert")
#endif
