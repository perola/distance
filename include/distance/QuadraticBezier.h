// -*- c++ -*-
#pragma once

#include <algorithm>
#include <cstddef>
#include "Bezier.h"
#include "CubicBezier.h"
#include <array>          // for array
#include "lerp.h"         // for lerp

namespace distance {

/** A Quadratic Bezier over three templated values
 */
template <typename T>
class QuadraticBezier {
  public:
    QuadraticBezier() = default;
    QuadraticBezier(T c0, T c1, T c2)
    {
        coeff[0] = c0;
        coeff[1] = c1;
        coeff[2] = c2;
    }
    explicit QuadraticBezier(const std::array<T, 3>& ds)
        : QuadraticBezier(std::begin(ds), std::end(ds))
    {
    }
    template <typename ForwardIt>
    QuadraticBezier(ForwardIt start, ForwardIt end)
    {
        assert(end > start && end - start == 3);
        std::copy(start, end, coeff);
    }
    CubicBezier<T> toCubic() const
    {
        // degree elevation
        return CubicBezier<T>(c0(), 1.0 / 3 * c0() + 2.0 / 3 * c1(),
                              2.0 / 3 * c1() + 1.0 / 3 * c2(), c2());
    }
    static constexpr std::size_t num_components() { return 3; }
    friend QuadraticBezier operator+(const QuadraticBezier& b, T t)
    {
        return QuadraticBezier(b.c0() + t, b.c1() + t, b.c2() + t);
    }
    // evaluate the Bezier
    T eval(double t) const { return BezierEvaluator<T, 0, 2>::eval(coeff, t); }
    // evaluate first derivative
    T eval1(double t) const { return BezierEvaluator<T, 1, 2>::eval(coeff, t); }
    // eval second derivative
    T eval2(double t) const { return BezierEvaluator<T, 2, 2>::eval(coeff, t); }
    // split the bezier into two
    void split(double t, QuadraticBezier<T>* a, QuadraticBezier<T>* b) const
    {
        // split the bezier using deCasteljaus algorithm
        const T c12 = lerp(coeff[0], coeff[1], t);
        const T c23 = lerp(coeff[1], coeff[2], t);
        const T c123 = lerp(c12, c23, t);

        *a = QuadraticBezier(coeff[0], c12, c123);
        *b = QuadraticBezier(c123, c23, coeff[2]);
    }
    double length() const { return BezierEvaluator<T, 0, 2>::length(coeff); }
    const T& operator[](int i) { return coeff[i]; }
    const T& c0() const { return coeff[0]; }
    const T& c1() const { return coeff[1]; }
    const T& c2() const { return coeff[2]; }
    const std::array<T, 3>& coeffs() const { return coeff; }

    bool friend operator==(const QuadraticBezier<T>& b1, const QuadraticBezier<T>& b2)
    {
        return b1.coeffs() == b2.coeffs();
    }
    friend bool operator!=(const QuadraticBezier<T>& b1, const QuadraticBezier<T>& b2)
    {
        return b1.coeffs() != b2.coeffs();
    }
  private:
    std::array<T,3> coeff;
};

}  // namespace distance
