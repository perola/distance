// -*- c++ -*-
#pragma once

#include <algorithm>       // for fill, for_each
#include <cstdlib>         // for abs
#include <limits>          // for numeric_limits
#include <tuple>           // for get, make_tuple, tuple
#include <vector>          // for vector
#include "Array2.h"        // for Array2
#include "DistanceTags.h"  // for State, State::Initial, State::Active, Stat...
#include "Eikonal.h"       // for QuadraticEikonal2D
#include "Index2.h"        // for Index2

#include <deque>

namespace distance {
// ------------------------------------------------------------
// ------------------------------------------------------------
class FastSweeping {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx1_, dx2_;

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int dim1, int dim2, double dx1,
        double dx2)
    {
      FastSweeping fig(begin, end, dim1, dim2, dx1, dx2);
        fig.run();
        return std::make_tuple(fig.distanceGrid_, fig.stateGrid_,
                               fig.updateGrid_);
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    FastSweeping(ForwardIter begin, ForwardIter end, int dim1, int dim2, double dx1,
                 double dx2)
        : distanceGrid_(dim1, dim2),
          stateGrid_(dim1, dim2),
          updateGrid_(dim1, dim2),
          dx1_(dx1),
          dx2_(dx2)
    {
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // initialize narrow band
        std::for_each(begin, end, [&](const std::tuple<Index2, double, double>& p) {
            if (std::abs(std::get<1>(p)) <
                std::abs(d(std::get<0>(p))))  // the smallest initializer
                                       // no matter sign should
                                       // "win"
	      touch(std::get<0>(p), std::get<1>(p), State::Initial);
        });
    }

    // ------------------------------------------------------------
    void run()
    {
        const int dim1 = distanceGrid_.dim1();
        const int dim2 = distanceGrid_.dim2();

        // Sweep.
        // 1st sweep. x+y+
        for (auto x = 0, endx = dim1; x < endx; x++)
            for (auto y = 0, endy = dim2; y < endy; y++)
                if (stateGrid_(x, y) != State::Initial) visit(x, y);
        // 2nd sweep. x-y+
        for (auto x = dim1 - 1; x >= 0; x--)
            for (auto y = 0, endy = dim2; y < endy; y++)
                if (stateGrid_(x, y) != State::Initial) visit(x, y);
        // 3rd sweep. x-y-
        for (auto x = dim1 - 1; x >= 0; x--)
            for (auto y = dim2 - 1; y >= 0; y--)
                if (stateGrid_(x, y) != State::Initial) visit(x, y);
        // 4th sweep. x+y-
        for (auto x = 0, endx = dim1; x < endx; x++)
            for (auto y = dim2 - 1; y >= 0; y--)
                if (stateGrid_(x, y) != State::Initial) visit(x, y);
    }

  protected:
    // ------------------------------------------------------------
    bool BOUNDS(const Index2& p) const { return distanceGrid_.bound(p); }
    // ------------------------------------------------------------
    double d(int x, int y) const { return distanceGrid_(x, y); }
    double d(const Index2& p) const { return distanceGrid_(p); }
    // ------------------------------------------------------------
    void visit(int x, int y)
    {
        auto oldval = d(x, y);
        auto newval = compute_checked(x, y);
        if (std::abs(newval) < std::abs(oldval)) touch(Index2{x, y}, newval);
    }
    // ------------------------------------------------------------
    double compute_checked(int x, int y)
    {
        const Index2 pt(x, y);
        const double dc = d(pt);
        const double uxp = BOUNDS(pt.e()) ? d(pt.e()) : dc;
        const double uxm = BOUNDS(pt.w()) ? d(pt.w()) : dc;
        const double uyp = BOUNDS(pt.n()) ? d(pt.n()) : dc;
        const double uym = BOUNDS(pt.s()) ? d(pt.s()) : dc;

        double u[2] = {std::abs(uxp) < std::abs(uxm) ? uxp : uxm,
                       std::abs(uyp) < std::abs(uym) ? uyp : uym};

#if 0
        // sort the input values
        if (std::abs(u[0]) > std::abs(u[1]))
            std::swap(u[0], u[1]);  // use the sign of the smallest neighbor to
                                    // decide sign of update
        return QuadraticEikonal2D(u[0], u[1], u[0] < 0 ? -1 : 1);
#else

        double sign = std::abs(u[0]) < std::abs(u[1]) ? u[0] < 0 ? -1 : 1
                                                      : u[1] < 0 ? -1 : 1;
        return QuadraticEikonal2D(u[0], u[1], dx2_, dx1_, sign);
#endif
    }

    // ------------------------------------------------------------
    void touch(const Index2& index, double val, State state = State::Active)
    {
        //        std::lock_guard<std::mutex> lock(mutex_);
        //        dbgassert(BOUNDS(index), "tbound");
        //        assert(val >= 0 || state == State::Initial, "touch pos");
        distanceGrid_(index) = val;
        stateGrid_(index) = state;
    }
};

}  // namespace distance
