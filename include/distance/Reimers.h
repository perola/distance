//===-- src/Reimers.h - A distance computing algorithm ----------*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
///
/// \file
/// \brief This file contains an implementation of the distance
/// computing algorithm in \cite Reimers:2005
/// \author Ola Nilsson, 2015
///
//===----------------------------------------------------------------------===//
#pragma once

#include <algorithm>            // for min
#include <cmath>                // for sqrt
#include <limits>               // for numeric_limits
#include <queue>                // for pop_heap, push_heap
#include <utility>              // for pair
#include <vector>               // for vector
#include "AdjacencyListMesh.h"  // for AdjacencyListMesh
#include "Dijkstra.h"           // for DistanceCompare

namespace distance {

template <typename VecT> class Reimers {
public:
  enum class State { ACTIVE, INACTIVE, FINAL };
  using vec_type = VecT;
  using mesh_type = AdjacencyListMesh<VecT>;

private:
  mutable std::vector<int> HeapData_;
  mesh_type Mesh_;

public:
  std::vector<State> Tags_;
  std::vector<double> Distance_;

  struct SphericalLocalUpdate {
    double computeDistance(double Djk, double Dij, double Dik, double Dj,
                           double Dk) {
      if ((std::abs(Dj - Dk) <= Djk) && (Djk <= (Dj + Dk))) {
        double x = (Djk * Djk + Dj * Dj - Dk * Dk) / (2 * Djk);
        double y = std::sqrt(Dj * Dj - x * x);

        double x2 = (Djk * Djk + Dij * Dij - Dik * Dik) / (2 * Djk);
        double y2 = std::sqrt(Dij * Dij - x2 * x2);

        // convexity tests
        double t = y * (x2 - x) / (y + y2) + x;
        if (t >= 0 && t <= Djk) {
          double w = std::sqrt((x - x2) * (x - x2) + (y + y2) * (y + y2));
          return w;
        }
      }

      // Dijkstra
      return std::min(Dij + Dj, Dik + Dk);
    }
  };

public:
  Reimers(const mesh_type &mesh)
      : Mesh_(mesh), Tags_(Mesh_.getVertices().size(), State::INACTIVE),
        Distance_(Mesh_.getVertices().size(),
                  std::numeric_limits<double>::max()) {}

  //!
  void initialize(int vtx) { touch({vtx, 0.0}); }

  void initialize(const std::vector<int> &points) {
    for (int vtx : points)
      touch({vtx, 0.0});
  }

  void touch(const std::vector<std::pair<int, double>> &boundary) {
    for (auto const &vertexValue : boundary)
      touch(vertexValue);
  }
  void touch(const std::pair<int, double> &point) {
    setDistance(point.first, point.second);
    add(point.first);
  }

  //! run the algorithm over the input
  void run() {
    while (hasNext()) {
      int vtx = getNext();
      setTag(vtx, State::FINAL);
      std::cerr << "R: pop: " << vtx << ", dist: " << getDistance(vtx)
                << std::endl;
      auto &ring = getMesh().getAdjacencyLists().at(vtx);
      int size = ring.front() != ring.back() ? ring.size() : ring.size() - 1;
      for (int i = 0; i < size; i++) {
        double d = computeDistance(ring.at(i), vtx);
        if (d < getDistance(ring.at(i))) {
          setDistance(ring.at(i), d);
          add(ring.at(i)); // add to queue, sets state ACTIVE
        }
      }
    }
  }

protected:
  const mesh_type &getMesh() const { return Mesh_; }

  //! add a vertex to the processing queue
  void add(int vtx) {
    setTag(vtx, State::ACTIVE);
    HeapData_.push_back(vtx);
    // push_heap assumes first, last-1 already heap.
    std::push_heap(HeapData_.begin(), HeapData_.end(),
                   DistanceCompare(Distance_));
  }

  //! does the queue contain more vertices to process ?
  bool hasNext() const {
    if (!HeapData_.empty()) {
      // the heap may contain INACTIVE elements which should be
      // discarded, so non-empty is not enough
      int vtx = HeapData_.at(0); // peak
      while (getTag(vtx) == State::INACTIVE) {
        std::pop_heap(HeapData_.begin(), HeapData_.end(),
                      DistanceCompare(Distance_));
        HeapData_.pop_back(); // discard
        if (HeapData_.empty())
          break;
        vtx = HeapData_.at(0); // peak
      }
    }
    return !HeapData_.empty();
  }

  //! extract and return the index of the 'next' vertex in the queue
  int getNext() {
    // calling getNext on a heap for which hasNext() == false is
    // undefined
    std::pop_heap(HeapData_.begin(), HeapData_.end(),
                  DistanceCompare(Distance_));
    int vtx = HeapData_.back(); // throws if empty
    HeapData_.pop_back();
    return vtx;
  }

  double getDistance(int vtx) const { return Distance_.at(vtx); }
  void setDistance(int vtx, double d) { Distance_.at(vtx) = d; }

  State getTag(int vtx) const { return Tags_.at(vtx); }
  void setTag(int vtx, State tag) { Tags_.at(vtx) = tag; }

  //! compute the smallest distance for x given the hint that
  //! information flows from parent
  double computeDistance(int vtx, int parentVtx) const {
    auto &ring = getMesh().getAdjacencyLists().at(vtx);
    double dnew = std::numeric_limits<double>::max();
    // not necesary to loop the whole ring
    // note that interior verts have start/end vertex stored twice in the
    // one-ring
    // such that it is always ok to loop [0, size()-1]
    for (int i = 0, num = ring.size() - 1; i < num; i++) {
      int j = ring.at(i), k = ring.at(i + 1);
      if (j == parentVtx || k == parentVtx) {
        SphericalLocalUpdate slu;
        dnew = std::min(dnew, slu.computeDistance(
                                  vertexDistance(j, k), vertexDistance(vtx, j),
                                  vertexDistance(vtx, k), getDistance(j),
                                  getDistance(k)));
      }
    }
    return dnew;
  }

  double vertexDistance(int vtx1, int vtx2) const {
    return (getMesh().getVertices().at(vtx1) - getMesh().getVertices().at(vtx2))
        .length();
  }
};
}

#endif
