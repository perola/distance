#pragma once

#define DISTANCE_HAS_FREE_FUNC(TYPE_NAME, FUNC_NAME)       \
    namespace detail {                                     \
    template <class TYPE_NAME>                             \
    constexpr auto has_##FUNC_NAME(const TYPE_NAME& obj)   \
        -> decltype(FUNC_NAME(obj), std::true_type{})      \
    {                                                      \
        return std::true_type{};                           \
    }                                                      \
    constexpr auto has_##FUNC_NAME(...) -> std::false_type \
    {                                                      \
        return std::false_type{};                          \
    }                                                      \
    }
