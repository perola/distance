// -*- c++ -*-
#pragma once

#include <algorithm>    // for max, min
#include <cassert>      // for assert
#include <type_traits>  // for __decay_and_strip<>::__type
#include <utility>      // for make_pair, pair
#include "Vec2d.h"      // for Vec2d, operator<<, operator==, operator+, ope...

#if DISTANCE_HAS_IOSTREAM
#include <iostream>  // for operator<<, ostream, basic_ostream
#endif

namespace distance {

struct aabb
{
    Vec2d pMin, pMax;
    aabb() : pMin({1, 1}), pMax({0, 0}) {}  // invalid, (but not empty)

    template <typename... Vec>
    explicit aabb(const Vec2d& p1, Vec&&... rest) : pMin(p1), pMax(pMin)
    {
        using expander = int[];
        (void)expander{0, ((void)grow(std::forward<Vec>(rest)), 0)...};
    }
    template <typename Iter>
    static aabb from_range(Iter begin, Iter end)
    {
        if (begin == end) return aabb();
        aabb box = aabb(*begin);
        while (++begin != end) box.grow(*begin);
        return box;
    }

    aabb& grow(const Vec2d& x)
    {
        pMin.x = std::min(pMin.x, x.x);
        pMax.x = std::max(pMax.x, x.x);
        pMin.y = std::min(pMin.y, x.y);
        pMax.y = std::max(pMax.y, x.y);
        return *this;
    }
    aabb& grow(const aabb& other)
    {
        grow(other.pMin);
        grow(other.pMax);
        return *this;
    }
    aabb& grow(double delta)
    {
        grow(pMin - Vec2d(delta, delta));
        grow(pMax + Vec2d(delta, delta));
        return *this;
    }
    aabb grown(double delta) const
    {
        assert(delta >= 0 && "cannot shrink aabb");
        aabb gr(*this);
        return gr.grow(delta);
    }
    aabb intersection(const aabb& other) const
    {
        return aabb(Vec2d(std::max(pMin.x, other.pMin.x),
                          std::max(pMin.y, other.pMin.y)),
                    Vec2d(std::min(pMax.x, other.pMax.x),
                          std::min(pMax.y, other.pMax.y)));
    }
    bool contains(const Vec2d& p) const
    {
        return (p.x > pMin.x && p.x < pMax.x) && (p.y > pMin.y && p.y < pMax.y);
    }
    bool contains(const Vec2d& p, double eps) const
    {
        return (p.x + eps > pMin.x && p.x - eps < pMax.x) &&
               (p.y + eps > pMin.y && p.y - eps < pMax.y);
    }
    typename Vec2d::value_type width() const { return pMax.x - pMin.x; }
    typename Vec2d::value_type height() const { return pMax.y - pMin.y; }

    std::pair<double, double> intersection(const Vec2d& origo,
                                           const Vec2d& dir) const
    {
        // if dir.x/y is zero then dirfrac becomes inf which propagate
        // correctly through the min/max
        // See for example:
        // https://tavianator.com/fast-branchless-raybounding-box-intersections/
        double dirfrac_x = 1.0 / dir.x;
        double dirfrac_y = 1.0 / dir.y;

        float t1 = (pMin.x - origo.x) * dirfrac_x;
        float t2 = (pMax.x - origo.x) * dirfrac_x;
        float t3 = (pMin.y - origo.y) * dirfrac_y;
        float t4 = (pMax.y - origo.y) * dirfrac_y;

        float tmin = std::max(std::min(t1, t2), std::min(t3, t4));
        float tmax = std::min(std::max(t1, t2), std::max(t3, t4));

        return std::make_pair(tmin, tmax);
    }
    bool isEmpty() const { return pMax == pMin; }
    bool isValid() const { return pMax >= pMin; }
#if DISTANCE_HAS_IOSTREAM
    friend std::ostream& operator<<(std::ostream& os, const aabb& box)
    {
        return box.isValid()
                   ? (os << '[' << box.pMin << " -> " << box.pMax << ']')
                   : (os << "[*,* -> *,*]");
    }
#endif
    friend bool operator==(const aabb& b1, const aabb& b2)
    {
        return b1.pMin == b2.pMin && b1.pMax == b2.pMax;
    }
};

}  // namespace distance
