// -*- c++ -*-
#pragma once

#include <stdlib.h>  // for abs

#include <algorithm>  // for for_each
#include <cmath>      // for isfinite, sqrt
#include <limits>     // for numeric_limits
#include <tuple>      // for tuple, make_tuple
#include <utility>    // for pair

#include "Array2.h"        // for Array2
#include "DebugAssert.h"   // for ASSERT, DebugAssert
#include "DistanceTags.h"  // for State, State::Initial, State::Active
#include "Index2.h"
#include "UpwindInterpolation.h"  // for upwind_dijkstra, upwind_interpolate

namespace distance {
// ------------------------------------------------------------
// ------------------------------------------------------------
template <typename T>
class FastSweeping3 {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<T> auxGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx1_, dx2_, dx12_;

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<T>, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int dim1, int dim2, double dx1,
        double dx2)
    {
        FastSweeping3 fig(begin, end, dim1, dim2, dx1, dx2);
        fig.run();
        return std::make_tuple(fig.distanceGrid_, fig.auxGrid_, fig.stateGrid_,
                               fig.updateGrid_);
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    FastSweeping3(ForwardIter begin, ForwardIter end, int dim1, int dim2,
                  double dx1, double dx2)
        : distanceGrid_(dim1, dim2),
          auxGrid_(dim1, dim2),
          stateGrid_(dim1, dim2),
          updateGrid_(dim1, dim2),
          dx1_(dx1),
          dx2_(dx2),
          dx12_(std::sqrt(dx1 * dx1 + dx2 * dx2))
    {
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // not needed to initialize auxGrid since every distGrid cell will be
        // written, and thus every auxGrid.

        // initialize narrow band
        std::for_each(begin, end, [&](const std::tuple<Index2, double, T>& p) {
            int i = std::get<0>(p).i;
            int j = std::get<0>(p).j;
            if (i < 0 || j < 0) return;
            if (i >= dim1 || j >= dim2) return;

            if (std::abs(std::get<1>(p)) <
                std::abs(d(i, j)))  // the smallest initializer
                                    // no matter sign should
                                    // "win"
                touch(i, j, std::get<1>(p), std::get<2>(p), State::Initial);
        });
    }

    // ------------------------------------------------------------
    void run()
    {
        const int dim1 = distanceGrid_.dim1();
        const int dim2 = distanceGrid_.dim2();

        if (dim1 == 1 || dim2 == 1) {
            // 1D case, assumes contiguous data
            double step = dim1 == 1 ? dx2_ : dx1_;
            auto visit1d = [&](std::size_t li, int stencil) {
                if (stateGrid_(li) == State::Initial) return;
                const double dUp = distanceGrid_(li + stencil);
                const double sign = dUp < 0 ? -1 : 1;
                const double dNew = dUp + sign * step;
                if (std::abs(dNew) < std::abs(distanceGrid_(li))) {
                    distanceGrid_(li) = dNew;
                    stateGrid_(li) = State::Active;
                }
                updateGrid_(li)++;
            };

            for (int li = 1, count = distanceGrid_.count(); li < count; li++) {
                visit1d(li, -1);
            }
            for (int li = distanceGrid_.count() - 2; li >= 0; li--) {
                visit1d(li, +1);
            }
            return;
        }

        // Sweep.
        // 1st sweep. row+col+
        for (int row = 1, endrow = dim1; row < endrow; row++)
            for (int col = 1, endcol = dim2; col < endcol; col++)
                if (stateGrid_(row, col) != State::Initial)
                    visit(row, col, dx2_, dx12_, dx1_, d(row, col - 1), d(row - 1, col),
                          a(row, col - 1), a(row - 1, col));

        // 2nd sweep. row-col+
        for (int row = dim1 - 2; row >= 0; row--)
            for (int col = 1, endcol = dim2; col < endcol; col++)
                if (stateGrid_(row, col) != State::Initial)
                    visit(row, col, dx1_, dx12_, dx2_, d(row + 1, col), d(row, col - 1),
                          a(row + 1, col), a(row, col - 1));

        // 3rd sweep. row-col-
        for (int row = dim1 - 2; row >= 0; row--)
            for (int col = dim2 - 2; col >= 0; col--)
                if (stateGrid_(row, col) != State::Initial)
                    visit(row, col, dx2_, dx12_, dx1_, d(row, col + 1), d(row + 1, col),
                          a(row, col + 1), a(row + 1, col));

        // 4th sweep. row+col-
        for (int row = 1, endrow = dim1; row < endrow; row++)
            for (int col = dim2 - 2; col >= 0; col--)
                if (stateGrid_(row, col) != State::Initial)
                    visit(row, col, dx1_, dx12_, dx2_, d(row - 1, col), d(row, col + 1),
                          a(row - 1, col), a(row, col + 1));
    }

  protected:
    // ------------------------------------------------------------
    double d(int row, int col) const { return distanceGrid_(row, col); }
    T a(int row, int col) const { return auxGrid_(row, col); }
    // ------------------------------------------------------------
    void visit(int row, int col, double dij, double djk, double dik, double dj,
               double dk, T aj, T ak)
    {
        const double oldval = d(row, col);
        ASSERT(std::isfinite(oldval), "finite numbers only", oldval, row, col);
        std::pair<double, T> newval;
        if (dj == std::numeric_limits<double>::max() ||
            dk == std::numeric_limits<double>::max())
            newval =
                upwind_dijkstra(dij, djk, std::abs(dj), std::abs(dk), aj, ak);
        else
            newval = upwind_interpolate(dij, djk, dik, std::abs(dj),
                                        std::abs(dk), aj, ak);
        double sign =
            std::abs(dj) < std::abs(dk) ? dj < 0 ? -1 : 1 : dk < 0 ? -1 : 1;
        if (newval.first < std::abs(oldval))
            touch(row, col, sign * newval.first, newval.second);
    }
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    void touch(int row, int col, double val, const T& t,
               State state = State::Active)
    {
        distanceGrid_(row, col) = val;
        auxGrid_(row, col) = t;
        stateGrid_(row, col) = state;
    }
};

}  // namespace distance
