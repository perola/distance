// -*- c++ -*-
#pragma once

#include <stdlib.h>   // for abs
#include <algorithm>  // for max
#include <cmath>      // for floor, isfinite
#include <deque>
#include <limits>         // for numeric_limits
#include <utility>        // for pair
#include "Bresenham.h"    // for PixelVisitor
#include "CubicBezier.h"  // for CubicBezier
#include "DebugAssert.h"
#include "Vec2d.h"   // for Vec2d
#include "dda.h"     // for dda
#include "ignore.h"  // for ignore

namespace distance {

namespace inner {
template <>
struct LengthConcept<Vec2d>
{
    static double length(const Vec2d& d) { return d.length(); }
};
}  // namespace inner

/** This class allows iteration over subdivided linear segments of a bezier
 * curve (each represented as a bezier)
 */
template <typename T, int MAX_DEPTH = 10>
class BezierLinearSegmentVisitor {
    static_assert(MAX_DEPTH > 1,
                  "Need a stack of at least 2 beziers in order to refine");

  public:
    // Any bezier has always at least one next
    bool hasNext() const { return curr_ > 0 || curr_ == -1; }

    // returns a bezier that is approximately linear
    const CubicBezier<T>& next()
    {
        if (curr_ > 0) {
            curr_--;
        } else {
            ASSERT(curr_ == -1);  // unstarted state
            curr_ = 0;
        }

        ASSERT(curr_ >= 0);
        refine();
        return stack_[curr_];
    }
    const CubicBezier<T> & bezier() const { return stack_[0]; }
    explicit BezierLinearSegmentVisitor(const CubicBezier<T>& b)
    {
        // precision requirements. the program is likely to enter an
        // inifinte loop if the step type (int) goes out of range
        for (const T& v : b.coeffs()) {
            ASSERT(v.x < std::numeric_limits<int>::max() ||
                   v.y < std::numeric_limits<int>::max());

            ASSERT(v.x > std::numeric_limits<int>::lowest() ||
                   v.y > std::numeric_limits<int>::lowest());

            ASSERT(std::isfinite(v.x) && std::isfinite(v.y));
            ignore(v);
        }
        stack_[0] = b;
        curr_ = -1;
    }
    void refine()
    {
        while (metric_d2(stack_[curr_]) && curr_ < (MAX_DEPTH - 1)) {
            stack_[curr_].split(.5, stack_ + curr_ + 1, stack_ + curr_);
            curr_++;
	    // std::cerr << stack_[curr_] <<std::endl;
            // ASSERT(curr_ < MAX_DEPTH - 1);
        }
	ASSERT(curr_ < MAX_DEPTH);
    }

    static bool metric_d2(const CubicBezier<T>& b)
    {
        typename T::value_type x4 = b.c3().x;
        typename T::value_type x3 = b.c2().x;
        typename T::value_type x2 = b.c1().x;
        typename T::value_type x1 = b.c0().x;

        typename T::value_type y4 = b.c3().y;
        typename T::value_type y3 = b.c2().y;
        typename T::value_type y2 = b.c1().y;
        typename T::value_type y1 = b.c0().y;

        typename T::value_type dx = x4 - x1;
        typename T::value_type dy = y4 - y1;

        typename T::value_type d2 = std::abs((x2 - x4) * dy - (y2 - y4) * dx);
        typename T::value_type d3 = std::abs((x3 - x4) * dy - (y3 - y4) * dx);

        return d2 + d3 > 1;  //*(dx*dx + dy*dy);
    }
    CubicBezier<T> stack_[MAX_DEPTH];
    int curr_;
};

template <typename T, int MAX_DEPTH = 10>
class BezierVisitor : public PixelVisitor {
    BezierLinearSegmentVisitor<T> segments_;

    /* The dda-visitor is more efficient but not numerically robust and does
         not always end up at end pos...)     */
    using LineVisitor = BresenhamVisitor;
    LineVisitor line_;

    static_assert(MAX_DEPTH > 1,
                  "Need a stack of at least 2 beziers in order to refine");

  public:
    const CubicBezier<T> bezier() const { return segments_.bezier(); }
    explicit BezierVisitor(const CubicBezier<T>& b) : segments_(b) {}
    bool hasNext() const override
    {
        return line_.hasNext() || segments_.hasNext();
    }
    std::pair<int, int> next() override
    {
        ASSERT(hasNext());
        if (!line_.hasNext()) {
            const auto& bezier = segments_.next();
            // std::cerr << "\n" << bezier << "\n";
            line_ = LineVisitor(bezier.c0().x, bezier.c0().y, bezier.c3().x,
                                bezier.c3().y);
        }
        ASSERT(line_.hasNext());
        return line_.next();
    }
};

template <typename T, int MAX_DEPTH = 10>
class CulledBezierVisitor : BezierVisitor<T, MAX_DEPTH> {
    using Parent = BezierVisitor<T, MAX_DEPTH>;
    std::pair<int, int> pos_;      // current pos
    std::pair<int, int> nextPos_;  // current peek pos
    bool hasNextPos_ =
        true;  // indicates that the pos stored in nextPos is valid

  public:
    explicit CulledBezierVisitor(const CubicBezier<T>& b) : Parent(b)
    {
        ASSERT(Parent::hasNext());
        nextPos_ = Parent::next();  // peek the stream, this is safe since
                                    // we know there's always at least one pos
                                    // in a bezier
    }
    bool hasNext() const override { return hasNextPos_; }
    std::pair<int, int> next() override
    {
        ASSERT(hasNextPos_);
        if (!Parent::hasNext()) {
            // early exit, parent stream is empty, we're done
            hasNextPos_ = false;
            return nextPos_;
        }

        // advance current
        pos_ = nextPos_;

        // we know there's more to consume from parent, advance 'peek' item
        nextPos_ = Parent::next();

        // skip repeated items from the parent visitor
        while (nextPos_ == pos_ && Parent::hasNext()) {
            nextPos_ = Parent::next();
        }

        // examine edge case "repeated item at end-of-stream"
        if (nextPos_ == pos_) {
            ASSERT(!Parent::hasNext());
            // indicate that we are exhausted, don't visit last (repeated)
            hasNextPos_ = false;
        }

        return pos_;
    }
};

#if 0
template <typename T, int MAX_DEPTH = 10>
class CulledBezierVisitor : PixelVisitor {
    std::deque<std::pair<int, int>> pixels_;
    int pos_ = 0;

  public:
    explicit CulledBezierVisitor(const CubicBezier<T>& b)
    {
        BezierVisitor<T, MAX_DEPTH> bezierVisitor(b);
        ASSERT(bezierVisitor.hasNext());
        pixels_.push_back(
            bezierVisitor.next());  // guaranteed at least one bezier
        while (bezierVisitor.hasNext()) {
            auto pos = bezierVisitor.next();
            if (pos == pixels_.back()) {
                continue;  // skip duplicates
            }
            pixels_.push_back(pos);
        }
    }
    bool hasNext() const override
    {
        return pos_ >= 0 && pos_ < static_cast<int>(pixels_.size());
    }
    std::pair<int, int> next() override { return pixels_.at(pos_++); }
};
#endif
}
