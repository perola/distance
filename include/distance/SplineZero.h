// -*- c++ -*-
#pragma once

#include <algorithm>       // for copy
#include <array>           // for array
#include <cstring>         // for memmove
#include <limits>          // for numeric_limits
#include <utility>         // for make_pair, pair

/** Spline root finding algorithm by K. Mørken and M. Reimers,
 * "An unconditionally convergent method for computing zeros of splines and
 * polynomials",
 * Mathematics of Computation 76 (2007), 845-865
 *
 * Finds the first root in the interval
 * Original pseudo code adopted to c++ by ola
 */

#include <array>
#include <utility>

template <int D, int MAX_ITER>
double knotAverage(const std::array<double, (D + 1) * 2 + MAX_ITER>& knots,
                   int k)
{
    double sum = 0;
    for (int i = k + 1; i <= k + D; i++) sum += knots[i];
    double avg = sum * 1.0 / D;
    return avg;
}

// Connected spline of degree D
// with knots t, coefficients c given
template <int D, int MAX_ITER>
std::pair<double, double> SplineZero(const std::array<double, (D + 1) * 2>& knots,
                                  const std::array<double, D + 1>& coeffs)
{
    std::array<double, (D + 1)* 2 + MAX_ITER> t = {0};
    std::array<double, D + 1 + MAX_ITER> c = {0};
    std::copy(std::begin(knots), std::end(knots), std::begin(t));
    std::copy(std::begin(coeffs), std::end(coeffs), std::begin(c));

    int k = 1;
    double err, lastX = 0;

    for (int it = 1; it <= MAX_ITER; it++) {
        // Compute the index of the smallest zero
        // of the control polygon
        int n = D + it - 1;  // last element
        while (k <= n && (c[k - 1] * c[k] > 0 || c[k - 1] == 0)) {
            k++;
        }
        if (k > n)
	  return std::make_pair(std::numeric_limits<double>::quiet_NaN(), -1.0);

        // Find zero of control polygon and check convergence
        double x = knotAverage<D, MAX_ITER>(t, k) -
            c[k] * (t[k + D] - t[k]) / (c[k] - c[k - 1]) * 1.0 / D;
	double dx = lastX-x;
	err = std::abs(dx) * (t[k + D - 1] - t[k + 1]);
        if (err < 1e-6) {
            return std::make_pair(err, x);
        }

        // Refine spline by Boehms algorithm
        int mu = k;
        while (x >= t[mu + 1]) {
            mu++;
        }

        c[n + 1] = c[n];  // push back
        for (int i = n; i >= mu + 1; i--) {
            c[i] = c[i - 1];
        }

        for (int i = mu; i >= mu - D + 1; i--) {
            double alpha = (x - t[i]) / (t[i + D] - t[i]);
            c[i] = (1 - alpha) * c[i - 1] + alpha * c[i];
        }

        {
            // t.insert(t.begin() + mu + 1, x);
            // insert is (usually) push_back + rotate right
            // rotate right needs reverse iterators so just use memmove and
            // insert
            // in place
            auto dst = mu + 2;
            auto src = mu + 1;
            auto byteCount = sizeof(double) * ((D + 1) * 2 + it - 2 - mu);
            std::memmove(std::begin(t) + dst, std::begin(t) + src,
                         byteCount);  // shift right
            t[src] = x;               // insert new knot
            lastX = x;
        }
    }
    // Max_iterations too small for convergence
    return std::make_pair(err, lastX);
}
