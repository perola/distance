#pragma once  // -*- c++ -*-

namespace distance {

// https://herbsutter.com/2009/10/18/mailbag-shutting-up-compiler-warnings/
template <class T>
void ignore(const T&)
{
}

}  // namespace distance
