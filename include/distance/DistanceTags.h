// -*- c++ -*-
#pragma once
namespace distance {
enum class State : int
{
    Far,
    Initial,
    Active,
    Final
};
}
