// -*- C++ -*-
#pragma once

#include <algorithm>            // for min
#include <limits>               // for numeric_limits
#include <queue>                // for pop_heap, push_heap
#include <utility>              // for pair
#include <vector>               // for vector
#include "AdjacencyListMesh.h"  // for AdjacencyListMesh

namespace distance {
enum class State { ACTIVE, FAR, FINAL };

struct DistanceCompare {
  DistanceCompare(const std::vector<double> &distances)
      : Distances_(distances) {}
  const std::vector<double> &Distances_;
  bool operator()(int i, int j) const {
    return Distances_.at(i) > Distances_.at(j);
  }
};

template <typename VecT> class Dijkstra {
  mutable std::vector<int> HeapData_;
  AdjacencyListMesh<VecT> Mesh_;

public:
  using vec_type = VecT;
  using mesh_type = AdjacencyListMesh<VecT>;

  std::vector<State> Tags_;
  std::vector<double> Distance_;

public:
  struct DijkstraLocalUpdate {
    double computeDistance(double /*Djk*/, double Dij, double Dik, double Dj,
                           double Dk) {
      return std::min(Dij + Dj, Dik + Dk);
    }
  };

  Dijkstra(const mesh_type &mesh)
      : Mesh_(mesh), Tags_(Mesh_.getVertices().size(), State::FAR),
        Distance_(Mesh_.getVertices().size(),
                  std::numeric_limits<double>::max()) {}

  //!
  void initialize(int vtx) {
      touch({vtx, 0.0});
  }
  void initialize(const std::vector<int>& points) {
    for(int vtx:points)
      touch({vtx, 0.0});
    //    touch(std::make_pair(0,0.0);
  }

  void touch(const std::vector<std::pair<int, double>> &boundary) {
    for (auto const &vertexValue : boundary)
      touch(vertexValue);
  }
  void touch(const std::pair<int, double> &point) {
    setDistance(point.first, point.second);
    add(point.first);
  }

  //! run the algorithm over the input
  void run() {
    while (hasNext()) {
      int vtx = getNext();
      setTag(vtx, State::FINAL);
      auto const &ring = getMesh().getAdjacencyLists().at(vtx);
      int size = ring.front() != ring.back() ? ring.size() : ring.size() - 1;
      for (int i = 0; i < size; i++) {
        double d = computeDistance(ring.at(i), vtx);
        if (d < getDistance(ring.at(i))) {
          setDistance(ring.at(i), d);
          add(ring.at(i)); // add to queue, sets state ACTIVE
        }
      }
    }
  }

protected:
  const mesh_type &getMesh() const { return Mesh_; }

  //! add a vertex to the processing queue
  void add(int vtx) {
    setTag(vtx, State::ACTIVE);
    HeapData_.push_back(vtx);
    // push_heap assumes first, last-1 already heap.
    std::push_heap(HeapData_.begin(), HeapData_.end(),
                   DistanceCompare(Distance_));
  }

  //! does the queue contain more vertices to process ?
  bool hasNext() const {
    if (!HeapData_.empty()) {
      // the heap may contain FINAL elements which should be
      // discarded, so non-empty is not enough
      int vtx = HeapData_.at(0); // peak
      while (getTag(vtx) != State::ACTIVE) {
	//std::cerr << "Popped " << vtx<< " " << int(getTag(vtx)) <<
        //std::endl;
        std::pop_heap(HeapData_.begin(), HeapData_.end(),
                      DistanceCompare(Distance_));
        HeapData_.pop_back(); // discard
        if (HeapData_.empty())
          break;
        vtx = HeapData_.at(0); // peak
      }
    }
    return !HeapData_.empty();
  }

  //! extract and return the index of the 'next' vertex in the queue
  int getNext() {
    // calling getNext on a heap for which hasNext is false is
    // undefined
    std::pop_heap(HeapData_.begin(), HeapData_.end(),
                  DistanceCompare(Distance_));
    int vtx = HeapData_.back(); // throws if empty
    HeapData_.pop_back();
    return vtx;
  }

  double getDistance(int vtx) const { return Distance_.at(vtx); }
  void setDistance(int vtx, double d) { Distance_.at(vtx) = d; }

  State getTag(int vtx) const { return Tags_.at(vtx); }
  void setTag(int vtx, State tag) { Tags_.at(vtx) = tag; }

  //! compute the smallest distance for x given the hint that
  //! information flows from parent
  double computeDistance(int vtx, int parentVtx) const {
    auto &ring = getMesh().getAdjacencyLists().at(vtx);
    double dnew = std::numeric_limits<double>::max();
    // not necesary to loop the whole ring
    // note that interior verts have start/end vertex stored twice in the
    // one-ring
    // such that it is always ok to loop [0, size()-1]
    for (int i = 0, num = ring.size() - 1; i < num; i++) {
      int j = ring.at(i), k = ring.at(i + 1);
      if (j == parentVtx || k == parentVtx) {
        DijkstraLocalUpdate dlu;
        dnew = std::min(dnew, dlu.computeDistance(
                                  vertexDistance(j, k), vertexDistance(vtx, j),
                                  vertexDistance(vtx, k), getDistance(j),
                                  getDistance(k)));
      }
    }
    return dnew;
  }

  double vertexDistance(int vtx1, int vtx2) const {
    return (getMesh().getVertices().at(vtx1) - getMesh().getVertices().at(vtx2))
        .length();
  }
};
}

