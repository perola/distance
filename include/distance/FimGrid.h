// -*- c++ -*-
#pragma once

#include <stdio.h>                // for printf
#include <stdlib.h>               // for abs
#include <algorithm>              // for move, for_each
#include <cmath>                  // for sqrt
#include <cstddef>                // for size_t
#include <deque>                  // for deque
#include <limits>                 // for numeric_limits
#include <mutex>                  // for mutex
#include <tuple>                  // for get, tuple, make_tuple
#include "Array2.h"               // for Array2
#include "DistanceTags.h"         // for State, State::Initial, State::Active
#include "Index2.h"               // for Index2
#include "UpwindInterpolation.h"  // for upwind_interpolate

namespace distance {

// ------------------------------------------------------------
// ------------------------------------------------------------
class FimGrid {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx_, dy_;

    std::deque<Index2> workList_, nextWorkList_;
    std::mutex mutex_;

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
        double dy)
    {
        FimGrid fig(begin, end, sx, sy, dx, dy);
        fig.run();
        return std::make_tuple(fig.distanceGrid_, fig.stateGrid_,
                               fig.updateGrid_);
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    FimGrid(ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
            double dy)
        : distanceGrid_(sx, sy),
          stateGrid_(sx, sy),
          updateGrid_(sx, sy),
          dx_(dx),
          dy_(dy)
    {
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // initialize narrow band
        std::for_each(
            begin, end, [&](const std::tuple<Index2, double, double>& p) {
                if (std::abs(std::get<1>(p)) <
                    std::abs(d(std::get<0>(p))))  // the smallest initializer
                                                  // no matter sign should
                                                  // "win"
                    touch(std::get<0>(p), std::get<1>(p), State::Initial);
            });

        // bootstrap algorithm by visiting neighbors to points in the
        // narrow band
        std::for_each(begin, end, [&](const std::tuple<Index2, double, double>& p) {
            Index2 pt = std::get<0>(p);
            const Index2 nbs[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                                  pt.s(), pt.sw(), pt.w(), pt.nw()};

            for (const Index2& nb : nbs) {
                if (!BOUNDS(nb)) continue;
                if (stateGrid_(nb) == State::Initial) continue;

                add(nb);
            }

        });
        std::swap(workList_, nextWorkList_);  // add puts inds into nextWorklist
    }

    // ------------------------------------------------------------
    void visit(const Index2& pt)
    {
        double old_d = d(pt);
        double new_d = computeDistance(pt, pt);
        ++updateGrid_(pt);

        if (std::abs(new_d) > std::abs(old_d)) new_d = old_d;
        touch(pt, new_d, State::Active);  // save smallest

        // test convergence
        if (std::abs(old_d - new_d) < 1e-9) {
            touch(pt, new_d, State::Final);
            visitnbs(pt);
        } else {
            // the point did not yet converge
            add(pt);  // add to next worklist to keep working
        }
    }

    // ------------------------------------------------------------
    void visitnbs(const Index2& pt)
    {
#if 0
        const Index2 nbs[] = {pt.n(), pt.ne(), pt.e(), pt.se(),
                              pt.s(), pt.sw(), pt.w(), pt.nw()};
#else
        const Index2 nbs[] = {pt.e(), pt.s(), pt.n(), pt.w()};
#endif

        for (const Index2& nb : nbs) {
            if (!BOUNDS(nb)) continue;
            // don't touch INITIALS
            if (stateGrid_(nb) == State::Initial) continue;
            // already active, skip
            if (stateGrid_(nb) == State::Active) continue;

            double old_d = d(nb);
            double new_d = computeDistance(nb, pt);
            ++updateGrid_(nb);

            if (std::abs(new_d) < std::abs(old_d)) {
                touch(nb, new_d, State::Active);
                add(nb);
            }
        }
    }

    // ------------------------------------------------------------
    void run()
    {
#ifndef NDEBUG
        std::size_t i = 0;
#endif
        while (!workList_.empty()) {
            for (Index2 pt : workList_) {
//#pragma omp parallel for
// for (int i = 0; i<workList_.size(); i++) {
//   Index2 pt = workList_.at(i);
#ifndef NDEBUG
                // upper bound num iterations, normally 8
                int maxVisitsPerPoint = 5;
                if (++i > distanceGrid_.dim1() * distanceGrid_.dim2() *
                              maxVisitsPerPoint) {
                    printf("fmmgrid early exit %d (%d)\n", i,
                           distanceGrid_.dim1() * distanceGrid_.dim2() *
                               maxVisitsPerPoint);
                    break;
                }
#endif
                visit(pt);  // visit may update distance, and if so it
                            // will also update neighbors
            }

            workList_.clear();
            std::swap(workList_, nextWorkList_);
        }
    }

    const Array2d& getDistanceGrid() const { return distanceGrid_; }
    const Array2<State>& getStateGrid() const { return stateGrid_; }
    const Array2<int>& getUpdateGrid() const { return updateGrid_; }

  protected:
    // ------------------------------------------------------------
    bool BOUNDS(const Index2& p) const { return distanceGrid_.bound(p); }
    // ------------------------------------------------------------
    double d(const Index2& p) const { return distanceGrid_(p); }
    // ------------------------------------------------------------
    double computeDistance(const Index2& pt, const Index2&) const
    {
        const double dc = d(pt);
        const double uxp = BOUNDS(pt.e()) ? d(pt.e()) : dc;
        const double uxm = BOUNDS(pt.w()) ? d(pt.w()) : dc;
        const double uyp = BOUNDS(pt.n()) ? d(pt.n()) : dc;
        const double uym = BOUNDS(pt.s()) ? d(pt.s()) : dc;

        double u[2] = {std::abs(uxp) < std::abs(uxm) ? uxp : uxm,
                       std::abs(uyp) < std::abs(uym) ? uyp : uym};
#if 1
        // sort the input values (doesn't work if dx!=dy)
        if (std::abs(u[0]) > std::abs(u[1]))
            std::swap(u[0], u[1]);  // use the sign of the smallest neighbor to
                                    // decide sign of update
        return QuadraticEikonal2D(u[0], u[1], u[0] < 0 ? -1 : 1);
#else

        double sign = std::abs(u[0]) < std::abs(u[1]) ? u[0] < 0 ? -1 : 1
                                                      : u[1] < 0 ? -1 : 1;
        return QuadraticEikonal2D(u[0], u[1], dx_, dy_, sign);
#endif
    }

    // ------------------------------------------------------------
    void touch(const Index2& index, double val, State state = State::Active)
    {
        //        std::lock_guard<std::mutex> lock(mutex_);
        //        assert(BOUNDS(index), "tbound");
        //        assert(val >= 0 || state == State::Initial, "touch pos");
        distanceGrid_(index) = val;
        stateGrid_(index) = state;
    }
    // ------------------------------------------------------------
    void add(const Index2& index)
    {
        //        std::lock_guard<std::mutex> lock(mutex_);
        //        assert(BOUNDS(index), "add bound");
        nextWorkList_.push_back(index);
    }
};

}  // namespace distance

#ifdef MATLAB_MEX_FILE
#pragma pop_macro("assert")
#endif
