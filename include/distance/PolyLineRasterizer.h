#pragma once

#include <algorithm>
#include <deque>

#if DISTANCE_HAS_IOSTREAM
#include <iostream>
#endif
#include <cmath>
#include <set>
#include <type_traits>
#include <utility>
#include <vector>

#include "Array2.h"
#include "Bresenham.h"
#include "Lattice.h"
#include "PolyLine.h"
#include "aabb.h"
#include "clamp.h"
#include "dda.h"
#include "lerp.h"

namespace distance {

#ifndef RASTER_DEBUG
#define RASTER_DEBUG 0
#endif

enum class InitType
{
    Vertex,
    Line,
    Poly
};

// for interchanging with the initializers used in fast sweeping
using GridProjectionBase = std::tuple<Index2, double, double>;
struct GridProjection : public GridProjectionBase
{
    // now we can piggyback our GridProjection wherever the tuple is wanted
    GridProjection() = default;
    GridProjection(Index2 coord, double d, double p)
        : GridProjectionBase(coord, d, p)
    {
    }
    Index2 coord() const { return std::get<0>(*this); }
    int i() const { return std::get<0>(*this).i; }
    int j() const { return std::get<0>(*this).j; }
    double distance() const { return std::get<1>(*this); }
    double parameter() const { return std::get<2>(*this); }
    void set_distance(double d) { std::get<1>(*this) = d; }
    void set_parameter(double p) { std::get<2>(*this) = p; }
#if RASTER_DEBUG
    vec2<double> pos, gpos;
    InitType type;
#endif
};

/** An O(N^2) implementation that can be used for comparison and verification
 */
template <typename Point_t, bool isSigned = true>
class BasicPolyLineRasterizer {
  public:
    // makes a polyline along the bounding box encompassing the polyline
    // four line segments
    static auto make_poly_box(const PolyLine<Point_t>& poly, double eps)
    {
        // four line segments
        aabb box = poly.bbox().grown(eps);
        PolyLine<Point_t> pbox({box.pMin,
                                {first(box.pMin), second(box.pMax)},
                                box.pMax,
                                {first(box.pMax), second(box.pMin)},
                                box.pMin});
        return pbox;
    }

    /// find all points within eps of the line together with their respective
    /// (signed) distance values
    static std::deque<GridProjection> rasterize(const PolyLine<Point_t>& poly,
                                                const Lattice<Point_t>& lattice,
                                                double eps)
    {
        auto poly_box = poly.bbox().grown(eps * 1.1);
        // simplify the distance calculation call for early exits
        // if the point is outside the (grown) box we know it can be disregarded

        std::deque<GridProjection> inits;
        for (decltype(lattice.size1_) x = 0; x < lattice.size1_; x++) {
            for (decltype(lattice.size2_) y = 0; y < lattice.size2_; y++) {
                const Point_t worldPoint(lattice.l2w(x, y));
                assert(isfinite(worldPoint));
                if (!poly_box.contains(worldPoint)) continue;

                if (isSigned) {
                    auto projection = poly.signed_closest(worldPoint);
                    assert(isfinite(projection.projection));
                    double d = length(projection.projection - worldPoint);
                    if (d <= eps)
                        inits.push_back(GridProjection {
                            Index2(x, y),
                                projection.side == Side::Left ? d : -d,
                                projection.parameter
#if RASTER_DEBUG
                                ,
                                projection.projection, worldPoint,
                                InitType::Poly
#endif
                        });
                } else {
                    auto projection = poly.closest(worldPoint);
                    double d = length(projection.projection - worldPoint);
                    if (d <= eps)
                        inits.push_back(GridProjection {
                            Index2(x, y), d, projection.parameter,
#if RASTER_DEBUG
                                projection.projection, worldPoint,
                                InitType::Poly
#endif
                        });
                }
            }
        }
        return inits;
    }

    /// find all points that project on the line (along direction dir)
    // within eps of the line together with their respective / (signed)
    // distance values
    static std::deque<GridProjection> rasterize(const PolyLine<Point_t>& poly,
                                                const Lattice<Point_t>& lattice,
                                                const Point_t& dir, double eps)
    {
        std::deque<GridProjection> inits;
        for (decltype(lattice.size1_) x = 0; x < lattice.size1_; x++) {
            for (decltype(lattice.size2_) y = 0; y < lattice.size2_; y++) {
                const Point_t worldPoint(lattice.l2w(x, y));
                assert(isfinite(worldPoint));
                if (isSigned) {
                    auto projection = poly.signed_closest(worldPoint, dir);
                    assert(isfinite(projection.projection));
                    if (projection.parameter < 0. || projection.parameter > 1.)
                        continue;

                    double d = length(projection.projection - worldPoint);
                    if (d <= eps)
                        inits.push_back(GridProjection {
                            Index2(x, y),
                                projection.side == Side::Left ? d : -d,
                                projection.parameter
#if RASTER_DEBUG
                                ,
                                projection.projection, worldPoint,
                                InitType::Poly
#endif
                        });
                } else {
                    auto projection = poly.closest(worldPoint, dir);
                    double d = (projection.projection - worldPoint).length();
                    if (d <= eps)
                        inits.push_back(GridProjection {
                            Index2(x, y), d, projection.parameter
#if RASTER_DEBUG
                                ,
                                projection.projection, worldPoint,
                                InitType::Poly
#endif
                        });
                }
            }
        }
        return inits;
    }
};

template <typename Point_t>
class PolyLineRasterizer {
  public:
    //    using LineVisitor = dda;
    using LineVisitor = BresenhamVisitor;

    using value_type = typename Point_t::value_type;

    /**  Visit all grid points within eps of the polyline and
         store their corresponding distance/paramater in a list of init points

         Slow, as it queries the full poly for closest distance at every visited
       point
     */
    static std::deque<GridProjection> rasterize(const PolyLine<Point_t>& poly,
                                                const Lattice<Point_t>& lattice,
                                                double eps)
    {
        std::deque<GridProjection> inits;

        // find the required number of neighboring grid cells to visit
        // in order to find the correct closest distance
        int numX = std::max<int>(1, std::ceil(eps / first(lattice.step_)));
        int numY = std::max<int>(1, std::ceil(eps / second(lattice.step_)));

        for (unsigned int i = 0; i < poly.points().size() - 1; i++) {
            const ls::LineSegment<Point_t> line(poly.points().at(i),
                                                poly.points().at(i + 1));
            Point_t p0g = lattice.w2l(line.p0_);
            Point_t p1g = lattice.w2l(line.p1_);
            LineVisitor visitor(first(p0g) + .5, second(p0g) + .5,
                                first(p1g) + .5, second(p1g) + .5);
            std::set<Index2> visited;
            while (visitor.hasNext()) {
                const Index2 coord = Index2(visitor.next());
                for (int offsetX = -numX; offsetX <= numX; offsetX++)
                    for (int offsetY = -numY; offsetY <= numY; offsetY++) {
                        Index2 c(coord.i + offsetX, coord.j + offsetY);
                        if (visited.count(c)) continue;
                        if (c.i < 0 || c.j < 0) continue;
                        if (c.i >= static_cast<int>(lattice.size1_) ||
                            c.j >= static_cast<int>(lattice.size2_))
                            continue;

                        visited.insert(c);

                        const Point_t worldPoint(lattice.l2w(c.i, c.j));
                        auto projection = poly.signed_closest(worldPoint);
                        double d = length(projection.projection - worldPoint);
                        if (d > eps) continue;

                        inits.push_back(GridProjection {
                            c, projection.side == Side::Left ? d : -d,
                                projection.parameter
#if RASTER_DEBUG
                                ,
                                projection.projection, worldPoint,
                                InitType::Poly
#endif
                        });
                    }
            }
        }
        return inits;
    }

    /** Visit all grid points within eps of the polyline and
        store their corresponding distance/paramater in a list of init points

        Only queries the local geometry, ie. linesegment and vertex.

        The amount of work per grid cell is O(numX*numY) where numX/Y
        = eps/lattice.dx/y. The excessive work is somewhat culled by
        keeping track of visited points, but the sets are not reusable
        between linesegments/vertices
     */
    static std::deque<GridProjection> rasterize3(
        const PolyLine<Point_t>& poly, const Lattice<Point_t>& lattice,
        double eps)
    {
        assert(poly.count() >= 2 && "require at least one line segment");
        std::deque<GridProjection> inits;

        // find the required number of neighboring grid cells to visit
        // in order to find the correct closest distance
        int numX = std::max<int>(1, std::ceil(eps / first(lattice.step_)));
        int numY = std::max<int>(1, std::ceil(eps / second(lattice.step_)));

        //
        // The line segment visitor
        //   Visits the neighborhood around the line segment by
        //   tracing the grid points along the line and visiting a
        //   stencil around each point. In order to decrease the work
        //   a set of already visited gridpoints is kept and checked
        //   for early exit possibilities
        //
        auto visit_ls = [&inits, &lattice, eps, numX, numY](
                            const ls::ParameterizedLineSegment<Point_t>& line) {
            // transform from world coordinates to grid coordinates for the grid
            // visitation
            Point_t p0g = lattice.w2l(line.p0_);
            Point_t p1g = lattice.w2l(line.p1_);
            LineVisitor visitor(first(p0g) + .5, second(p0g) + .5,
                                first(p1g) + .5, second(p1g) + .5);
            std::set<Index2> visited;
            while (visitor.hasNext()) {
                const Index2 coord = Index2(visitor.next());
                for (int offsetX = -numX; offsetX <= numX; offsetX++)
                    for (int offsetY = -numY; offsetY <= numY; offsetY++) {
                        Index2 c(coord.i + offsetX, coord.j + offsetY);
                        if (c.i < 0 || c.j < 0) continue;
                        if (c.i >= static_cast<int>(lattice.size1_) ||
                            c.j >= static_cast<int>(lattice.size2_))
                            continue;

                        // wait with visited checks until real computations are
                        // needed, assume that inserting and/or searching the
                        // set is slower than the simple 'inside' checks above
                        if (visited.count(c)) continue;
                        visited.insert(c);

                        // transform grid to world coordinates in order to
                        // compute world space distance
                        const Point_t worldPoint(lattice.l2w(c.i, c.j));
                        double t = line.project(worldPoint);
                        if (t < 0. || t > 1.) continue;

                        auto pos = line.eval(t);
                        double d = length(pos - worldPoint);
                        if (d > eps) continue;

                        Side side = line.halfspace(worldPoint);

                        inits.push_back(GridProjection {
                            c, side == Side::Left ? d : -d, line.evalp(t)
#if RASTER_DEBUG
                                                                ,
                                pos, worldPoint, InitType::Line
#endif
                        });
                    }
            }
        };

        // apply visitor to all line segments
        for (unsigned int i = 0; i < poly.points().size() - 1; i++) {
            const ls::ParameterizedLineSegment<Point_t> line(
                poly.points().at(i), poly.points().at(i + 1),
                poly.params().at(i), poly.params().at(i + 1));
            visit_ls(line);
        }

        //
        // The vertex visitor
        //   Visits the neighborhood around a vertex, v, by visiting
        //   all the points in a stencil centered in v.
        //   The vertex tangent, T, is used to define sidedness of visited grid
        //   points. The vertex parameter, t, is assumed constant.
        //
        //   There is no way (barring repeated searching/deletion of the deque)
        //   of avoiding the insertion of duplicate vertex points into
        //   the initializers.
        auto visit_v = [&inits, &lattice, eps, numX, numY](
                           Point_t v, Point_t tangent, value_type t) {
            Point_t pg = lattice.w2l(v);
            Index2 coord(first(pg) + .5, second(pg) + .5);
            for (int offsetX = -numX; offsetX <= numX; offsetX++)
                for (int offsetY = -numY; offsetY <= numY; offsetY++) {
                    Index2 c(coord.i + offsetX, coord.j + offsetY);
                    if (c.i < 0 || c.j < 0) continue;
                    if (c.i >= static_cast<int>(lattice.size1_) ||
                        c.j >= static_cast<int>(lattice.size2_))
                        continue;

                    const Point_t worldPoint(lattice.l2w(c.i, c.j));
                    double d = length(worldPoint - v);

                    if (d > eps) continue;

                    Side side = halfspace(v, tangent, worldPoint);
                    auto gp = GridProjection
                    {
                        c, side == Side::Left ? d : -d, t,
#if RASTER_DEBUG
                            v, worldPoint, InitType::Vertex
#endif
                    };
                    inits.push_back(gp);
                }
        };

        // Visit vertices

        // 1. visit first vertex
        {
            // one sided finite difference if not closed
            Point_t first_tangent =
                ls::LineSegment<Point_t>(poly.points().at(0),
                                         poly.points().at(1))
                    .dir();
            if (poly.is_closed()) {
                const Point_t prev_tangent =
                    ls::LineSegment<Point_t>(
                        poly.points().at(poly.points().size() - 2),
                        poly.points().at(0))
                        .dir();
                first_tangent =
                    normalized(first_tangent) + normalized(prev_tangent);
            }
            visit_v(poly.points().front(), first_tangent,
                    poly.params().front());
        }

        // 2 .visit all intermediate vertices, 1..N-1/2 (2 if closed)
        for (unsigned int i = 1;
             i < poly.points().size() - (poly.is_closed() ? 2 : 1); i++) {
            const Point_t vertex_tangent =
                normalized(ls::LineSegment<Point_t>(poly.points().at(i - 1),
                                                    poly.points().at(i))
                               .dir()) +
                normalized(ls::LineSegment<Point_t>(poly.points().at(i),
                                                    poly.points().at(i + 1))
                               .dir());
            visit_v(poly.points().at(i), vertex_tangent, poly.params().at(i));
        }

        // 3. visit last vertex if not closed
        if (!poly.is_closed()) {
            // one sided finite difference tangent
            const Point_t last_tangent =
                ls::LineSegment<Point_t>(
                    poly.points().at(poly.points().size() - 2),
                    poly.points().at(poly.points().size() - 1))
                    .dir();
            visit_v(poly.points().back(), last_tangent, poly.params().back());
        }

        return inits;
    }
};

template <typename Point_t>
class DirectionalPolyLineRasterizer {
  public:
    //    using LineVisitor = dda;
    using LineVisitor = BresenhamVisitor;

    using value_type = typename Point_t::value_type;
    static std::deque<GridProjection> rasterize(const PolyLine<Point_t>& poly,
                                                const Lattice<Point_t>& lattice,
                                                const Point_t& dir, double eps)
    {
        assert(poly.count() >= 2 && "require at least one line segment");
        std::deque<GridProjection> inits;

        // find the required number of neighboring grid cells to visit
        // in order to find the correct closest distance
        int numX = std::max<int>(1, std::ceil(eps / first(lattice.step_)));
        int numY = std::max<int>(1, std::ceil(eps / second(lattice.step_)));

        //
        // The line segment visitor
        //   Visits the neighborhood around the line segment by
        //   tracing the grid points along the line and visiting a
        //   stencil around each point. In order to decrease the work
        //   a set of already visited gridpoints is kept and checked
        //   for early exit possibilities
        //
        auto visit_ls = [&inits, &lattice, eps, numX, numY, dir](
                            const ls::ParameterizedLineSegment<Point_t>& line) {
            Point_t p0g = lattice.w2l(line.p0_);
            Point_t p1g = lattice.w2l(line.p1_);
            LineVisitor visitor(first(p0g) + .5, second(p0g) + .5,
                                first(p1g) + .5, second(p1g) + .5);
            std::set<Index2> visited;
            while (visitor.hasNext()) {
                const Index2 coord = Index2(visitor.next());
                for (int offsetX = -numX; offsetX <= numX; offsetX++)
                    for (int offsetY = -numY; offsetY <= numY; offsetY++) {
                        Index2 c(coord.i + offsetX, coord.j + offsetY);

                        if (c.i < 0 || c.j < 0) continue;
                        if (c.i >= static_cast<int>(lattice.size1_) ||
                            c.j >= static_cast<int>(lattice.size2_))
                            continue;

                        // wait with visited checks until real computations are
                        // needed, assume that inserting and/or searching the
                        // set is slower than the simple 'inside' checks above
                        if (visited.count(c)) continue;
                        visited.insert(c);

                        const Point_t worldPoint(lattice.l2w(c.i, c.j));
                        double t = line.project(worldPoint, dir);
                        if (t < 0. || t > 1.) continue;

                        auto pos = line.eval(t);
                        double d = length(pos - worldPoint);
                        if (d > eps) continue;

                        Side side = line.halfspace(worldPoint);

                        inits.push_back(GridProjection{
                            c, side == Side::Left ? d : -d,
                            line.evalp(t)});  // , InitType::Line});
                    }
            }
        };

        // apply visitor to all line segments
        for (unsigned int i = 0; i < poly.points().size() - 1; i++) {
            const ls::ParameterizedLineSegment<Point_t> line(
                poly.points().at(i), poly.points().at(i + 1),
                poly.params().at(i), poly.params().at(i + 1));
            visit_ls(line);
        }

        // No need for vertex visitor for projection along direction
        return inits;
    }
};

template <typename Point_t>
class VerticalPolyLineRasterizer {
  public:
    //    using LineVisitor = dda;
    using LineVisitor = BresenhamVisitor;

    using value_type = typename Point_t::value_type;
    static std::deque<GridProjection> rasterize(const PolyLine<Point_t>& poly,
                                                const Lattice<Point_t>& lattice,
                                                double eps)
    {
        assert(poly.count() >= 2 && "require at least one line segment");
        std::deque<GridProjection> inits;

        // find the required number of neighboring grid cells to visit
        // in order to find the correct closest distance
        int numX = std::max<int>(1, std::ceil(eps / first(lattice.step_)));
        int numY = std::max<int>(1, std::ceil(eps / second(lattice.step_)));

        //
        // The line segment visitor
        //   Visits the neighborhood around the line segment by
        //   tracing the grid points along the line and visiting a
        //   stencil around each point. In order to decrease the work
        //   a set of already visited gridpoints is kept and checked
        //   for early exit possibilities
        //
        auto visit_ls = [&inits, &lattice, eps, numX, numY](
                            const ls::ParameterizedLineSegment<Point_t>& line) {
            Point_t p0g = lattice.w2l(line.p0_);
            Point_t p1g = lattice.w2l(line.p1_);
            LineVisitor visitor(first(p0g) + .5, second(p0g) + .5,
                                first(p1g) + .5, second(p1g) + .5);
            std::set<Index2> visited;
            while (visitor.hasNext()) {
                const Index2 coord = Index2(visitor.next());
                for (int offsetX = -numX; offsetX <= numX; offsetX++)
                    for (int offsetY = -numY; offsetY <= numY; offsetY++) {
                        Index2 c(coord.i + offsetX, coord.j + offsetY);

                        if (c.j < 0 || c.j >= static_cast<int>(lattice.size2_))
                            continue;

                        // wait with visited checks until real computations are
                        // needed, assume that inserting and/or searching the
                        // set is slower than the simple 'inside' checks above
                        if (visited.count(c)) continue;
                        visited.insert(c);

                        const Point_t worldPoint(lattice.l2w(c.i, c.j));
                        double t = line.project(worldPoint, Point_t(1, 0));
                        if (t < 0. || t > 1.) continue;

                        auto pos = line.eval(t);
                        double d = length(pos - worldPoint);
                        if (d > eps) continue;

                        Side side = line.halfspace(worldPoint);

                        inits.push_back(GridProjection{
                            c, side == Side::Left ? d : -d,
                            line.evalp(t)});  // , InitType::Line});
                    }
            }
        };

        // apply visitor to all line segments
        for (unsigned int i = 0; i < poly.points().size() - 1; i++) {
            const ls::ParameterizedLineSegment<Point_t> line(
                poly.points().at(i), poly.points().at(i + 1),
                poly.params().at(i), poly.params().at(i + 1));
            visit_ls(line);
        }

        // No need for vertex visitor for projection along direction
        return inits;
    }
#if 0
        void rasterize(const PolyLine<Point_t>& poly,
                       const Lattice<Point_t>& lattice, Array2<T>& dist,
                       Array2<T>& param)
        {
            using size_type = typename Array2<T>::size_type;
            for (size_type col = 0, numCols = dist.dim2(); col < numCols;
                 col++) {
                for (size_type row = 0, numRows = param.dim1(); row < numRows;
                     row++) {
                    param(row, col) = col / static_cast<double>(numCols - 1);
                }
            }

            int sign = poly.points().front().trace < poly.points().back().trace;

            auto visit_ls = [&lattice, sign,
                             &dist](const ls::LineSegment<Point_t>& line) {
                Point_t p0g = lattice.w2l(line.p0_);
                Point_t p1g = lattice.w2l(line.p1_);
                LineVisitor visitor(p0g[0] + .5, p0g[1] + .5, p1g[0] + .5,
                                    p1g[1] + .5);
                while (visitor.hasNext()) {
                    const Index2 coord = Index2(visitor.next());

                    // skip columns outside of grid
                    if (coord.j < 0) continue;
                    if (coord.j >= static_cast<int>(lattice.size2_)) continue;

                    const Point_t worldPoint(lattice.l2w(coord.i, coord.j));
                    double t = line.project(worldPoint, {1., 0.});
                    auto pos = line.eval(t);
                    double d = length(pos - worldPoint);

                    // the value at the start of the column (in the lattice), ie
                    // dist(0, col),
                    double low = sign * (coord.i * lattice.step_.z + d);

                    for (size_type row = 0, numRows = dist.dim1();
                         row < numRows; row++) {
                        // just increase every row with lattice step
                        const double newd = low + sign * row * lattice.step_.z;
                        if (std::abs(newd) < std::abs(dist(row, coord.j)))
                            dist(row, coord.j) = newd;
                    }
                }
            };
            // apply visitor to all line segments
            for (unsigned int i = 0; i < poly.points().size() - 1; i++) {
                const ls::LineSegment<Point_t> line(poly.points().at(i),
                                                    poly.points().at(i + 1));

                visit_ls(line);
            }
        }
#endif
};

}  // namespace distance
