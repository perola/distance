// -*- c++ -*-
#pragma once

#include "Array2.h"
#include "DistanceTags.h"
#include "Vec2d.h"
#include "lerp.h"
#include <cassert>
#include <limits>

#ifdef MATLAB_MEX_FILE
#pragma push_macro("assert")
#ifndef NDEBUG
#undef assert
#define assert(x, txt) \
    if (!(x)) mexErrMsgTxt(#txt)
#endif
#endif

namespace distance {
// ------------------------------------------------------------
// ------------------------------------------------------------
class SweepingSimilar {
  public:
    using Array2d = Array2<double>;

  private:
    Array2d distanceGrid_;
    Array2<State> stateGrid_;
    Array2<int> updateGrid_;
    double dx_, dy_;
    Vec2d dir_;

  public:
    // ------------------------------------------------------------
    template <typename ForwardIter>
    static std::tuple<Array2d, Array2<State>, Array2<int>> distance(
        ForwardIter begin, ForwardIter end, int sx, int sy, double dx,
        double dy, Vec2d dir)
    {
        SweepingSimilar fig(begin, end, sx, sy, dx, dy, dir);
        fig.run();
        return std::make_tuple(fig.distanceGrid_, fig.stateGrid_,
                               fig.updateGrid_);
    }
    // ------------------------------------------------------------
    template <typename ForwardIter>
    SweepingSimilar(ForwardIter begin, ForwardIter end, int sx, int sy,
                    double dx, double dy, Vec2d dir)
        : distanceGrid_(sx, sy),
          stateGrid_(sx, sy),
          updateGrid_(sx, sy),
          dx_(dx),
          dy_(dy),
          dir_(dir)
    {
        std::cerr << "dir:" << dir_.x << "," << dir_.y << std::endl;
        // initialize distance and states to "FAR"
        std::fill(distanceGrid_.begin(), distanceGrid_.end(),
                  std::numeric_limits<double>::max());
        std::fill(stateGrid_.begin(), stateGrid_.end(), State::Far);
        std::fill(updateGrid_.begin(), updateGrid_.end(), 0);

        // initialize narrow band
        std::for_each(begin, end,
                      [&](const std::tuple<Index2, double, double>& p) {
                          int i = std::get<0>(p).i;
                          int j = std::get<0>(p).j;
                          if (std::abs(std::get<1>(p)) <
                              std::abs(d(i, j)))  // the smallest initializer
                                                  // no matter sign should
                                                  // "win"
                              touch(i, j, std::get<1>(p),
                                    /*std::get<2>(p),*/ State::Initial);
                      });
    }

    // ------------------------------------------------------------
    void run()
    {
        const int dim1 = distanceGrid_.dim1();
        const int dim2 = distanceGrid_.dim2();

        /* Place a triangle in p=(x,y), pj=(xj, yj), pk=(xk, yk)
           The distances at pj and pk, d(j), d(k) and the direction dir
           define a line-segment s0 to s1:
           s0 = pj - dir*d(j)
           s1 = pk - dir*d(k)
           s(u) = s0*(1-u) + s1*u

           The distance along dir from p to s described by:
           s(u) == p + dir*t

           Without loss of generality translate p to (0,0)
           s0'*(1-u) + s1'*u == dir*t

         */

        // Sweep.
        // 1st sweep. x+, y+
        if (dir_.x >= 0 && dir_.y >= 0)
            for (int x = 1, endx = dim1; x < endx; x++)
                for (auto y = 1, endy = dim2; y < endy; y++) {
                    if (stateGrid_(x, y) != State::Initial) {
                        visit(x, y, Vec2d(0, -dy_), d(x, y - 1),
                              Vec2d(-dx_, -dy_), d(x - 1, y - 1));
                        visit(x, y, Vec2d(-dx_, -dy_), d(x - 1, y - 1),
                              Vec2d(-dx_, 0), d(x - 1, y));
                    }
                }

        // 2nd sweep. x-y+
        if (dir_.x < 0 && dir_.y >= 0)
            for (int x = dim1 - 1; x >= 0; x--)
                for (auto y = 0, endy = dim2; y < endy; y++)
                    if (stateGrid_(x, y) != State::Initial) {
                        visit(x, y, Vec2d(dx_, 0), d(x + 1, y),
                              Vec2d(dx_, -dy_), d(x + 1, y - 1));
                        visit(x, y, Vec2d(dx_, -dy_), d(x + 1, y - 1),
                              Vec2d(0, -dy_), d(x, y - 1));
                    }
        // 3rd sweep. x-y-
        if (dir_.x < 0 && dir_.y < 0)
            for (int x = dim1 - 1; x >= 0; x--)
                for (auto y = dim2 - 1; y >= 0; y--)
                    if (stateGrid_(x, y) != State::Initial) {
                        visit(x, y, Vec2d(0, dy_), d(x, y + 1), Vec2d(dx_, dy_),
                              d(x + 1, y + 1));
                        visit(x, y, Vec2d(dx_, dy_), d(x + 1, y + 1),
                              Vec2d(dx_, 0), d(x + 1, y));
                    }
        // 4th sweep. x+y-
        if (dir_.x >= 0 && dir_.y < 0)
            for (int x = 0, endx = dim1; x < endx; x++)
                for (auto y = dim2 - 1; y >= 0; y--)
                    if (stateGrid_(x, y) != State::Initial) {
                        visit(x, y, Vec2d(-dx_, 0), d(x - 1, y),
                              Vec2d(-dx_, dy_), d(x - 1, y + 1));
                        visit(x, y, Vec2d(-dx_, dy_), d(x - 1, y + 1),
                              Vec2d(0, dy_), d(x, y + 1));
                    }
    }

  protected:
    // ------------------------------------------------------------
    double d(int x, int y) const { return distanceGrid_(x, y); }
    double d(const Index2& p) const { return distanceGrid_(p); }
    // ------------------------------------------------------------
    void touch(const Index2& index, double val, State state = State::Active)
    {
        touch(index.i, index.j, val, state);
    }
    // ------------------------------------------------------------
    void visit(int x, int y, const Vec2d& p0, double d0, const Vec2d& p1,
               double d1)
    // ------------------------------------------------------------
    {
        // need two valid neighbors, uninitialized has limits::max
        // if (d0 == std::numeric_limits<double>::max() ||
        //     d1 == std::numeric_limits<double>::max())
        //     return;

        double oldval = d(x, y);
        double sign = 1;
        //            std::abs(d0) < std::abs(d1) ? d0 < 0 ? -1 : 1 : d1 < 0 ?
        //            -1 : 1;
        Vec2d dir = sign * dir_;  // flip the dir on the negative side
        double newval =
            project(dir, p0 - dir * std::abs(d0), p1 - dir * std::abs(d1));
        if (newval < std::abs(oldval)) touch(x, y, sign * newval);
    }
    // ------------------------------------------------------------
    static double project(const Vec2d& dir, const Vec2d& s0, const Vec2d& s1)
    // ------------------------------------------------------------
    {
        // Solve[(1 - u) {s0x, s0y} + u {s1x, s1y} == {0, 0} - t {dx, dy}, {t,
        // u}]
        // {{t -> (s0y s1x - s0x s1y) / (dy s0x - dx s0y - dy s1x + dx s1y),
        //   u -> (dy s0x - dx s0y)   / (dy s0x - dx s0y - dy s1x + dx s1y)}}

        double denom = dir.y * (s0.x - s1.x) + dir.x * (-s0.y + s1.y);
        double t = (s0.y * s1.x - s0.x * s1.y) / denom;
	if (t < 0) return std::numeric_limits<double>::max();
        double u = (dir.y * s1.x - dir.x * s1.y) / denom;
	std::cout << t << ", " << u << std::endl;
	if (u < 0 || u > 1) return std::numeric_limits<double>::max();
        return lerp(s0, s1, u).length();
    }
    // ------------------------------------------------------------
    void touch(int x, int y, double val,  // const T& t,
               State state = State::Active)
    {
        distanceGrid_(x, y) = val;
        stateGrid_(x, y) = state;
    }
};

}  // namespace distance

#ifdef MATLAB_MEX_FILE
#pragma pop_macro("assert")
#endif
