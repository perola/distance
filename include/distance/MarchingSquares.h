// -*- c++ -*-
#pragma once

#include <deque>     // for _Deque_iterator, deque
#include <limits>    // for numeric_limits
#include <utility>   // for pair, make_pair
#include "Array2.h"  // for Array2
#include "Index2.h"  // for Index2

namespace distance {
// hide lookup-tables in anonymous namespace
namespace {
// clang-format off
double Vertices[][2] =
    {	{ 0.0, 0.0 },
	{ 1.0, 0.0 },
	{ 1.0, 1.0 },
	{ 0.0, 1.0 }
    };
  
int Edge2Vertices[][2] =
    {	{ 0, 1 },
	{ 1, 2 },
	{ 2, 3 },
	{ 3, 0}
    };

int Line2Edges[][4] =
    {	{ },
	{ 3, 0 },
	{ 0, 1 },
	{ 3, 1 },
	{ 1, 2 },
	{ 3, 0, 1, 2 },
	{ 0, 2 },
	{ 3, 2 },
	{ 2, 3 },
	{ 2, 0 },
	{ 0, 1, 2, 3 },
	{ 2, 1 },
	{ 1, 3 },
	{ 1, 0 },
	{ 0, 3 },
	{ }
    };
int NumLines[] = { 0, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0 };
// clang-format on
}  // anonymous namespace

std::deque<std::pair<double, double>> march4(int xbase, int ybase,
                                              double* data4,
                                              double IsoValue = 0.0)
{
    std::deque<std::pair<double, double>> poly;

    int Case = 0;
    for (int i = 0; i < 4; i++) Case |= (data4[i] > IsoValue) << i;
    //    mexPrintf("The case: %d, from %f %f %f %f", Case, data4[0], data4[1],
    //    data4[2], data4[3]);

    for (int line = 0; line < NumLines[Case]; line++) {
        int* edges = &Line2Edges[Case][line * 2];
        for (int i = 0; i < 2; i++) {
            int* vertex = Edge2Vertices[edges[i]];
            double weight = 0;
            if (data4[vertex[0]] != data4[vertex[1]])
                weight = (data4[vertex[0]] - IsoValue) /
                         (data4[vertex[0]] - data4[vertex[1]]);

            std::pair<double, double> v =
                std::make_pair(xbase + (1.0 - weight) * Vertices[vertex[0]][0] +
                                   weight * Vertices[vertex[1]][0],
                               ybase + (1.0 - weight) * Vertices[vertex[0]][1] +
                                   weight * Vertices[vertex[1]][1]);
            poly.push_back(v);
        }
        poly.push_back(
            std::make_pair(std::numeric_limits<double>::quiet_NaN(),
                           std::numeric_limits<double>::quiet_NaN()));
    }
    return poly;
}

std::deque<std::pair<double, double>> march(const Array2<double>& data,
                                             double IsoValue = 0.0)
{
    std::deque<std::pair<double, double>> result;
    for (int c = 0, lastCol = data.dim2() - 1; c < lastCol; c++)
        for (int r = 0, lastRow = data.dim1() - 1; r < lastRow; r++) {
            Index2 ind{r, c};
            double data4[] = {data(ind), data(ind.e()), data(ind.ne()),
                              data(ind.n())};
            // in march4 the column corresponds to x-coord and row to y-coord,
            // therefore the switch
            std::deque<std::pair<double, double>> lines =
                march4(c, r, data4, IsoValue);
            result.insert(result.end(), lines.begin(), lines.end());
        }
    return result;
}

}  // namespace distance
