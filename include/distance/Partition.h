// -*- c++ -*-
#pragma once

#include "Array2.h"
#include "FloodFill.h"

#include <set>
namespace distance {

struct SplitStruct
{
    int splits, creates;
    const double* xyarr;  // immutable data pointer
    int size[2];          // should be [2,x]
};


struct tform
{
    std::size_t nx, ny;
    double xmin, ymin;
    double xincr, yincr;
    aabb bound() const
    {
        return aabb(Vec2d(xmin, ymin),
                    Vec2d(xmin + xincr * (nx + 1), ymin + yincr * (ny + 1)));
    }
    Vec2d w2l(const Vec2d& w) const
    {
        return Vec2d((w.x - xmin) / xincr, (w.y - ymin) / yincr);
    }
    Vec2d l2w(const Vec2d& l) const
    {
        return Vec2d(l.x * xincr + xmin, l.y * yincr + ymin);
    }
};

Array2<double> split(const CubicBezierInterpolant<Vec2d>& polybez,
                     const tform& trans);
void partition1(Array2<int>& part, const Array2<double>& splitGrid, int splits,
                int creates);

// ================================================================
template <typename Iter>
inline void partition(Array2<int>& part, Iter curve_begin, Iter curve_end,
                      const tform& trans)
{
    partition_p(part, curve_begin, curve_end, trans);
}
// ================================================================
inline void partition(Array2<int>& part,
                      const CubicBezierInterpolant<Vec2d>& curve, int splits,
                      int creates, const tform& trans)
{
    const aabb larger =
        trans.bound().grow(2 * std::max(trans.xincr, trans.yincr));

    partition1(part, split(curve.extrapolated(larger, 1e-3), trans), splits,
               creates);
}

// ================================================================
template <typename Iter>
inline void partition_p(Array2<int>& part, Iter curve_begin, Iter curve_end,
                        const tform& trans)
{
    // std::vector<std::future<Array2<double>>> splitGrids;
    // std::vector<std::pair<int, int>> splitDirectives;
    std::vector<Array2<double>> res;
    std::vector<std::pair<int, int>> splitDirectives;

    const aabb larger =
        trans.bound().grow(2 * std::max(trans.xincr, trans.yincr));
    int workSize = std::distance(curve_begin, curve_end);
#pragma omp parallel for
    for (int i = 0; i < workSize; i++) {
        auto it = curve_begin + i;
        const Vec2d* points = reinterpret_cast<const Vec2d*>(it->xyarr);
        CubicBezierInterpolant<Vec2d> polybez =
            CubicBezierInterpolant<Vec2d>(points, points + it->size[1])
                .extrapolated(larger, 1e-3);
        ;
        //=
        //    CubicBezierInterpolantCreator(it->xyarr, it->size[0], it->size[1])
        //                .extrapolated(larger, 1e-3);

        splitDirectives.emplace_back(std::make_pair(it->splits, it->creates));

        res.emplace_back(split(polybez, trans));
    }

    for (int i = 0; i < workSize; i++) {
        const Array2<double>& split = res.at(i);
        auto splitDirective = splitDirectives.at(i);
        partition1(part, split, splitDirective.first, splitDirective.second);
    }
}
// ================================================================
inline Array2<double> split(const CubicBezierInterpolant<Vec2d>& polybez,
                            const tform& trans)
{
    Array2<double> splitGrid(trans.nx, trans.ny);
    std::fill(splitGrid.begin(), splitGrid.end(),
              -std::numeric_limits<double>::max());

    std::deque<std::pair<int, int>> startPos;

    // now visit neighborhood around the curve
    for (const CubicBezier<Vec2d>& segment : polybez.beziers()) {
        std::array<Vec2d, 4> data = segment.coeffs();
        for (auto& v : data) {
            v = trans.w2l(v);  // world to grid coord
        }

        BezierVisitor<Vec2d> visitor(CubicBezier<Vec2d>(data) + Vec2d(.5, .5));
        // the projection is costly, so keep track of alreday visited points
        // for the current segment and avoid redundant computations
        std::set<Index2> visited;
        while (visitor.hasNext()) {
            Index2 coord(visitor.next());
            Index2 nbs[] = {coord.nw(), coord.w(), coord.sw(),
                            coord.n(),  coord,     coord.s(),
                            coord.ne(), coord.e(), coord.se()};
            for (const Index2& nb : nbs) {
                if (visited.count(nb)) continue;
                if (!splitGrid.bound(nb)) continue;
                visited.insert(nb);
                // convert grid point back to world
                Vec2d worldPos = trans.l2w(Vec2d(nb.i, nb.j));
                Vec2d projPos;
                double t;
                std::tie(t, projPos) =
                    ProjectOnBezier(worldPos, segment.coeffs(), 1e-3);

                Vec2d vec(worldPos - projPos);
                double len2 = vec.length2();

                if (len2 < std::abs(splitGrid(nb))) {
                    Vec2d d1 = segment.eval1(t);
                    double sign = cross(d1, vec) >= 0 ? 1.0 : -1.0;
                    splitGrid(nb) = sign * len2;
                    if (sign == 1) {
                        startPos.push_back({nb.i, nb.j});
                    }
                }
            }
        }
    }

    // It is not enough to 'seed' (start) from startPos since
    // points may have been pushed several times with different
    // signs. make sure that we only start from positive points
    FloodFill(
        splitGrid, startPos.begin(), startPos.end(),
        [=](double s) { return s >= 0; },  // start predicate
        [=](double s) { return s == -std::numeric_limits<double>::max(); },
        [=](double) { return 1; });

    // now, grid points on the positive side of the interface (but
    // outside of the narrow band) are flood filled to +1,

    // the narrow band contains points have a positive distance
    // value, and points which have a negative signed distance
    // value.  all other points (which shoule be on the 'negative'
    // side) are negative: -numeric_limits<double>::max()

    // the wanted split region is the union of all positive values
    return splitGrid;
}

// ================================================================
inline void partition1(Array2<int>& partitionGrid,
                       const Array2<double>& splitGrid, int splits, int creates)
{
    // the wanted split region is the union of all positive values
    for (unsigned int i = 0; i < partitionGrid.count(); i++) {
        if (partitionGrid(i) == splits && splitGrid(i) >= 0)
            partitionGrid(i) = creates;
    }

#if 0
        // ALTERNATIVE SPLIT BEHAVIOUR Instead of flood filling on a
        // seperate buffer, we can fill on the partition directly, but
        // then only regions that are directly in contact with the
        // interface are split
        // Some more book keeping would then be needed to keep track of
        // the sidedness of the initializer points

#endif
}

}  // namespace distance
