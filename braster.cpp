#include <algorithm>
#include "mex.h"

#include "matrix.h" // for mwSize
#include <cmath> // for floor/ceil
#include <cassert> // for assert
#include <limits>
#include <vector>
#include "Array2.h"
#include "Vec2d.h"
#include "Bresenham.h"

#include "../matlabargparser/MatlabArgParser.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  
  DataArg<MatlabType<double> > polyLine("P", DimensionRange(2), "The line segment array: [p1x p2x p3x; p1y p2y p3y]");

  LHSArg<MatlabType<double> > distance("D", DimensionRange(2), "The resulting grid with distance initialized for pixels close to the polyline.");


  MatlabArgParser parser("Bezier segment rasterizer. Fills pixels close to a Bezier with enumeration values.");
  parser.registerRHS(&polyLine);
  
  parser.registerLHS(&distance);
  
  parser.parse(nlhs, plhs, nrhs, prhs);

  int numPoints = polyLine.getSize().dim2;
  mexPrintf("Size of pl: %d %d\n", polyLine.getSize().dim1, polyLine.getSize().dim2);
  mexPrintf("Num points: %d\n", numPoints);
  
  const distance::Vec2d * firstPoint = reinterpret_cast<const distance::Vec2d*>( polyLine.getConstData() );
  const distance::Vec2d * pEnd = firstPoint + numPoints;
  double sx=0, sy=0;
  for(const distance::Vec2d * p = firstPoint; p!=pEnd; p++){
    sx = std::max(sx, p->x);
    sy = std::max(sy, p->y);
    if(p->x < 0 || p->y < 0)
      mexErrMsgTxt("No negative points allowed. Aborting.\n");
  }
  //char buffer[128];
  mexPrintf("Grid size: %dx%d.\n", int(sx), int(sy));
  //  Size gSize(sx, sy);
  Size gSize(20, 20);
  distance::Array2<double> data(gSize.dim1, gSize.dim2);
  std::fill(data.begin(), data.end(), std::numeric_limits<double>::quiet_NaN());
  
  const distance::Vec2d & p1 = *firstPoint;
  const distance::Vec2d & p2 = *(firstPoint+1);
  const distance::Vec2d & p3 = *(firstPoint+2);
  distance::BezierSegmentVisitor bsv(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
  int enumerate = 0;
  //  while(bsv.hasNext()) {
  //    bsv.next();

#if 0 
  // void plotQuadBezierSeg(int x0, int y0, int x1, int y1, int x2, int y2)
  {
    int x0 = p1.x;
    int y0 = p1.y;
    int x1 = p2.x;
    int y1 = p2.y;
    int x2 = p3.x;
    int y2 = p3.y;

    int sx = x2-x1, sy = y2-y1;
    long xx = x0-x1, yy = y0-y1, xy;    /* relative values for checks */
    double dx, dy, err, cur = xx*sy-yy*sx;    /* curvature */
    assert(xx*sx <= 0 && yy*sy <= 0);    /* sign of gradient must not change */
    
    if (sx*(long)sx+sy*(long)sy > xx*xx+yy*yy) { /* begin with longer part */
      x2 = x0; x0 = sx+x1; y2 = y0; y0 = sy+y1; cur = -cur; /* swap P0 P2 */
    }
    if (cur != 0) {      /* no straight line */
      xx += sx; xx *= sx = x0 < x2 ? 1 : -1; /* x step direction */
      yy += sy; yy *= sy = y0 < y2 ? 1 : -1; /* y step direction */
      xy = 2*xx*yy; xx *= xx; yy *= yy;      /* differences 2nd degree */

      if (cur*sx*sy < 0) {	/* negated curvature? */
	xx = -xx; yy = -yy; xy = -xy; cur = -cur;
      }
      dx = 4.0*sy*cur*(x1-x0)+xx-xy;      /* differences 1st degree */
      dy = 4.0*sx*cur*(y0-y1)+yy-xy;
      xx += xx; yy += yy; err = dx+dy+xy;      /* error 1st step */

      do {
	std::cout << "pos:" << x0 << " " << y0 << std::endl;
	data(x0, y0) = enumerate++;
	//setPixel(x0,y0);	/* plot curve */
	if (x0 == x2 && y0 == y2) goto next;	/* last pixel -> curve finished */
	y1 = 2*err < dx;	/* save value for test of y step */
	if (2*err > dy) { x0 += sx; dx -= xy; err += dy += yy; } /* x step */
	if (y1)         { y0 += sy; dy -= xy; err += dx += xx; } /* y step */
      } while (dy < 0 && dx > 0);      /* gradient negates -> algorithm fails */
    }
    //plotLine(x0,y0, x2,y2); /* plot remaining part to end */
    std::cout << "end: " << x0 << ' ' << y0  << ' '<< x2 << ' ' << y2 << std::endl; 
  }
next:
#endif
   do {
     std::pair<int, int> pos = bsv.curr();
     using namespace distance;
     std::cout << "pos:" << pos << std::endl;
     data(pos.first, pos.second) = enumerate++;
     bsv.next();
   } while(bsv.hasNext());
    //  }
  
  
  distance.setSize(Size(data.dim1(), data.dim2()));
  distance.allocateData();
  std::copy(data.begin(), data.end(), distance.getData());

}

