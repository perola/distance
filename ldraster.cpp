#include <algorithm>
#include "mex.h"

#include "matrix.h" // for mwSize
#include <cmath> // for floor/ceil
#include <cassert> // for assert
#include <limits>
#include <vector>
#include "Array2.h"
#include "Vec2d.h"
#include "LineDistanceRaster.h"

#include "../matlabargparser/MatlabArgParser.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  
  DataArg<MatlabType<double> > bbox("B", DimensionRange(2), "The bounding box of the computational domain.");
  DataArg<MatlabType<double> > polyLine("P", DimensionRange(2), "The line segment array: [p1x p2x p3x; p1y p2y p3y]");
  DataArg<MatlabType<double> > gridSize("S", DimensionRange(2), "The size of the computational grid.");

  LHSArg<MatlabType<double> > distance("D", DimensionRange(2), "The resulting grid with distance initialized for pixels close to the polyline.");


  MatlabArgParser parser("Polyline distance value rasterizer. Fills pixels close to a polyline with distance values.");
  parser.registerRHS(&bbox);
  parser.registerRHS(&polyLine);
  parser.registerRHS(&gridSize);
  
  parser.registerLHS(&distance);
  
  parser.parse(nlhs, plhs, nrhs, prhs);


  // 1. Create a grid, initialize to not-visited
  // 2. Rasterize all line-segments onto the grid
  // 3. Return grid

  // 2. For each line in line-segments
  //   a. For each grid point in the line's "extruded bounding box, not aa?"
  //     1. Compute exact distance, d, from the ray to the grid point
  //     2. Examine current value att grid point, dc
  //     3. if |d| < |dc| then dc = d
  //   b. For each end point in the line segment
  //     ... do the same ...
  Size gSize(*gridSize.getConstData(), *(gridSize.getConstData()));
  distance::Array2<double> data(gSize.dim1, gSize.dim2);
  //std::fill(data.begin(), data.end(), std::numeric_limits<double>::quiet_NaN());
  std::fill(data.begin(), data.end(), std::numeric_limits<double>::max());

  int numPoints = polyLine.getSize().dim2;
  mexPrintf("Size of pl: %d %d\n", polyLine.getSize().dim1, polyLine.getSize().dim2);
  mexPrintf("Num points: %d\n", numPoints);

  const distance::Vec2d * firstPoint = reinterpret_cast<const distance::Vec2d*>( polyLine.getConstData() );
  const distance::Vec2d * pEnd = firstPoint + numPoints;
  
  std::vector<distance::Vec2d> lineNormals;
  lineNormals.reserve(numPoints-1);

  for( const distance::Vec2d * curr = firstPoint, *next = firstPoint+1;
       next < pEnd; curr++, next++ )
    {
      lineNormals.push_back( (*next - *curr).normalized().orthogonal() );
      mexPrintf("%f %f;...\n", lineNormals.back().x, lineNormals.back().y);  
    }
  
  std::vector<distance::Vec2d> pointNormals;
  pointNormals.reserve(numPoints);
  pointNormals.push_back(lineNormals.front());

  mexPrintf("\n");  
  mexPrintf("%f %f;...\n", pointNormals.back().x, pointNormals.back().y);  
  for(std::vector<distance::Vec2d>::const_iterator it = lineNormals.begin();
      (it+1) != lineNormals.end(); ++it){
    pointNormals.push_back( (*it + *(it+1)).normalized());
    mexPrintf("%f %f;...\n", pointNormals.back().x, pointNormals.back().y);  
  }
  pointNormals.push_back(lineNormals.back());
  
  mexPrintf("%f %f\n", pointNormals.back().x, pointNormals.back().y);
  mexPrintf("PNormals: %d\n", pointNormals.size());
  mexPrintf("LNormals: %d\n", lineNormals.size());
  mexPrintf("Points: %d\n", numPoints);

  // loop over line segments
  distance::Vec2d * firstLineNormal = &lineNormals.at(0);
  for( const distance::Vec2d * curr = firstPoint, *next = firstPoint+1;
       next < pEnd; curr++, next++ )
    {
      const distance::Vec2d& p0 = *curr;
      const distance::Vec2d& p1 = *next;
      
      //plotLineWidth(x0, y0, x1, y1, 6, data);
      //plotLine(x0, y0, x1, y1, data);
      //fillBBox(x0, y0, x1, y1, data);
      //fillWideBBox(x0, y0, x1, y1, 8, data);
      //fillWideBBoxSegmented(x0, y0, x1, y1, 6, data);
      //computeInBBox(x0, y0, x1, y1, data);
      //computeInWideBBox(x0, y0, x1, y1, 3, data);
      //computeInWideBBoxSegmented(p0, p1, 3, data);
      //wideBBoxSegmentedDistanceRaster(p0, p1, 3, data);
      bresenhamFillRaster2(p0, p1, 3, data);
    }

  
  // loop over interior points, with prev and next line normals
  for(const distance::Vec2d* p = firstPoint+1, 
	* ln0 = firstLineNormal, * ln1 = firstLineNormal+1,
	* secondLastPoint = firstPoint + numPoints - 1;
      p < secondLastPoint;
      p++, ln0++, ln1++)
    {
      mexPrintf("%d %f %f\n", std::distance(firstPoint, p), ln0->x, ln0->y);
      //fillForMidPoint(*p, *ln0, *ln1, 5, data);
      wedgeDistanceRaster(*p, *ln0, *ln1, 3, data);
    }

  // visit end points

  
  distance.setSize(Size(data.dim1(), data.dim2()));
  distance.allocateData();
  std::copy(data.begin(), data.end(), distance.getData());

}

