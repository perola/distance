#include <algorithm>                    // for fill
#include <assert.h>                     // for assert
#include <deque>                        // for deque
#include <fstream>                      // IWYU pragma: keep
#include <iostream>                     // for operator<<, basic_ostream, etc
#include <limits>                       // for numeric_limits
#include <stdexcept>                    // for runtime_error
#include <string>                       // for string, getline, operator+, etc
#include <sstream>                      // IWYU pragma: keep
#include <vector>                       // for vector, etc
#include "Array2.h"                     // for Array2
#include "LineDistanceRaster.h"         // for rasterizePoint, etc
#include "Vec2d.h"                      // for Vec2d, operator+, operator-


struct Size{
  int dim1, dim2;
  Size(int a=0, int b=0) : dim1(a), dim2(b) {}
  bool operator!=(const Size& other) const { return dim1 != other.dim1 || dim2 != other.dim2; }
  bool isValid() const { return dim1>0 && dim2>0; }
  std::string toString() const { return std::to_string(dim1) + 'x' + std::to_string(dim2); }
};

class MalformedPolyLineFormatError : public std::runtime_error {
public:
  MalformedPolyLineFormatError(const std::string &what)
    : runtime_error("Malformed line: " + what)
  {}
};


struct PolyLineReader{
  std::deque<std::vector<distance::Vec2d>> polyLines;

  PolyLineReader(std::istream &is)  {

    for (std::string linebuf; std::getline(is, linebuf); ) {
      if(polyLines.empty() || (linebuf.empty() && !polyLines.back().empty()))
	polyLines.push_back(std::vector<distance::Vec2d>()); // allocate new
      
      std::istringstream iss(linebuf);
      ws(iss); // eat space
      if(iss.eof()) // skip if empty
	continue;

      double x = std::numeric_limits<double>::quiet_NaN(), y = std::numeric_limits<double>::quiet_NaN();
      if( ! (iss >> x >> y) || isnan(x) || isnan(y) ) {
	throw MalformedPolyLineFormatError(linebuf);
      }
      ws(iss); // eat any whitespace 
      if(!iss.eof()) // don't want any crap behind numbers
	throw MalformedPolyLineFormatError(linebuf);

      polyLines.back().push_back(distance::Vec2d(x,y));
    }
    
    // discard empty bins
    if(polyLines.back().empty())
      polyLines.pop_back();
  }
};

void rasterizePolyLine(const std::vector<distance::Vec2d> &line, distance::Array2<double> &grid){

  if(line.empty()) {
    assert(false);
    return;
  }

  if(line.size() == 1)
    return rasterizePoint(line.front(), 3, grid);

  const distance::Vec2d * firstPoint = &line.front();
  const distance::Vec2d * pEnd = firstPoint + line.size();
  
  std::vector<distance::Vec2d> lineNormals;
  lineNormals.reserve(line.size()-1);

  for( const distance::Vec2d * curr = firstPoint, *next = firstPoint+1;
       next < pEnd; curr++, next++ ){
    lineNormals.push_back( (*next - *curr).normalized().orthogonal() );
  }
  
  std::vector<distance::Vec2d> pointNormals;
  pointNormals.reserve(line.size());
  pointNormals.push_back(lineNormals.front());

  for(std::vector<distance::Vec2d>::const_iterator it = lineNormals.begin();
      (it+1) != lineNormals.end(); ++it){
    pointNormals.push_back( (*it + *(it+1)).normalized());
  }
  pointNormals.push_back(lineNormals.back());
  
  // loop over line segments
  distance::Vec2d * firstLineNormal = &lineNormals.at(0);
  for( const distance::Vec2d * curr = firstPoint, *next = firstPoint+1;
       next < pEnd; curr++, next++ )
    {
      const distance::Vec2d& p0 = *curr;
      const distance::Vec2d& p1 = *next;
      
      //plotLineWidth(x0, y0, x1, y1, 6, grid);
      //plotLine(x0, y0, x1, y1, grid);
      //fillBBox(x0, y0, x1, y1, grid);
      //fillWideBBox(x0, y0, x1, y1, 8, grid);
      //fillWideBBoxSegmented(x0, y0, x1, y1, 6, grid);
      //computeInBBox(x0, y0, x1, y1, grid);
      //computeInWideBBox(x0, y0, x1, y1, 3, grid);
      //computeInWideBBoxSegmented(p0, p1, 3, grid);
      //wideBBoxSegmentedDistanceRaster(p0, p1, 3, grid);
      bresenhamFillRaster2(p0, p1, 3, grid);
    }

  
  // loop over interior points, with prev and next line normals
  for(const distance::Vec2d* p = firstPoint+1, 
	* ln0 = firstLineNormal, * ln1 = firstLineNormal+1,
	* secondLastPoint = firstPoint + line.size() - 1;
      p < secondLastPoint;
      p++, ln0++, ln1++)
    {
      wedgeDistanceRaster(*p, *ln0, *ln1, 3, grid);
    }

  // visit end points


}




int main(int argc, char *argv[]){

  // argv[1] width
  // argv[2] line segment file in
  // argv[3] grid file out
  
  if (argc < 3 || argc > 4) {
    std::cerr << "Usage: " << argv[0] << " w polyline-file-in [grid-file-out]" << std::endl;
    return 1;
  }

  std::fstream output; // not valid
  if(argc == 4){
    output.open(argv[3], std::ios_base::out);
  }
  
  if (output.bad())
    return 2;

  double width = std::stof(std::string(argv[1])); // may throw

  const char * inFilename = argv[2];
  std::ifstream is(inFilename, std::ios_base::in);
  std::cout << "inFilename: '" << inFilename << "'" << std::endl;
  
  if (!is.good())
    return 2;

  std::deque<std::vector<distance::Vec2d>> polyLines;

  try {
    polyLines = PolyLineReader(is).polyLines;
  }
  catch(MalformedPolyLineFormatError &err) {
    std::cerr << err.what() << std::endl;
    return 3;
  }
  
  distance::BresenhamVisitor bvt;
  distance::BresenhamVisitor bvt2(1, 1, 1, 1);
  assert(bvt2.hasNext());
  {
    using namespace distance;
    std::cout << "*_" << bvt2.curr() << std::endl;
    std::cout << "*:" << bvt2.next() << std::endl;
  }
  assert(bvt2.curr()==std::make_pair(1,1));
  

  //  Size gSize(99,103);
  Size gSize(10000,10000);
  distance::Array2<double> data(gSize.dim1, gSize.dim2);
  std::fill(data.begin(), data.end(), std::numeric_limits<double>::quiet_NaN());
  
  for(auto l : polyLines)
    rasterizePolyLine(l, data);

  if(output.is_open())
    output.write(reinterpret_cast<const char *>(data.begin()), data.dim1()*data.dim2()*sizeof(double));
  else // write to cout
    std::cout.write(reinterpret_cast<const char *>(data.begin()), data.dim1()*data.dim2()*sizeof(double));

}

