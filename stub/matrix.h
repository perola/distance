#ifndef stub_matrix_h
#define stub_matrix_h

#include "../Array2.h"
#include <cstdint>

typedef enum
  {
    mxUNKNOWN_CLASS = 0,
    mxCELL_CLASS,
    mxSTRUCT_CLASS,
    mxLOGICAL_CLASS,
    mxCHAR_CLASS,
    mxVOID_CLASS,
    mxDOUBLE_CLASS,
    mxSINGLE_CLASS,
    mxINT8_CLASS,
    mxUINT8_CLASS,
    mxINT16_CLASS,
    mxUINT16_CLASS,
    mxINT32_CLASS,
    mxUINT32_CLASS,
    mxINT64_CLASS,
    mxUINT64_CLASS,
    mxFUNCTION_CLASS,
    mxOPAQUE_CLASS,
    mxOBJECT_CLASS, /* keep the last real item in the list */
#if defined(_LP64) || defined(_WIN64)
    mxINDEX_CLASS = mxUINT64_CLASS,
#else
    mxINDEX_CLASS = mxUINT32_CLASS,
#endif
    /* TEMPORARY AND NASTY HACK UNTIL mxSPARSE_CLASS IS COMPLETELY ELIMINATED */
    mxSPARSE_CLASS = mxVOID_CLASS /* OBSOLETE! DO NOT USE */
  }
  mxClassID;
/**
 * Indicates whether floating-point mxArrays are real or complex.
 */
typedef enum
{
    mxREAL,
    mxCOMPLEX
}
mxComplexity;

using mwSize = int;

struct mxArray {
  std::vector<char> data;
  std::vector<mwSize> dim;
  mxClassID type;
  mxComplexity complex;
  mxArray(mwSize dimCount, const mwSize * dims, mxClassID type, mxComplexity complex) 
    : data(0), dim(dims, dims+dimCount), type(type), complex(complex) {
      mwSize typeSize = 0;
      switch(type){
      case mxDOUBLE_CLASS:
	typeSize = sizeof(double); break;
      case mxSINGLE_CLASS:
	typeSize = sizeof(float); break;
      case mxINT32_CLASS:
	typeSize = sizeof(int32_t); break;
      case mxUINT32_CLASS:
	typeSize = sizeof(uint32_t); break;
      default:
	assert(false && "False");
      }
      data.resize(typeSize*getNumberOfElements());
  } 
  mwSize getNumberOfElements() const { return std::accumulate(dim.begin(), dim.end(), 1, std::multiplies<mwSize>()); }
};

inline mwSize mxGetNumberOfDimensions(const mxArray * arr) { return arr->dim.size(); }
inline const mwSize * mxGetDimensions(const mxArray * arr) { return &arr->dim.at(0); }
inline mxClassID mxGetClassID(const mxArray * arr) { return mxSINGLE_CLASS; }

/* this nasty in-out const mismatch signature is verbatim from the matlab header (matrix.h). */
inline double * mxGetPr(const mxArray * arr) { return reinterpret_cast<double *>(&const_cast<mxArray*>(arr)->data.at(0)); }

inline void mxFree(void *) {}
inline const char * mxArrayToString(const mxArray *arr) { return nullptr; }
inline mxArray * mxCreateNumericArray(mwSize ndims, const mwSize * dims, mxClassID id, mxComplexity complex) {
  mxArray * ret = new mxArray(ndims, dims, id, complex);
  return ret;
}
inline const char * mxGetClassName(const mxArray * arr) {
  switch(arr->type){
  case mxDOUBLE_CLASS:
  return "mxDOUBLE_CLASS";
  case mxSINGLE_CLASS:
  return "mxSINGLE_CLASS";
  case mxINT32_CLASS:
  return "mxINT32_CLASS";
  case mxUINT32_CLASS:
  return "mxUINT32_CLASS";
  default:
    assert(false && "not implemented");
  }
}
inline mwSize mxGetNumberOfElements(const mxArray * arr) { return arr->getNumberOfElements(); }

#endif
