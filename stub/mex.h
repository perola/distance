#ifndef stub_mex_h
#define stub_mex_h

#include "matrix.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

inline void mexPrintf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}
inline void mexErrMsgTxt(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    exit(1);
}
inline const char * mexFunctionName() { return "main_stub"; }

extern void mexFunction(int, mxArray**, int, const mxArray**);


#endif
