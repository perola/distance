#include <iostream>                     // for operator<<, basic_ostream, etc
#include <memory>                       // for unique_ptr
#include <sstream>                      // IWYU pragma: keep
#include <fstream>                      // IWYU pragma: keep
#include <string>                       // for char_traits, getline, etc
#include "Array2.h"                     // for Array2
#include "FastMarching.h"               // for FastMarching


int main(int argc, const char *argv[]){

  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
    return -1;
  }

  const char * filename = argv[1];
  std::ifstream is(filename, std::ios_base::in);
  std::cout << "filename: '" << filename << "'" << std::endl;

  if (!is.good())
    return 1;

  is.seekg(0, is.end);
  int numBytes = is.tellg();
  is.seekg (0, is.beg);
  std::cout << "numBytes: " << numBytes << std::endl;  

  std::string linebuf;
  std::getline(is, linebuf);
  int magicNumber = 0;
  std::istringstream iss(linebuf);
  if (!(iss >> magicNumber) || magicNumber != 251)
    return 2;
  std::cout << "magicNumber: '" << linebuf << "'" << std::endl;  

  std::getline(is, linebuf);
  iss.str(linebuf); // set to new data
  iss.clear();
  unsigned int dim1 = 0, dim2 = 0;
  if ( !(iss >> dim1 >> dim2) ) {
    std::cout << linebuf << "->" << dim1 << ", " << dim2 << std::endl;
    return 3;
  }
  std::cout << "dim1: " << dim1 << ", dim2: " << dim2 << std::endl;  

  std::getline(is, linebuf);
  iss.str(linebuf); // set to new data
  iss.clear();
  double d1 = 0, d2 = 0;
  if ( !(iss >> d1 >> d2) ){ 
    std::cout << linebuf << "->" << d1 << ", " << d2 << std::endl;
    return 4;
  }
  std::cout << "d1: " << d1 << ", d2: " << d2 << std::endl;  
  
  // now we should be at the start of data.
  // test the size of the rest of the data
  int remainingBytes = numBytes - is.tellg();
  if (remainingBytes != (dim1*dim2*sizeof(double)))
    return 5;
  std::cout << "remBytes: " << remainingBytes << std::endl;  
  
  std::unique_ptr<char> bytes(new char[remainingBytes]);
  if(!bytes)
    return 6;

  is.read(bytes.get(), remainingBytes);

  distance::Array2<double> input(dim1, dim2, reinterpret_cast<double*>(bytes.get()));
  
  distance::FastMarching fm(input, d1, d2);
        
  fm.initialize();
  fm.march();
  
  std::cout << "Bye." << std::endl;
  
  return 0;
}
