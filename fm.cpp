
#include <mex.h>                        // for mexPrintf
#include "matrix.h"                     // for mxArray

#include <algorithm>                    // for copy

#include "../matlabargparser/MatlabArgParser.h"  // for MatlabArgParser
#include "../matlabargparser/MatlabArgs.h"  // for DataArg, DimensionRange, etc
#include "../matlabargparser/MatlabArgParser2.h"  
#include "../matlabargparser/MatlabTypes.h"  // for MatlabType, etc
#include "Array2.h"                    // for Array2d
#include "FastMarching.h"               // for FastMarching



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    DataArg<MatlabType<double> > input("I", DimensionRange(2), "Input grid");
    DataArg<MatlabType<double> > dx("delta", DimensionRange(2), "Delta for grid steps along dim1 and dim2 (rows and columns).");
    // StringArg method("method", "method to use");

    LHSArg<MatlabType<double> > distance("D", DimensionRange(2), "Output distances");
    
    MatlabArgParser parser("Distance computation using a fast marching variant");
    parser.registerRHS(&input);
    parser.registerRHS(&dx);
    //parser.registerRHS(&method);

    parser.registerLHS(&distance);
    
    parser.parse(nlhs, plhs, nrhs, prhs);

    distance::Array2<double> distanceGrid(input.getSize().dim1, input.getSize().dim2, input.getConstData());
    distance::FastMarching fm(distanceGrid, *dx.getConstData(), *(dx.getConstData()+1));
        
    fm.initialize();
    fm.march();
    
    distance.setSize(input.getSize());
    distance.allocateData();
    std::copy(distanceGrid.begin(), distanceGrid.end(), distance.getData());
}
