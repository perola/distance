function d = distance2poly(X, P)

assert(size(X,2)==2 & size(P,2)==2)

d = Inf(size(X,1),1);

for k=1:size(P,1)-1
    d = min(d, dist2line(X, P(k:k+1,:)));
end

%%
function d = dist2line(X, L)
d = Inf(size(X,1),1);
v = L(2,:) - L(1,:);
w = X - repmat(L(1,:), size(X,1), 1);

c1 = w*v';
c2 = v*v';

b = c1 ./ c2;
Pb = repmat(L(1,:), size(X,1), 1) + [b .* v(1), b .* v(2)]; % closest points

d(b <= 0) = dist2point(X(b <= 0,:), L(1,:));
d(b > 0 & b < 1) = dist2points(X(b > 0 & b < 1,:), Pb(b > 0 & b < 1,:)); % we know location of closest point, compute dist
d(b >= 1) = dist2point(X(b >= 1,:), L(2,:));


%%
function d = dist2point(X, p)
d = X-repmat(p, size(X,1),1);
d = sqrt(sum(d'.^2)');

%%
function d = dist2points(X, P)
d = X-P;
d = sqrt(sum(d'.^2)');
