#include <mex.h>    // for mexPrintf
#include "matrix.h" // for mxArray

#include "../matlabargparser/MatlabArgParser2.h"
#include "Array2.h"       // for Array2d
#include "FastMarching.h" // for FastMarching

#include <algorithm> // for copy
#include <iterator>  // for distance
#include <string>    //

using namespace map;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  RHSArg<double> input("I", "Input grid", Dimensions(2));
  RHSArg<double> dx(
      "delta", "Delta for grid steps along dim1 and dim2 (rows and columns).",
      Dimensions({1, 2}));
  RHSArg<std::string> method("method", "method to use", Dimensions({1, -1}));

  LHSArg<double> distance("D", "Output distances", Dimensions(2));

  MatlabArgParser parser("Distance computation using a fast marching variant");
  parser.rhs().push_back(&input);
  parser.rhs().push_back(&dx);
  parser.rhs().push_back(&method);
  // parser.registerRHS(&method);

  parser.lhs().push_back(&distance);

  parser.parse(nlhs, plhs, nrhs, prhs);

  distance::Array2<double> distanceGrid(input.getDimensions().dim1(),
                                        input.getDimensions().dim2(),
                                        input.begin());
  distance::FastMarching fm(distanceGrid, *dx.begin(), *(dx.begin() + 1));

  // fm.initialize();
  // fm.march();

  mexPrintf("method: %s\n", std::string(method.begin(), method.end()).c_str());
  mexPrintf("method: %s\n", method.toString().c_str());
  mexPrintf("method size: %d\n", std::distance(method.begin(), method.end()));

  distance.setDimensions(input.getDimensions());
  if (distance.allocate())
    std::copy(distanceGrid.begin(), distanceGrid.end(), distance.begin());
}
