#include <catch2/catch.hpp>
#include <initializer_list>
#include <iostream>
#include <set>
#include <stdexcept>

#include "common.h"
#include "distance/AdjacencyListMesh.h"
#include "distance/Vec2d.h"

using namespace distance;
namespace test {
struct Vec3
{
    double x, y, z;
    Vec3() {}
    Vec3(double x, double y, double z) : x(x), y(y), z(z) {}
    Vec3(std::initializer_list<double> data)
        : x(*data.begin()), y(*(data.begin() + 1)), z(*(data.begin() + 2))
    {
        // assert(data.size() == 3, "Requires 3 data points");
    }
};

Vec3 read_obj_vert(std::istream& is, Vec3& v)
{
    for (int i = 0; i < 3; i++) {
        is >> reinterpret_cast<double*>(&v)[i];
        if (!is) throw std::runtime_error("ReadVert error");
    }
    return v;
}
}  // namespace test

using namespace test;

namespace distance {

template <typename T>
vec2<T> read_obj_vert(std::istream& is, vec2<T>& v)
{
    is >> v.x >> v.y;
    if (!is) throw std::runtime_error("ReadVert error");
    return v;
}
}  // namespace distance

SCENARIO("AdjacencyListMesh is constructed", "[almesh]")
{
    GIVEN("an empty mesh")
    {
        Vec2d* Vit = nullptr;
        Triangle* Tit = nullptr;
        REQUIRE_NOTHROW(AdjacencyListMesh<Vec2d>(Vit, Vit, Tit, Tit));
    }
    GIVEN("a single triangle")
    {
        std::array<Vec2d, 3> verts = {Vec2d{0, 0}, Vec2d{1, 1}, Vec2d{1, 0}};
        Triangle tri(0, 1, 2);
        AdjacencyListMesh<Vec2d> alm = AdjacencyListMesh<Vec2d>(
            verts.begin(), verts.end(), &tri, &tri + 1);
        REQUIRE(alm.getTriangles().size() == 1);
        REQUIRE(alm.getTriangles().front() == tri);
        REQUIRE(alm.getVertices().size() == 3);
        for (auto i : {0, 1, 2}) {
            REQUIRE(alm.getVertices().at(i) == verts.at(i));
        }
    }
#if DISTANCE_HAS_IOSTREAM
    GIVEN("a serialized 2d mesh")
    {
        std::string mesh = R"(
    v 0 0 
    v 1 1 
    v 1 0 
    f 1 2 3
)";
        {
            std::stringstream mis(mesh);
            auto alm = AdjacencyListMesh<Vec2d>::readOBJ(mis);
            REQUIRE(alm->getTriangles().size() == 1);
            REQUIRE(alm->getVertices().size() == 3);
        }
    }
    GIVEN("a serialized 3d mesh")
    {
        std::string mesh = R"(
    v 0 0 0
    v 1 1 0
    v 1 0 0
    f 1 2 3
)";

        {
            std::stringstream mis(mesh);
            auto alm3d = AdjacencyListMesh<Vec3>::readOBJ(mis);
            REQUIRE(alm3d->getTriangles().size() == 1);
            REQUIRE(alm3d->getVertices().size() == 3);
        }
        {
            // The vec2 version just skips the z-coordinate
            std::stringstream mis(mesh);
            auto alm3d = AdjacencyListMesh<Vec2d>::readOBJ(mis);
            REQUIRE(alm3d->getTriangles().size() == 1);
            REQUIRE(alm3d->getVertices().size() == 3);
        }
    }
#endif
}
