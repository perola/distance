#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Bezier.h"
#include "distance/CubicBezier.h"

using namespace distance;

SCENARIO("CubicBeziers and derivatives are computed", "[cubicbezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> coeffs = {{0, .21, 1, 2}};
        CubicBezier<double> bezier(coeffs);

        THEN("Control end points coincide with the f(0) and f(1)")
        {
            REQUIRE(bezier.eval(0) == coeffs[0]);
            REQUIRE(bezier.eval(1) == coeffs[3]);
        }
        THEN("Evaluates to same as BezierEvaluator")
        {
            using Cubic = BezierEvaluator<double, 0, 3>;
            for (int i = 0; i <= 30; i++) {
                double t = i / (i + 1.0);
                // double t = GENERATE(Catch::Generators::between(0.0, 1.0, 1.0
                // / 30));
                // std::cout << "t: " << t << std::endl;
                REQUIRE(
                    bezier.eval(t) ==
                    Approx(Cubic::eval(coeffs.data(), t)).epsilon(bezierEps()));
            }
        }

        THEN("1st derivative")
        {
            using d1Cubic = BezierEvaluator<double, 1, 3>;

            THEN("end point derivatives coincide with the 'control polygon'")
            {
                REQUIRE(bezier.eval1(0) == 3 * (coeffs[1] - coeffs[0]));
                REQUIRE(bezier.eval1(1) == 3 * (coeffs[3] - coeffs[2]));
                REQUIRE(bezier.eval1(.5) == d1Cubic::eval(coeffs.data(), .5));
            }
        }

        THEN("2st derivative is same as BezierEvaluator")
        {
            using d2Cubic = BezierEvaluator<double, 2, 3>;

            REQUIRE(bezier.eval2(0) == d2Cubic::eval(coeffs.data(), 0));
            REQUIRE(bezier.eval2(1) == d2Cubic::eval(coeffs.data(), 1));
            REQUIRE(bezier.eval2(.1) == d2Cubic::eval(coeffs.data(), .1));
        }

        THEN("3rd derivative")
        {
            using d3Cubic = BezierEvaluator<double, 3, 3>;

            THEN("3rd derivative should be constant")
            {
                REQUIRE(bezier.eval3(0) == bezier.eval3(.1));
                REQUIRE(bezier.eval3(.63) ==
                        d3Cubic::eval(coeffs.data(), .8626));
            }
        }
    }
}

SCENARIO("Cubic beziers are invariant under affine transformations",
         "[cubicbezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> coeffs = {{0, 1, 1.1, 2}};
        CubicBezier<double> bezier(coeffs);

        WHEN("control polygon is translated")
        {
            CubicBezier<double> translated = bezier + -1;

            THEN("derivatives don't change")
            {
                for (double t : {0.0, 1.0, 2.0 / 3, .999999}) {
                    REQUIRE(translated.eval1(t) == bezier.eval1(t));

                    REQUIRE(translated.eval2(t) == bezier.eval2(t));
                    REQUIRE(translated.eval3(t) == bezier.eval3(t));
                }
            }
        }
        WHEN("control polygon is scaled")
        {
            auto scale = 3.14;
            CubicBezier<double> scaled = bezier * scale;

            THEN("derivatives are scaled")
            {
                for (double t : {0.0, 1.0, 2.0 / 3, .999999}) {
                    REQUIRE(scaled.eval(t) == scale * bezier.eval(t));
                    REQUIRE(scaled.eval1(t) == Approx(scale * bezier.eval1(t)));
                    REQUIRE(scaled.eval2(t) == Approx(scale * bezier.eval2(t)));
                    REQUIRE(scaled.eval3(t) == Approx(scale * bezier.eval3(t)));
                }
            }
        }
    }
}
