#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Bezier.h"
#include "distance/BezierProject.h"
#include "distance/Vec2d.h"

using namespace distance;

SCENARIO("Projecting point onto bezier", "[bezier][project]")
{
    GIVEN("A cubic bezier")
    {
        std::array<Vec2d, 4> cubic =
            random_array<4>(Vec2d(-10.0, -10), Vec2d(100.0, 100));
        using Cubic = BezierEvaluator<Vec2d, 0, 3>;
        WHEN("projecting a random point")
        {
            Vec2d point =
                random_array<1>(Vec2d(-10, -10), Vec2d(100, 100)).at(0);
            double t = -1;
            Vec2d projected;
            std::tie(t, projected) = ProjectOnBezier(point, cubic, 1e-9);
            THEN("Parameter is in [0,1]")
            {
                REQUIRE(t >= 0.0);
                REQUIRE(t <= 1.0);
                REQUIRE_THAT(Cubic::eval(cubic.data(), t),
                             Vec2dApproxEquals(projected));
            }
        }
    }
}
