// -*- c++ -*-
#pragma once
#include <array>
#include <random>
#include <sstream>
#include <type_traits>

#include "catch2/catch.hpp"
#include "distance/Vec2d.h"
#include "distance/ignore.h"

// #if !(DISTANCE_HAS_IOSTREAM)
// #error("The distance tests require iostream support, compile with
// '-DDISTANCE_HAS_IOSTREAM=1'") #endif

constexpr double bezierEps()
{
    return std::numeric_limits<double>::epsilon() * 250;
}

template <std::size_t N, typename T>
std::array<T, N> random_array(T low, T high)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<T> dist(low, high);

    std::array<T, N> rarray;
    std::generate(rarray.begin(), rarray.end(), [&]() { return dist(mt); });
    return rarray;
}
template <std::size_t N, typename T>
std::array<distance::vec2<T>, N> random_array(distance::vec2<T> low,
                                              distance::vec2<T> high)
{
    std::array<T, N> xs = random_array<N, T>(low.x, high.x);
    std::array<T, N> ys = random_array<N, T>(low.y, high.y);

    std::array<distance::vec2<T>, N> rarray;
    for (std::size_t i = 0; i < N; i++)
        rarray[i] = distance::vec2<T>(xs[i], ys[i]);
    return rarray;
}

template <typename C, typename F>
auto REQUIRE_EQUAL(const C& v1, const C& v2, F&& f, std::string tag = "")
    -> decltype(v1.size(), v1[0], void())
{
#if DISTANCE_HAS_IOSTREAM
    INFO(tag << "C1:  " << v1 << "\n\nC2:  " << v2);
#endif
    INFO(tag);
    REQUIRE(v1.size() == v2.size());
    for (std::size_t i = 0; i < v1.size(); i++) {
        std::forward<F>(f)(v1[i], v2[i]);
    }
}

template <typename T, typename F, typename P>
class GM : public Catch::MatcherBase<T> {
    const T& m_data;
    F m_matchFunc;
    P m_describeFunc;

  public:
    GM(const T& data, F&& f, P&& p)
        : m_data(data), m_matchFunc(std::move(f)), m_describeFunc(std::move(p))
    {
    }

    // loop over items and approx-compare them
    virtual bool match(const T& other) const override
    {
        return m_matchFunc(m_data, other);
    }

    virtual std::string describe() const override
    {
        return m_describeFunc(m_data);
    }
};

// The generic builder function
template <typename T, typename F, typename P>
inline GM<T, F, P> GenericMatches(const T& v, F&& f, P&& p)
{
    return GM<T, F, P>(v, std::forward<F>(f), std::forward<P>(p));
}

// The builder function for collections
template <typename T>
inline auto CollectionApproxEquals(const T& v, Approx a = Approx(0))
{
    return GenericMatches(
        v,
        [a](const T& c1, const T& c2) {
            if (c1.size() != c2.size()) return false;
            for (decltype(c1.size()) i = 0; i < c1.size(); i++) {
                if (!(c1[i] == a(c2[i]))) return false;
            }
            return true;
        },
        [a](const T& c) {
            std::ostringstream os;
            os << "(Collection)ApproxEquals (" << a.toString() << ") ";
            os << "(";
            if (c.size() > 0) {
                os << c[0];
                for (decltype(c.size()) i = 1; i < c.size(); i++) {
                    os << ", " << c[i];
                }
            }
            os << ")";
            return os.str();
        });
}

// The builder function for vec2d
template <typename T>
inline auto Vec2dApproxEquals(const distance::vec2<T>& v, Approx a = Approx(0))
{
    return GenericMatches(
        v,
        [a](const distance::vec2<T>& v1, const distance::vec2<T>& v2) {
            return v1.x == a(v2.x) && v1.y == a(v2.y);
        },
        [a](const distance::vec2<T>& v) {
#if DISTANCE_HAS_IOSTREAM
            std::ostringstream os;
            os << "ApproxEquals (" << a.toString() << ")" << v;
            return os.str();
#else
            distance::ignore(v);
            distance::ignore(a);
            return "Vec2d::ApproxEquals";
#endif
        });
}
