#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Bezier.h"

using namespace distance;

SCENARIO("Cubic beziers and derivatives are computed", "[bezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> cubic = random_array<4>(-10.0, 100.0);
        using Cubic = BezierEvaluator<double, 0, 3>;

        THEN("Control end points coincide with the f(0) and f(1)")
        {
            REQUIRE(Cubic::eval(cubic.data(), 0) == cubic[0]);
            REQUIRE(Cubic::eval(cubic.data(), 1) == cubic[3]);
        }

        WHEN("1st derivative")
        {
            using d1Cubic = BezierEvaluator<double, 1, 3>;

            THEN("end point derivatives coincide with the 'control polygon'")
            {
                REQUIRE(d1Cubic::eval(cubic.data(), 0) ==
                        3 * (cubic[1] - cubic[0]));
                REQUIRE(d1Cubic::eval(cubic.data(), 1) ==
                        3 * (cubic[3] - cubic[2]));
            }
        }

        WHEN("3rd derivative")
        {
            using d3Cubic = BezierEvaluator<double, 3, 3>;

            THEN("3rd derivative should be constant")
            {
                REQUIRE(d3Cubic::eval(cubic.data(), 0) ==
                        d3Cubic::eval(cubic.data(), .1));
                REQUIRE(d3Cubic::eval(cubic.data(), .2) ==
                        d3Cubic::eval(cubic.data(), .00001));
                REQUIRE(d3Cubic::eval(cubic.data(), .63) ==
                        d3Cubic::eval(cubic.data(), .965));
            }
        }
        THEN("4th derivative should be zero")
        {
            using d4Cubic = BezierEvaluator<double, 4, 3>;
            REQUIRE(d4Cubic::eval(cubic.data(), 1.0 / 3) == 0);
        }
    }
}

SCENARIO("Hodograph", "[bezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> cubic = random_array<4>(0.0, 1.0);

        WHEN("Computing the hodograph")
        {
            std::array<double, 3> hodograph;
            BezierEvaluator<double, 0, 3>::hodograph(cubic.data(),
                                                     hodograph.data());

            using dCubic = BezierEvaluator<double, 1, 3>;
            using Quadratic = BezierEvaluator<double, 0, 2>;
            THEN("The 1st derivative coincides")
            {
                for (double t : {0.0, .222, .111, 2.0 / 6, 1.0}) {
                    REQUIRE(dCubic::eval(cubic.data(), t) ==
                            Approx(Quadratic::eval(hodograph.data(), t))
                                .epsilon(bezierEps()));
                }
            }
        }
    }
}

SCENARIO("Affine parameter transform invariance", "[bezier]")
{
    GIVEN("A quartic bezier")
    {
        std::array<double, 5> quartic = random_array<5>(0.0, 1.0);

        WHEN("Reversing the bezier")
        {
            std::array<double, 5> reversed = {
                {quartic[4], quartic[3], quartic[2], quartic[1], quartic[0]}};

            using Quartic = BezierEvaluator<double, 0, 4>;
            using dQuartic = BezierEvaluator<double, 1, 4>;
            using d2Quartic = BezierEvaluator<double, 2, 4>;
            THEN("The reversed evaluation is invariant")
            {
                for (double t : {0.0, .222, .111, 2.0 / 6, 1.0}) {
                    REQUIRE(Quartic::eval(quartic.data(), t) ==
                            Approx(Quartic::eval(reversed.data(), 1 - t))
                                .epsilon(bezierEps()));
                    REQUIRE(dQuartic::eval(quartic.data(), t) ==
                            Approx(-dQuartic::eval(reversed.data(), 1 - t))
                                .epsilon(bezierEps()));
                    REQUIRE(d2Quartic::eval(quartic.data(), t) ==
                            Approx(d2Quartic::eval(reversed.data(), 1 - t))
                                .epsilon(bezierEps()));
                }
            }
        }
    }
}

namespace Catch {
template <typename T>
std::vector<T> between(T start, T endInclusive, T stepSize)
{
    std::vector<T> range;
    int i = 0;
    do {
        range.push_back(start + i++ * stepSize);
    } while (range.back() < endInclusive);
    return range;
}
}  // namespace Catch

SCENARIO("Spltting invariance", "[bezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> cubic = random_array<4>(0.0, 1.0);

        WHEN("Splitting at middle")
        {
            std::array<double, 4> leftright[2];
            BezierSplitter<double, 3>::split(cubic.data(), .5,
                                             leftright[0].data());
            THEN("The curve is unchanged")
            {
                for (double t : Catch::between(0.0, 1.0, 1.0 / 22)) {
                    using Cubic = BezierEvaluator<double, 0, 3>;
                    if (t < .5)
                        REQUIRE(Cubic::eval(cubic.data(), t) ==
                                Approx(Cubic::eval(leftright[0].data(), t * 2))
                                    .epsilon(bezierEps()));
                    else
                        REQUIRE(Cubic::eval(cubic.data(), t) ==
                                Approx(Cubic::eval(leftright[1].data(),
                                                   (t - .5) * 2))
                                    .epsilon(bezierEps()));
                }
            }
        }
    }
}

SCENARIO("Monomial roundtrip", "[bezier]")
{
    GIVEN("A cubic bezier")
    {
        std::array<double, 4> cubic = random_array<4>(0.0, 1.0);
        WHEN("converted to monomial")
        {
            std::array<double, 4> monomial =
                BezierConverter<double, 3>::toMonomial(cubic);
            THEN("rep changed but evaluates to same")
            {
                REQUIRE(cubic != monomial);
                auto m3 = [](const std::array<double, 4>& m, double x) {
                    return m[0] + x * m[1] + x * x * m[2] + x * x * x * m[3];
                };
                using Cubic = BezierEvaluator<double, 0, 3>;
                REQUIRE(Cubic::eval(cubic.data(), 0) == m3(monomial, 0));
                REQUIRE(Cubic::eval(cubic.data(), 1.0 / 3) ==
                        Approx(m3(monomial, 1.0 / 3)));
            }
            WHEN("converting back to bezier")
            {
                std::array<double, 4> bezier =
                    BezierConverter<double, 3>::fromMonomial(monomial);
                THEN("should equal start values")
                {
                    REQUIRE_THAT(bezier,
                                 CollectionApproxEquals(cubic, Approx(0)));
                }
            }
        }
    }
}
