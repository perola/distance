#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Array2.h"

using namespace distance;

SCENARIO("Array2 are constructed", "[array2]")
{
    GIVEN("an ampty array2")
    {
        Array2<double> a;
        REQUIRE(a.dim1() == 0);
        REQUIRE(a.dim2() == 0);
        REQUIRE(a.begin() == a.end());
        REQUIRE(a.data_.size() == 0);
        // SIGFPE
        // REQUIRE(a.coord2li(a.li2coord(0).i, a.li2coord(0).j) == 0);
    }
    GIVEN("an empty array2 with one dim nonzero")
    {
        Array2<double> a(0, 3);
        REQUIRE(a.dim1() == 0);
        REQUIRE(a.dim2() == 3);
        REQUIRE(a.begin() == a.end());
        REQUIRE(a.data_.size() == 0);
    }
    GIVEN("a 1x1 array")
    {
        Array2<double> a(1, 1);
        REQUIRE(a.dim1() == 1);
        REQUIRE(a.dim2() == 1);
        REQUIRE(a.begin() != a.end());
        REQUIRE(a.data_.size() == 1);
        REQUIRE(a.coord2li(a.li2coord(0)) == 0);
    }
    GIVEN("a 2x4 array with copied data")
    {
        std::vector<double> data(8, 0);
        Array2<double> a(2, 4, data.data());
        REQUIRE(a.dim1() == 2);
        REQUIRE(a.dim2() == 4);
        REQUIRE(a.begin() != a.end());
        REQUIRE(a.data_.size() == 8);
        for (auto i : {0, 1, 2, 3, 4, 5, 6, 7})
            REQUIRE(a.coord2li(a.li2coord(i)) == i);
    }
}

SCENARIO("Array2 are accessed", "[array2]")
{
    GIVEN("a 2x2 array2")
    {
        std::vector<double> data{1, 2, 3, 4};
        Array2<double> a(2, 2, data.data());
        REQUIRE(a.dim1() == 2);
        REQUIRE(a.dim2() == 2);
        THEN("format is column major")
        {
            REQUIRE(&a(1, 0) == &a(0, 0) + 1);

            REQUIRE(a(0, 0) == 1);
            REQUIRE(a(1, 0) == 2);
            REQUIRE(a(0, 1) == 3);
            REQUIRE(a(1, 1) == 4);
        }
        THEN("references should be captured")
        {
            const double& cref_li = a(0);
            REQUIRE(&cref_li == &a(0));

            const double& cref_ij = a(0, 1);
            REQUIRE(&cref_ij == &a(0, 1));

            const double& cref_c = a(Index2(1, 1));
            REQUIRE(&cref_c == &a({1, 1}));
        }
    }
}

SCENARIO("Array2 is negated", "[array2]")
{
    GIVEN("a 2x2 array2")
    {
        std::vector<double> data{1, 2, 3, 4};
        Array2<double> a(2, 2, data.data());
        auto a2 = -a;
        for (auto i : {
                 0,
                 1,
                 2,
                 3,
             }) {
            REQUIRE(a2(i) == -a(i));
        }
    }
}
