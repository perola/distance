#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/BezierVisitor.h"

using namespace distance;
namespace distance {
#if !DISTANCE_HAS_IOSTREAM
// dummy output
template <template <class> class T, class C>
std::ostream& operator<<(std::ostream& os, const T<C>&)
{
    return os;
}
#endif
template <typename T>
inline std::ostream& operator<<(std::ostream& os,
                                const BezierLinearSegmentVisitor<T>& b)
{
    return os << "BezierLinearSegmentVisitor(" << b.bezier() << ')';
}
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const BezierVisitor<T>& bv)
{
    return os << "BezierVisitor(" << bv.bezier() << ')';
}
}  // namespace distance

SCENARIO("BezierVisitor is constructed", "[beziervisitor]")
{
    GIVEN("default is a singular line (0,0) with one step")
    {
        BezierVisitor<Vec2d> b(CubicBezier<Vec2d>{});
        INFO(b);
        REQUIRE(b.hasNext());
        REQUIRE(b.next() == std::pair<int, int>{0, 0});
        REQUIRE_FALSE(b.hasNext());
    }
    GIVEN("a singular line from a point has one step")
    {
        std::array<Vec2d, 4> zero{
            {Vec2d{0.0, 0}, Vec2d{0, 0}, Vec2d{0, 0}, Vec2d{0, 0}}};
        BezierVisitor<Vec2d> b(CubicBezier<Vec2d>{zero});
        REQUIRE(b.hasNext() == true);
        REQUIRE(b.next() == std::pair<int, int>(0, 0));
        REQUIRE_FALSE(b.hasNext());
    }
}

inline std::pair<int, int> mp(double d1, double d2)
{
    return std::pair<int, int>(std::floor(d1), std::floor(d2));
}

SCENARIO("BezierLinearSegmentVisitor random curves", "[beziervisitor][linear]")
{
    auto coeffs = random_array<8, double>(-1000, 1000);
    CubicBezier<Vec2d> bezier(
        reinterpret_cast<const std::array<Vec2d, 4>&>(coeffs));
    BezierLinearSegmentVisitor<Vec2d> random(bezier);

    REQUIRE(random.hasNext());
    auto curr = random.next();
    REQUIRE(curr.c0() == bezier.c0());

    while (random.hasNext()) {
        auto next = random.next();
        REQUIRE(next != curr);

        // std::cerr << curr << std::endl;
        curr = next;
    }
    REQUIRE(curr.c3() == bezier.c3());
}

SCENARIO("BezierVisitor random curves", "[beziervisitor]")
{
    for (int i = 0; i < 2000; i++) {
        // limit the scale to [-1000, 1000]
        auto coeffs = random_array<8, double>(-1000, 1000);

        auto bezier = CubicBezier<Vec2d>(
            reinterpret_cast<const std::array<Vec2d, 4>&>(coeffs));
        BezierVisitor<Vec2d> random(bezier);
        INFO(random);
        INFO(bezier);

        REQUIRE(random.hasNext());
        const auto firstPos = mp(coeffs[0], coeffs[1]);
        const auto lastPos = mp(coeffs[6], coeffs[7]);

        auto pos = random.next();  // get the first
        REQUIRE(pos == firstPos);

        while (random.hasNext()) {
            auto next = random.next();
            // REQUIRE(next != pos);
            pos = next;
        }
        REQUIRE(pos == lastPos);
        REQUIRE_FALSE(random.hasNext());
    }
}
