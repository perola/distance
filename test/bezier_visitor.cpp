#include <array>
#include <fstream>
#include <iostream>
#include <memory>

#include "distance/BezierVisitor.h"
#include "distance/Bresenham.h"
#include "distance/CubicBezier.h"
#include "distance/dda.h"

#if !DISTANCE_HAS_IOSTREAM
// duplicate som code here
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::pair<T, T>& p)
{
    return (os << '(' << p.first << ", " << p.second << ')');
}
namespace distance {
template <template <class> class T, class C>
std::ostream& operator<<(std::ostream& os, const T<C>&)
{
    return os;
}
}  // namespace distance
#endif

template <class stream_type, bool do_delete>
void stream_deleter(stream_type* stream)
{
    if (do_delete) {
        delete stream;
        std::cerr << "Closing file " << ((void*)stream) << std::endl;
    }
}

using namespace distance;

using SPtr = std::unique_ptr<std::istream, void (*)(std::istream*)>;

bool readBezier(SPtr stream_ptr, CubicBezier<Vec2d>& output)
{
    assert(stream_ptr);
    assert(*stream_ptr);
    std::array<Vec2d, 4> data;
    stream_ptr->read((char*)data.data(), data.size() * sizeof(data[0]));
    output = CubicBezier<Vec2d>(data);
    return !!*stream_ptr;
}

template <typename T>
inline T manhattan(const std::pair<T, T>& p)
{
    return std::abs(p.first) + std::abs(p.second);
}
template <typename T>
inline std::pair<T, T> operator-(const std::pair<T, T>& p1,
                                 const std::pair<T, T>& p2)
{
    return {p1.first - p2.first, p1.second - p2.second};
}

int main(int argc, char* argv[])
{
    std::cerr << "argc: " << argc << std::endl;
    CubicBezier<Vec2d> bezier;

    switch (argc) {
        case 1:
            std::cerr << "Reading cin: " << std::endl;
            if (!readBezier({&std::cin, stream_deleter<std::istream, false>},
                            bezier))
                return -1;
            break;
        case 2:
            std::cerr << "Reading file: " << argv[1] << std::endl;
            if (!readBezier({new std::ifstream(argv[1], std::ios::in),
                             stream_deleter<std::istream, true>},
                            bezier))
                return -1;
            break;
        default:
#if !defined(FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION)
            assert(argc >= 9);
            try {
                std::array<Vec2d, 4> data;
                for (int i = 0; i < 4; i++) {
                    data[i].x = std::stod(argv[i * 2 + 1]);
                    data[i].y = std::stod(argv[i * 2 + 2]);
#if DISTANCE_HAS_IOSTREAM
                    std::cerr << data[i] << " ";
                }
                std::cerr << std::endl;
#else
                }
#endif
                bezier = CubicBezier<Vec2d>(data);
                break;
            } catch (...) {
                std::cerr << " failed to read input, aborting." << std::endl;
            }
#endif
            return -3;
    }

    std::cout << bezier << std::endl;
    if (std::any_of(bezier.coeffs().begin(), bezier.coeffs().end(),
                    [](Vec2d v) {
                        return !(std::isfinite(v.x) && std::isfinite(v.y));
                    }))
        return -1;
    if (std::any_of(bezier.coeffs().begin(), bezier.coeffs().end(),
                    [](Vec2d v) { return v.length() > 1000; }))
        return -2;

    //    BezierVisitor<Vec2d> visitor(bezier);
    CulledBezierVisitor<Vec2d> visitor(bezier);

    assert(visitor.hasNext());
    auto pos = visitor.next();
    auto mp = [](const Vec2d& v) {
        return std::pair<int, int>(std::floor(v.x), std::floor(v.y));
    };
    assert(pos == mp(bezier.c0()));
    std::cout << pos << " ";
    while (visitor.hasNext()) {
        auto next_pos = visitor.next();
        std::cout << next_pos << " ";
        assert(next_pos != pos);
        auto diff = pos - next_pos;
        assert(manhattan(diff) == 1 || manhattan(diff) == 2);
        pos = next_pos;
    }
    std::cout << std::endl;
    assert(pos == mp(bezier.c3()));
    return 0;
}
