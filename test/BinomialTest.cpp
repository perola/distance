#include <catch2/catch.hpp>

#include "distance/Binomial.h"

using namespace distance;

SCENARIO("Using the compile time binomials", "[binomial]")
{
    GIVEN("Some binomials")
    {
        using Bin11 = Binomial<1, 1>;
        REQUIRE(Bin11::value() == 1);

        using Bin23 = Binomial<2, 3>;
        REQUIRE(Bin23::value() == 0);

        using Bin41 = Binomial<4, 1>;
        REQUIRE(Bin41::value() == 4);
    }
}

SCENARIO("Using the compile time binomial tables", "[binomial]")
{
    GIVEN("Some binomials tables")
    {
        REQUIRE(BinomialTable<0>::table.size() == 1);
        REQUIRE(BinomialTable<1>::table.size() == 2);
        REQUIRE(BinomialTable<2>::table.size() == 3);
        REQUIRE(BinomialTable<3>::table.size() == 4);
        REQUIRE(BinomialTable<4>::table.size() == 5);
        REQUIRE(BinomialTable<5>::table.size() == 6);
        REQUIRE(BinomialTable<6>::table.size() == 7);

        REQUIRE(BinomialTable<1>::table[0] == 1);
        REQUIRE(BinomialTable<1>::table[1] == 1);

        REQUIRE(BinomialTable<2>::table[0] == 1);
        REQUIRE(BinomialTable<2>::table[1] == 2);
        REQUIRE(BinomialTable<2>::table[2] == 1);

        REQUIRE(BinomialTable<3>::table[0] == 1);
        REQUIRE(BinomialTable<3>::table[1] == 3);
        REQUIRE(BinomialTable<3>::table[2] == 3);
        REQUIRE(BinomialTable<3>::table[3] == 1);

        REQUIRE(BinomialTable<4>::table[0] == 1);
        REQUIRE(BinomialTable<4>::table[1] == 4);
        REQUIRE(BinomialTable<4>::table[2] == 6);
        REQUIRE(BinomialTable<4>::table[3] == 4);
        REQUIRE(BinomialTable<4>::table[4] == 1);
    }
}
