#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/dda.h"

using namespace distance;

using p_ = std::pair<int, int>;
namespace std {
template <typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& p)
{
    return os << '(' << p.first << ", " << p.second << ')';
}
}  // namespace std

SCENARIO("dda tests", "[dda]")
{
    GIVEN("An empty line")
    {
        dda empty;
        REQUIRE(empty.hasNext() == false);
    }
    GIVEN("An singular line")
    {
        dda s(1.1, 1.1, 1.1, 1.1);
        REQUIRE(s.hasNext() == true);
        REQUIRE(s.next() == p_{1, 1});
        REQUIRE(s.hasNext() == false);
    }
    GIVEN("A horizontal integer line")
    {
        dda horizontal(0, 0, 5, 0);

        std::vector<p_> ddasteps;
        while (horizontal.hasNext()) ddasteps.push_back(horizontal.next());
        REQUIRE(ddasteps == std::vector<p_>{p_{0, 0}, p_{1, 0}, p_{2, 0},
                                            p_{3, 0}, p_{4, 0}, p_{5, 0}});
    }
    GIVEN("A vertical integer line")
    {
        dda horizontal(2, -1, 2, 3);

        std::vector<p_> ddasteps;
        while (horizontal.hasNext()) ddasteps.push_back(horizontal.next());
        REQUIRE(ddasteps == std::vector<p_>{p_{2, -1}, p_{2, 0}, p_{2, 1},
                                            p_{2, 2}, p_{2, 3}});
    }
    GIVEN("A slightly horizontal  line")
    {
        dda horizontal(0.1, -.1, 5.1, .2);

        std::vector<p_> ddasteps;
        while (horizontal.hasNext()) ddasteps.push_back(horizontal.next());
        REQUIRE(ddasteps == std::vector<p_>{p_{0, -1}, p_{1, -1}, p_{1, 0},
                                            p_{2, 0}, p_{3, 0}, p_{4, 0},
                                            p_{5, 0}});
    }
    GIVEN("A completely diagonal line")
    {
        dda horizontal(0, 0, 2, 2);

        std::vector<p_> ddasteps;
        while (horizontal.hasNext()) ddasteps.push_back(horizontal.next());
        THEN("dda choses x before y step")
        {
            REQUIRE(ddasteps == std::vector<p_>{p_{0, 0}, p_{1, 0}, p_{1, 1},
                                                p_{2, 1}, p_{2, 2}});
        }
    }
}

std::pair<int, int> mp(double d1, double d2)
{
    return std::pair<int, int>(std::floor(d1), std::floor(d2));
}

SCENARIO("dda random lines", "[dda]")
{
    for (int i = 0; i < 2000; i++) {
        auto line = random_array<4, double>(-1000, 1000);
        if (line[2] < line[0]) {
            using std::swap;
            swap(line[0], line[2]);
            swap(line[1], line[3]);
        }
        // std::cerr << line[0] << " " << line[1] << " " << line[2] << " "
        //           << line[3] << std::endl;
        dda random(line[0], line[1], line[2], line[3]);
        REQUIRE(random.hasNext());
        const auto firstPos = mp(line[0], line[1]);
        const auto lastPos = mp(line[2], line[3]);

        auto pos = random.next();  // get the first
        REQUIRE(pos == firstPos);

        while (random.hasNext()) {
            pos = random.next();
            if (random.hasNext()) {
                // not the last pos
                REQUIRE(pos != firstPos);
                REQUIRE(pos != lastPos);
            }
        }
        REQUIRE(pos == lastPos);
        REQUIRE_FALSE(random.hasNext());
    }
}
