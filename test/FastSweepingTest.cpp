#include <cassert>
#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/FastSweeping.h"
#include "distance/FastSweeping2.h"
#include "distance/FastSweeping3.h"
#include "distance/Lattice.h"
#include "distance/PolyLine.h"
#include "distance/PolyLineRasterizer.h"
#include "distance/Trace.h"

using namespace distance;
using Latticed = Lattice<Vec2d>;
using PolyLined = PolyLine<Vec2d>;
using Rasterizerd = distance::BasicPolyLineRasterizer<Vec2d>;

namespace distance {

std::ostream& operator<<(std::ostream& os, const Array2<double>& a)
{
    os << "Array2d( ";
    for (std::size_t row = 0; row < a.dim1(); row++) {
        os << "( ";
        for (std::size_t col = 0; col < a.dim2(); col++) {
            os << a(row, col) << ", ";
        }
        os << " )" << std::endl;
    }
    return os << ")";
}

}  // namespace distance

SCENARIO("Distance maps are computed with fast sweeping", "[fsm]")
{
    GIVEN("a single initializers")
    {
        std::deque<std::tuple<Index2, double, double>> initializers = {
            {std::make_tuple(Index2{0, 0}, 0.0, -1.0)}};

        WHEN("running distance")
        {
            auto res = FastSweeping::distance(initializers.begin(),
                                              initializers.end(), 10, 10, 1, 1);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are know")
            {
                auto distances = std::get<0>(res);
                REQUIRE(distances(9, 0) == 9);
            }
        }
    }
}
SCENARIO("Distance maps are computed with fast sweeping v2", "[fsm2]")
{
    GIVEN("a single initializer")
    {
        std::deque<std::tuple<Index2, double, double>> initializers = {
            {std::make_tuple(Index2{0, 0}, 0.0, -1.0)}};

        WHEN("running distance")
        {
            auto res = FastSweeping2::distance(
                initializers.begin(), initializers.end(), 10, 10, 1, 1);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are know")
            {
                auto distances = std::get<0>(res);
                REQUIRE(distances(9, 0) == 9);
            }
        }
    }
}

SCENARIO("Uniform distance maps are computed with fast sweeping v3", "[fsm3]")
{
    Latticed l({0, 0}, {1., 1.}, 6, 7);
    GIVEN("a single initializers")
    {
        std::deque<std::tuple<Index2, double, double>> initializers = {
            {std::make_tuple(Index2{0, 0}, 0.0, -1.0)}};

        WHEN("running distance")
        {
            auto res = FastSweeping3<double>::distance(
                initializers.begin(), initializers.end(), l.size1_, l.size2_,
                l.step_.x, l.step_.y);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are know")
            {
                auto distances = std::get<0>(res);
                REQUIRE(distances(l.size1_ - 1, 0) == l.size1_ - 1);
            }
        }
    }
    GIVEN("a line initializer")
    {
        std::deque<std::tuple<Index2, double, double>> initializers;
        for (std::size_t i = 2; i <= 4; i++) {
            for (std::size_t j = 0; j < 10; j++) {
                initializers.push_back(std::make_tuple(
                    Index2(i, j), i - 3.0, j /*j / (l.size2_ - 1.0)*/));
            }
        }

        WHEN("running distance")
        {
            auto res = FastSweeping3<double>::distance(
                initializers.begin(), initializers.end(), l.size1_, l.size2_,
                l.step_.x, l.step_.y);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are known")
            {
                auto distances = std::get<0>(res);
                auto params = std::get<1>(res);
                for (int i = 0, iend = l.size1_; i < iend; i++) {
                    for (std::size_t j = 0; j < l.size2_; j++) {
                        REQUIRE(distances(i, j) == Approx((i - 3)));
                        REQUIRE(
                            params(i, j) + 1 ==
                            Approx(j + 1));  // Approx(0) is a tricky bastard
                    }
                }
            }
        }
    }
}

SCENARIO("Non-uniform distance maps are computed with fast sweeping v3",
         "[fsm3][nonuniform]")
{
    GIVEN("a 3x1 step")
    {
        Latticed l({0, 0}, {3., 1.}, 6, 7);
        std::deque<std::tuple<Index2, double, double>> initializers;
        for (int i = 2; i <= 4; i++) {
            for (int j = 0; j < 10; j++) {
                initializers.push_back(
                    std::make_tuple(Index2(i, j), (i - 3.0) * l.step_.x,
                                    j /*j / (l.size2_ - 1.0)*/));
            }
        }

        WHEN("running distance")
        {
            auto res = FastSweeping3<double>::distance(
                initializers.begin(), initializers.end(), l.size1_, l.size2_,
                l.step_.x, l.step_.y);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are known")
            {
                auto distances = std::get<0>(res);
                auto params = std::get<1>(res);
                INFO(distances);
                for (int i = 0, iend = l.size1_; i < iend; i++) {
                    for (std::size_t j = 0; j < l.size2_; j++) {
                        REQUIRE(distances(i, j) == Approx((i - 3) * l.step_.x));
                        REQUIRE(
                            params(i, j) + 1 ==
                            Approx(j + 1));  // Approx(0) is a tricky bastard
                    }
                }
            }
        }
    }
    GIVEN("a 1x3 step")
    {
        Latticed l({0, 0}, {1., 3}, 6, 7);
        PolyLined poly({{3.0, 0}, {3.0, 18.0}});
        auto initializers = Rasterizerd::rasterize(poly, l, 2);

        WHEN("running distance")
        {
            auto res = FastSweeping3<double>::distance(
                initializers.begin(), initializers.end(), l.size1_, l.size2_,
                l.step_.x, l.step_.y);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are known")
            {
                auto distances = std::get<0>(res);
                auto params = std::get<1>(res);
                for (int i = 0, iend = l.size1_; i < iend; i++) {
                    for (std::size_t j = 0; j < l.size2_; j++) {
                        INFO(distances << "\n"
                                       << params << ", " << i << ", " << j);
                        REQUIRE(distances(i, j) ==
                                Approx(-(i - 3) * l.step_.x));
                        REQUIRE(params(i, j) + 1 ==
                                Approx(j * 1. / (l.size2_ - 1) +
                                       1.));  // Approx(0) is a tricky bastard
                    }
                }
            }
        }
    }
    GIVEN("a 2x3 step and a slanted source line")
    {
        Latticed l({0, 0}, {2., 3.}, 6, 7);
        PolyLined poly({{3.0, 0}, {0.0, 18.0}});
        auto initializers = Rasterizerd::rasterize(poly, l, 2.1);
        WHEN("running distance")
        {
            auto res = FastSweeping3<double>::distance(
                initializers.begin(), initializers.end(), l.size1_, l.size2_,
                l.step_.x, l.step_.y);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(std::isfinite(distances(i)));
            }
            THEN("some values are known")
            {
                auto distances = std::get<0>(res);
                REQUIRE(distances(0, 6) == 0);
                auto len = [&poly](const Vec2d& p) {
                    auto proj = poly.closest(p).projection;
                    return (p - proj).length();
                };
                REQUIRE(distances(5, 6) == Approx(-len(l.l2w(5, 6))));
                REQUIRE(distances(0, 0) == Approx(len(l.l2w(0, 0))));
            }
        }
    }
}
