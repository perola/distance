#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/aabb.h"

using namespace distance;

SCENARIO("axis aligned bbox tests", "[aabb]")
{
    GIVEN("A default bbox")
    {
        aabb box;
        THEN("the box should be invalid") { REQUIRE_FALSE(box.isValid()); }
        THEN("the box should not be empty") { REQUIRE_FALSE(box.isEmpty()); }
    }

    GIVEN("It's not possible to create an invalid bbox from point input")
    {
        aabb box(Vec2d(1, 1), Vec2d(-1, -1));
        REQUIRE(box.isValid() == true);
        REQUIRE(box == aabb(Vec2d(-1, -1), Vec2d(1, 1)));
    }

    GIVEN("An empty bbox")
    {
        aabb box({0., 0.});
        THEN("the box should be empty and valid")
        {
            REQUIRE(box.isEmpty() == true);
            REQUIRE(box.contains(Vec2d(0, 0)) == false);
            REQUIRE(box.width() == 0);
            REQUIRE(box.height() == 0);

            REQUIRE(box.isValid() == true);
        }
        THEN("the box can contain some points with a sufficient eps")
        {
            REQUIRE(box.contains(Vec2d(0, 0), 1e-9) == true);
        }

        GIVEN("The box grows")
        {
            box.grow(Vec2d(1, 1));
            THEN("The box is no longer empty")
            {
                REQUIRE(box.isEmpty() == false);
                REQUIRE(box.contains(Vec2d(0, 0)) == false);
                REQUIRE(box.width() == 1);
                REQUIRE(box.height() == 1);

                REQUIRE(box.isValid() == true);
            }
            THEN("The box contains a point inside the boundary")
            {
                REQUIRE(box.contains({.1, .55}) == true);
            }
            THEN(
                "The box does not contain an inside point with a sufficient "
                "negative eps")
            {
                REQUIRE(box.contains(Vec2d(.5, .5), -1) == false);
            }
        }
    }
    GIVEN("a unit box")
    {
        aabb box(Vec2d(0, 0), Vec2d(1, 1));
        aabb box2(Vec2d(0, 0));
        REQUIRE(box.isValid());
        REQUIRE(box.grow(Vec2d(1, 1)) == box);
    }
}

SCENARIO("aabb manipulation tests", "[aabb]")
{
    GIVEN("a unit bbx")
    {
        aabb box(Vec2d(0, 0), Vec2d(1, 1));
        WHEN("growing with itself")
        {
            box.grow(box);
            THEN("the box doesn't change")
            {
                REQUIRE(box == aabb(Vec2d(0, 0), Vec2d(1, 1)));
            }
        }
        WHEN("growing with 0")
        {
            box.grow(0.0);
            THEN("the box doesn't change")
            {
                REQUIRE(box == aabb(Vec2d(0, 0), Vec2d(1, 1)));
            }
        }
        WHEN("growning with 0")
        {
            THEN("the box doesn't change")
            {
                REQUIRE(box.grown(0.0) == aabb(Vec2d(0, 0), Vec2d(1, 1)));
            }
        }
        WHEN("intersecting with itself")
        {
            THEN("the box doesn't change")
            {
                REQUIRE(box.intersection(box) == box);
            }
        }
    }
}

SCENARIO("aabb variadic creation tests", "[aabb]")
{
    GIVEN("variadic boxes")
    {
        REQUIRE(aabb(Vec2d(0, 0), Vec2d(1, 1)) ==
                aabb(Vec2d(0, 0)).grow(Vec2d(1, 1)));
        REQUIRE(aabb(Vec2d(0, 0), Vec2d(1, 1), Vec2d(2, 2)) ==
                aabb(Vec2d(0, 0)).grow(Vec2d(1, 1)).grow(Vec2d(2, 2)));
    }
}

SCENARIO("aabb intersection tests", "[aabb]")
{
    GIVEN("a unit bbx")
    {
        aabb box(Vec2d(0, 0), Vec2d(1, 1));

        WHEN("intersection with a unit vector centered in origo")
        {
            auto unitinter = box.intersection(Vec2d(0, 0), Vec2d(1, 1));
            THEN("a first intersection at 0 and second at 1")
            {
                REQUIRE(unitinter.first == 0);
                REQUIRE(unitinter.second == 1);
            }
        }
        WHEN("intersecting with a vector inside")
        {
            Vec2d pos(.25, .25);
            Vec2d dir(1, 1);
            auto inter = box.intersection(pos, dir);
            THEN("one intersection positive and one negative")
            {
                REQUIRE(inter.first < 0);
                REQUIRE(inter.second > 0);
                REQUIRE(inter.first == -.25);
                REQUIRE(inter.second == +.75);
            }
            THEN("a perfect intersection is on the edge and not inside")
            {
                Vec2d edge1 = pos + inter.first * dir;
                Vec2d edge2 = pos + inter.second * dir;
                REQUIRE(box.contains(edge1) == false);
                REQUIRE(box.contains(edge2) == false);

                REQUIRE(box.contains(edge1, 1e-12) == true);
                REQUIRE(box.contains(edge2, 1e-12) == true);
            }
        }
        WHEN("intersecting with dir that creates inf")
        {
            Vec2d pos(.1926, .8965);
            Vec2d dir(0, .701);
            auto inter = box.intersection(pos, dir);
            THEN("we want finite numbers")
            {
                REQUIRE(std::isfinite(inter.first));
                REQUIRE(std::isfinite(inter.second));
            }

            dir = dir.orthogonal();
            inter = box.intersection(pos, dir);
            THEN("we want finite numbers")
            {
                REQUIRE(std::isfinite(inter.first));
                REQUIRE(std::isfinite(inter.second));
            }
        }
    }
}
