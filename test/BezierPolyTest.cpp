#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Bezier.h"
#include "distance/PolyBezier.h"

using namespace distance;

SCENARIO("Constructing", "[bezierpoly]")
{
    GIVEN("An empty cubic bezier poly")
    {
        PolyBezier3<Vec2d> poly;
        THEN("the size should be empty")
        {
            REQUIRE(poly.count() == 0);
            REQUIRE_FALSE(poly.is_closed());
            REQUIRE(poly.beziers().size() == 0);
            REQUIRE(poly.params().size() == 0);
            REQUIRE_THROWS(poly.bezier(0));
            REQUIRE_THROWS(poly.param(0));
        }
#if 0
	THEN("evaluation should fail") {
	  // eval empty asserts
	  REQUIRE_THROWS(poly.eval(0.0));
        }
#endif
    }

    GIVEN("A single bezier patch")
    {
        std::array<Vec2d, 4> cubic =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));

        PolyBezier3<Vec2d> poly({CubicBezier<Vec2d>(cubic)});
        THEN("the size should be non-empty")
        {
            REQUIRE(poly.count() == 1);
            REQUIRE_FALSE(poly.is_closed());
            REQUIRE(poly.beziers().size() == 1);
            REQUIRE(poly.params().size() == 2);
            REQUIRE_NOTHROW(poly.bezier(0));
            REQUIRE_NOTHROW(poly.param(0));
            REQUIRE_THROWS(poly.bezier(1));
            REQUIRE_THROWS(poly.param(2));
        }
        THEN("evaluation should match at endpoints")
        {
            REQUIRE(poly.eval(0.0).x == cubic[0].x);
            REQUIRE(poly.eval(0.0).y == cubic[0].y);
            REQUIRE(poly.eval(1.0).x == cubic[3].x);
            REQUIRE(poly.eval(1.0).y == cubic[3].y);
        }
        THEN("the poly should have unit parametrization")
        {
            REQUIRE(poly.param(0) == 0.0);
            REQUIRE(poly.param(1) == 1.0);
        }
    }
    GIVEN("An single bezier patch with params")
    {
        std::array<Vec2d, 4> cubic =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));
        PolyBezier3<Vec2d> poly({CubicBezier<Vec2d>(cubic)}, {0.0, 1.0});
        THEN("the size should be nonempty")
        {
            REQUIRE(poly.count() == 1);
            REQUIRE(poly.beziers().size() == 1);
            REQUIRE(poly.params().size() == 2);
            REQUIRE(poly.bezier(0).c0().x == cubic[0].x);
            REQUIRE(poly.param(0) == 0.0);
        }
    }
    GIVEN("A two bezier patches poly")
    {
        std::array<Vec2d, 4> cubic1 =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));
        std::array<Vec2d, 4> cubic2 =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));

        PolyBezier3<Vec2d> poly(
            {CubicBezier<Vec2d>(cubic1), CubicBezier<Vec2d>(cubic2)});
        THEN("the size should be non-empty")
        {
            REQUIRE(poly.count() == 2);
            REQUIRE(poly.beziers().size() == 2);
            REQUIRE(poly.params().size() == 3);
            REQUIRE_NOTHROW(poly.bezier(0));
            REQUIRE_NOTHROW(poly.param(0));
        }
        THEN("evaluation should match at endpoints")
        {
            REQUIRE(poly.eval(0.0).x == cubic1[0].x);
            REQUIRE(poly.eval(0.0).y == cubic1[0].y);
            REQUIRE(poly.eval(1.0).x == cubic2[3].x);
            REQUIRE(poly.eval(1.0).y == cubic2[3].y);
        }
        THEN("the poly should have unit parametrization")
        {
            REQUIRE_THAT(poly.params(),
                         Catch::Equals(std::vector<double>{0.0, .5, 1.0}));
        }
    }
}

namespace distance {
namespace inner {
template <>
struct LengthConcept<Vec2d>
{
    static double length(const Vec2d& d) { return d.length(); }
};
}  // namespace inner
}  // namespace distance

SCENARIO("Cubic beziers polys are computed", "[bezierpoly]")
{
    GIVEN("A two bezier patches poly")
    {
        std::array<Vec2d, 4> cubic1 =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));
        std::array<Vec2d, 4> cubic2 =
            random_array<4>(Vec2d(0, 0), Vec2d(100, 50));

        PolyBezier3<Vec2d> poly(
            {CubicBezier<Vec2d>(cubic1), CubicBezier<Vec2d>(cubic2)});

        WHEN("refining")
        {
            auto poly2 = poly.refine();
            THEN("the rep changes but evaluates to same")
            {
                REQUIRE(poly2.eval(.1).x == Approx(poly.eval(.1).x));
                REQUIRE(poly2.eval(.987).y == Approx(poly.eval(.987).y));
                REQUIRE(poly2.eval(.55555).x == Approx(poly.eval(.55555).x));
            }
        }

        THEN("the size should be non-empty")
        {
            REQUIRE(poly.count() == 2);
            REQUIRE(poly.beziers().size() == 2);
            REQUIRE(poly.params().size() == 3);
            REQUIRE_NOTHROW(poly.bezier(0));
            REQUIRE_NOTHROW(poly.param(0));
        }
        THEN("evaluation should match at endpoints")
        {
            REQUIRE(poly.eval(0.0).x == cubic1[0].x);
            REQUIRE(poly.eval(0.0).y == cubic1[0].y);
            REQUIRE(poly.eval(1.0).x == cubic2[3].x);
            REQUIRE(poly.eval(1.0).y == cubic2[3].y);
        }
        THEN("the poly should have unit parametrization")
        {
            REQUIRE_THAT(poly.params(),
                         Catch::Equals(std::vector<double>{0.0, .5, 1.0}));
        }
    }
}

SCENARIO("Cubic beziers interpolants", "[bezierpoly]")
{
    GIVEN("Zero points")
    {
        WHEN("Constructing an interpolant")
        {
            using Hermite = HermiteInterpolant<Vec2d>;
            // auto cubic = Cubic(); // deleted empty constructor

            auto cubicEmpty = Hermite::interpolate(std::vector<Vec2d>{});
            auto cubicSingular =
                Hermite::interpolate(std::vector<Vec2d>{{0, 0}});
            auto cubicLine =
                Hermite::interpolate(std::vector<Vec2d>{{0, 0}, {1, 1}});
        }
    }
}
