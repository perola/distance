#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/FmmGrid.h"

using namespace distance;

SCENARIO("Distance maps are computed with fast marching grid", "[fmmgrid]")
{
    GIVEN("a single initializers")
    {
        std::deque<std::tuple<Index2, double, double>> initializers = {
            {std::make_tuple(Index2{0, 0}, 0.0, 0.0)}};

        WHEN("running distance")
        {
            auto res = FmmGrid<double>::distance(
                initializers.begin(), initializers.end(), 10, 10, 1, 1);
            THEN("no nans")
            {
                auto distances = std::get<0>(res);
                for (decltype(distances.count()) i = 0; i < distances.count();
                     i++)
                    REQUIRE(!std::isnan(distances(i)));
            }
            THEN("some values are know")
            {
                auto distances = std::get<0>(res);
                REQUIRE(distances(9, 0) == 9);
            }
        }
    }
}
