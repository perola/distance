#include <catch2/catch.hpp>  // first
// second

#include <iostream>
#include <map>

#include "common.h"
#include "distance/Lattice.h"
#include "distance/PolyLine.h"
#include "distance/PolyLineRasterizer.h"

using namespace distance;
using namespace distance::ls;
using vec2d = distance::vec2<double>;

using PolyLined = PolyLine<vec2d>;
using LineSegmentd = LineSegment<vec2d>;
using Latticed = Lattice<vec2d>;

namespace distance {
#if !DISTANCE_HAS_IO
template <template <class> class T, class C>
std::ostream& operator<<(std::ostream& os, const T<C>&)
{
    return os;
}
#endif
std::ostream& operator<<(std::ostream& os, const GridProjection& gp)
{
    os << "{(" << gp.i() << ", " << gp.j() << ") " << gp.distance() << ", "
       << gp.parameter() << "}";
    return os;
}
}  // namespace distance
namespace std {
std::ostream& operator<<(std::ostream& os, const std::deque<GridProjection>& in)
{
    os << "{ ";
    for (auto p : in) {
        os << p << ", ";
    }
    os << "}";
    return os;
}
}  // namespace std

SCENARIO("Rasterizing unit tests", "[polyline][rasterize]")
{
    auto COMP = [](const GridProjection& p1, const GridProjection& p2) {
        REQUIRE(p1.coord() == p2.coord());
        REQUIRE(p1.distance() == Approx(p2.distance()));
        REQUIRE(p1.parameter() == Approx(p2.parameter()));
    };

    GIVEN("A straight line")
    {
        using vec2d = distance::vec2<double>;
        vec2d p0 = {.6, .12}, p1{2.6, 2.7};

        PolyLined line({p0, p1});
        using BasicRaster = BasicPolyLineRasterizer<vec2d, true>;
        Latticed lattice{{0, 0}, {1, 1}, 6, 3};
        double width = 1.4;
        auto inits1_parallel = BasicRaster::rasterize(line, lattice, width);
        vec2d proj_dir(1, 0);  // projection direction
        auto inits1_similar =
            BasicRaster::rasterize(line, lattice, proj_dir, width);

        WHEN("subdividing")
        {
            PolyLined line2({p0, lerp(p0, p1, 0.5), p1}, {0., .5, 1.0});
            //           INFO(line << "\n" << line2);
            THEN("rasterizing parallel should be invariant")
            {
                auto inits2_parallel =
                    BasicRaster::rasterize(line2, lattice, width);
                REQUIRE_EQUAL(inits1_parallel, inits2_parallel, COMP);
            }
            AND_THEN("rasterizing similar should be invariant")
            {
                auto inits2_similar =
                    BasicRaster::rasterize(line2, lattice, proj_dir, width);
                REQUIRE_EQUAL(inits1_similar, inits2_similar, COMP);
            }
        }
    }
    GIVEN("a V line")
    {
        vec2d p0 = {.6, .12}, p1{2.6, 2.7}, p2{6.2, 1.2};
        PolyLined line({p0, p1, p2});
        Latticed lattice{{0, 0}, {1, 4}, 6, 6};
        double width = 2.4;

        WHEN("fast rasterizing")
        {
            THEN("rasterizing parallel should be invariant")
            {
                using BasicRaster = BasicPolyLineRasterizer<vec2d>;
                using Raster = PolyLineRasterizer<vec2d>;
                auto inits1 = BasicRaster::rasterize(line, lattice, width);

                auto prune = [](const std::deque<GridProjection>& gp) {
                    // std::cerr << "prune: " << gp.size() << std::endl;
                    std::map<Index2, GridProjection> newgp;
                    for (auto g : gp) {
                        Index2 index = g.coord();
                        auto it = newgp.find(index);
                        if (it == newgp.end()) {
                            newgp.insert({index, g});
                            // std::cerr << index << " -> " << g << std::endl;
                        } else {
                            if (std::abs(g.distance()) <
                                std::abs(it->second.distance())) {
                                // std::cerr << index << ": "
                                //           << it->second.distance << " -> " <<
                                //           g
                                //           << std::endl;
                                newgp[index] = g;
                            }
                        }
                    }
                    std::deque<GridProjection> ret;
                    std::transform(
                        newgp.begin(), newgp.end(), std::back_inserter(ret),
                        [](const std::map<Index2, GridProjection>::value_type&
                               pair) { return pair.second; });
                    return ret;
                };
                auto inits2 = Raster::rasterize3(line, lattice, width);
                inits2 = prune(inits2);

                REQUIRE_EQUAL(inits1, inits2, COMP, "1 vs 3 faster\n");
            }
        }
    }
}

/*
 The scenarios below are hidden by default
 They can be used to print out text arrays loadable in python
 (np.load('output.txt'))
*/
SCENARIO("Rasterizing output", "[rasterize-single][.]")
{
    GIVEN("A straight line")
    {
        using vec2d = distance::vec2<double>;
        vec2d p0 = {6, 12}, p1{76, 87};

        PolyLined line({p0, p1});
        Latticed lattice{{0, 0}, {1, 1}, 100, 100};
        using raster = BasicPolyLineRasterizer<vec2d, true>;
        auto inits = raster::rasterize(line, lattice, 25.0);
        Array2<double> dist(lattice.size1_, lattice.size2_);
        std::fill(dist.begin(), dist.end(), 100);
        for (const auto& it : inits) {
            dist(it.coord()) = it.distance();
        }
        for (uint i = 0; i < dist.dim1(); i++) {
            for (uint j = 0; j < dist.dim2(); j++) {
                std::cerr << dist(i, j) << " ";
            }
            std::cerr << "\n";
        }
    }
}

SCENARIO("Rasterizing d", "[.][rasterize-double]")
{
    GIVEN("A v-poly")
    {
        using vec2d = distance::vec2<double>;
        vec2d p0 = {6, 12}, p1{46, 87}, p2{91, 21};

        PolyLined line({p0, p1, p2});
        //        std::cout << line << std::endl;
        Latticed lattice{{0, 0}, {1, 1}, 100, 100};
        using raster = BasicPolyLineRasterizer<vec2d, true>;
        auto inits = raster::rasterize(line, lattice, 25.0);
        Array2<double> dist(lattice.size1_, lattice.size2_);
        std::fill(dist.begin(), dist.end(), 100);
        for (const auto& it : inits) {
            dist(it.coord()) = it.distance();
        }
        for (uint i = 0; i < dist.dim1(); i++) {
            for (uint j = 0; j < dist.dim2(); j++) {
                std::cerr << dist(i, j) << " ";
            }
            std::cerr << "\n";
        }
    }
}

SCENARIO("Rasterizing polylines", "[.][rasterize-complex]")
{
    GIVEN("A complex line")
    {
        using vec2d = distance::vec2<double>;
        vec2d p0 = {0, 0}, p1{22, 74}, p2{31, 34}, p3{75.3, 2}, p4{72, 54},
              p5{110, 87};

        PolyLined line({p0, p1, p2, p3, p4, p5});
        // std::cout << line << std::endl;
        Latticed lattice{{0, 0}, {1, 1}, 100, 100};
        using raster = BasicPolyLineRasterizer<vec2d, true>;
        auto inits = raster::rasterize(line, lattice, 25.0);
        Array2<double> dist(lattice.size1_, lattice.size2_);
        std::fill(dist.begin(), dist.end(), 100);
        for (const auto& it : inits) {
            dist(it.coord()) = it.distance();
        }
        for (uint i = 0; i < dist.dim1(); i++) {
            for (uint j = 0; j < dist.dim2(); j++) {
                std::cerr << dist(i, j) << " ";
            }
            std::cerr << "\n";
        }
    }
}

SCENARIO("Rasterizing polylines nonuniform", "[.][rasterize-nonuniform]")
{
    GIVEN("A complex line")
    {
        using vec2d = distance::vec2<double>;
        vec2d p0 = {0, 0}, p1{22, 74}, p2{31, 34}, p3{75.3, 2}, p4{72, 54},
              p5{110, 87};

        PolyLined line({p0, p1, p2, p3, p4, p5});
        // std::cout << line << std::endl;
        Latticed lattice{{0, 0}, {3, 1}, 100, 100};
        using raster = BasicPolyLineRasterizer<vec2d, true>;
        auto inits = raster::rasterize(line, lattice, 25.0);
        Array2<double> dist(lattice.size1_, lattice.size2_);
        std::fill(dist.begin(), dist.end(), 100);
        for (const auto& it : inits) {
            dist(it.coord()) = it.distance();
        }
        for (uint i = 0; i < dist.dim1(); i++) {
            for (uint j = 0; j < dist.dim2(); j++) {
                std::cerr << dist(i, j) << " ";
            }
            std::cerr << "\n";
        }
    }
}
