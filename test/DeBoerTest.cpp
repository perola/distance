#include <catch2/catch.hpp>
#include <iostream>
#include <set>

#include "common.h"
#include "distance/Bezier.h"
#include "distance/DeBoer.h"

using namespace distance;
namespace {
double deboer_cubic(const std::vector<double>& coordinates,
                    const std::vector<double>& knots, double t)
{
    // find the first position that is not less than new_knot
    auto it = std::lower_bound(knots.begin(), knots.end(), t);
    assert(it != knots.end());
    assert(*it >= t);
    // go back to the start position for this segment
    auto offset = std::max<int>(0, std::distance(knots.begin(), it) - 3);
    // std::cerr << "t: " << t << ", offset: " << offset << std::endl;

    return distance::DeBoerEvaluator<double, 3, 3>::eval(
        coordinates.data() + offset, knots.data() + offset, t);
}
}  // namespace

SCENARIO("Splines are evaluated", "[spline]")
{
    GIVEN("A regular simple knot vector and coordinates")
    {
        std::vector<double> knots = {0., 0., 0., 1., 1., 1.};
        std::vector<double> coordinates = {0., .75, .75, 0.};

        WHEN("spline is evaluated")
        {
            THEN("Control end points coincide with the f(0) and f(1)")
            {
                REQUIRE(deboer_cubic(coordinates, knots, 0) ==
                        coordinates.front());
                REQUIRE(deboer_cubic(coordinates, knots, 1) ==
                        coordinates.back());
            }
        }
        WHEN("converting to bezier form")
        {
            auto beziers = to_bezier<double, 3>(coordinates, knots);
            THEN("a single bezier is the result")
            {
                REQUIRE(beziers.size() == 1);
            }
            THEN(
                "First and last control points should coincide with f(0) and "
                "f(1)")
            {
                REQUIRE(beziers.front().at(0) == coordinates.front());
                REQUIRE(beziers.back().at(3) == coordinates.back());
            }
        }
    }
    GIVEN("A regular knot vector and coordinates")
    {
        std::vector<double> knots = {0., 0., 0., .5, 1., 1., 1.};
        std::vector<double> coordinates = {0., .75, .6, .75, -1.0};

        WHEN("spline is evaluated")
        {
            THEN(
                "Control end points coincide with the f(0) and f(1) and some "
                "known")
            {
                /*
                  BSplineFunction[ {0, .75, .6, .75, -1},
                    SplineKnots -> {0, 0, 0, 0, .5, 1, 1, 1, 1}][.55] = 0.67535
                  BSplineFunction[ {0, .75, .6, .75, -1},
                    SplineKnots -> {0, 0, 0, 0, .5, 1, 1, 1, 1}][.45] = 0.67635
                 */
                REQUIRE(deboer_cubic(coordinates, knots, 0) ==
                        coordinates.front());
                REQUIRE(deboer_cubic(coordinates, knots, .45) ==
                        Approx(.67635));
                REQUIRE(deboer_cubic(coordinates, knots, .5) == .675);
                REQUIRE(deboer_cubic(coordinates, knots, .55) ==
                        Approx(.67535));
                REQUIRE(deboer_cubic(coordinates, knots, 1) ==
                        coordinates.back());
            }
        }
        WHEN("converting to bezier form")
        {
            auto beziers = to_bezier<double, 3>(coordinates, knots);
            THEN("we get N-1 beziers where N is the number of unique knots")
            {
                REQUIRE(beziers.size() ==
                        std::set<double>(knots.begin(), knots.end()).size() -
                            1);
            }
            THEN(
                "First and last control points should coincide with f(0) and "
                "f(1)")
            {
                REQUIRE(beziers.front().at(0) == coordinates.front());
                REQUIRE(beziers.front().at(3) == .675);
                REQUIRE(beziers.back().at(3) == coordinates.back());
            }
            THEN(
                "The first and second derivatives are continuous between "
                "beziers")
            {
                auto bezier = [](const std::array<double, 4>& c, double t) {
                    return BezierEvaluator<double, 0, 3>::eval(c.data(), t);
                };
                auto bezier1 = [](const std::array<double, 4>& c, double t) {
                    return BezierEvaluator<double, 1, 3>::eval(c.data(), t);
                };
                auto bezier2 = [](const std::array<double, 4>& c, double t) {
                    return BezierEvaluator<double, 2, 3>::eval(c.data(), t);
                };
                REQUIRE(bezier(beziers.at(0), 1.0) ==
                        bezier(beziers.at(1), 0.0));
                REQUIRE(bezier1(beziers.at(0), 1.0) ==
                        bezier1(beziers.at(1), 0.0));
                REQUIRE(bezier2(beziers.at(0), 1.0) ==
                        bezier2(beziers.at(1), 0.0));
            }
        }
    }
}
