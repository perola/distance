#include <catch2/catch.hpp>
#include <initializer_list>
#include <iostream>
#include <set>

#include "common.h"
#include "distance/Array1.h"

using namespace distance;

template <typename T>
void REQUIRE_EQUAL(const std::vector<T>& v1, const std::vector<T>& v2)
{
    REQUIRE(v1.size() == v2.size());
    for (std::size_t i = 0; i < v1.size(); i++) {
        REQUIRE(v1.at(i) == Approx(v2.at(i)));
    }
}

SCENARIO("Array1 ", "[array1]")
{
    Array1<double> arr{0, 1, 2};
    REQUIRE(interp1(arr, 0.0) == 0.0);
    REQUIRE(interp1(arr, 0.5) == 1.);
    REQUIRE(interp1(arr, 1.0) == 2.);
    REQUIRE(interp1(arr, .25) == .5);
    REQUIRE(interp1(arr, .75) == 1.5);

    std::vector<float> params{0., .2, .7, 1.0};
    auto res = interp1(arr, params);
    REQUIRE_EQUAL(res, std::vector<double>{0., .4, 1.4, 2.0});
}
