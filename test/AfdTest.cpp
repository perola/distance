#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Vec2d.h"
#include "distance/afd.h"

using namespace distance;

SCENARIO("afds are constructed", "[afd]")
{
    GIVEN("an afd from an empty bezier")
    {
        std::array<double, 4> bezier{};
        afd<double> a(bezier);
        REQUIRE(a.to_bezier() == bezier);
        REQUIRE(a.hasNext());
        REQUIRE(a.curr() == 0);
        REQUIRE(a.next() == 0);
        REQUIRE(a.hasNext() == false);
    }
    GIVEN("an afd from a straight bezier")
    {
        std::array<double, 4> bezier{{0, 1, 2, 3}};
        afd<double> a(bezier);
        REQUIRE(a.to_bezier() == bezier);
        REQUIRE(a.curr() == 0);
        REQUIRE(a.hasNext());
        REQUIRE_NOTHROW(a.L());  // refine
        REQUIRE(a.curr() == 0.0);
        REQUIRE(a.next() == 1.5);
        REQUIRE(a.next() == 3.0);
        REQUIRE(a.hasNext() == false);
    }
    GIVEN("an afd from a curved bezier")
    {
        std::array<double, 4> bezier{{0, 2, 1, 3}};
        afd<double> a(bezier);
        REQUIRE(a.to_bezier() == bezier);
        REQUIRE(a.curr() == 0);
        REQUIRE(a.hasNext());
        REQUIRE_NOTHROW(a.L());  // refine
        REQUIRE(a.curr() == 0.0);
        REQUIRE(a.next() == 1.5);
        REQUIRE(a.next() == 3.0);
        REQUIRE(a.hasNext() == false);
    }
}
