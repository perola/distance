#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/Bresenham.h"

using namespace distance;

namespace std {
template <typename T1, typename T2>
std::pair<T1, T2> abs(const std::pair<T1, T2>& p)
{
    using std::abs;
    return {abs(p.first), abs(p.second)};
}
}  // namespace std

SCENARIO("Breseham line rasterizing is tested", "[bresenham]")
{
    GIVEN("default an empty line has no steps")
    {
        BresenhamVisitor b;
        REQUIRE(b.hasNext() == false);
    }
    GIVEN("a singular line from a point has one step")
    {
        BresenhamVisitor b(0, 0, 0, 0);
        REQUIRE(b.hasNext() == true);
        REQUIRE(b.next() == std::pair<int, int>(0, 0));
    }
}

inline std::pair<int, int> mp(double d1, double d2)
{
    return std::pair<int, int>(std::floor(d1), std::floor(d2));
}
template <typename T>
inline std::pair<T, T> operator-(const std::pair<T, T>& p1,
                                 const std::pair<T, T>& p2)
{
    return {p1.first - p2.first, p1.second - p2.second};
}
template <typename T>
inline T manhattan(const std::pair<T, T>& p)
{
    return std::abs(p.first) + std::abs(p.second);
}

SCENARIO("Bresenham random lines", "[bresenham]")
{
    for (int i = 0; i < 2000; i++) {
        auto line = random_array<4, double>(-1000, 1000);

        BresenhamVisitor random(line[0], line[1], line[2], line[3]);
        REQUIRE(random.hasNext());
        const auto firstPos = mp(line[0], line[1]);
        const auto lastPos = mp(line[2], line[3]);

        auto pos = random.next();  // get the first
        REQUIRE(pos == firstPos);

        while (random.hasNext()) {
            auto next = random.next();
            REQUIRE(next != pos);
            auto diff = next - pos;
            REQUIRE((manhattan(diff) == 1 || std::abs(diff) == mp(1, 1)));
            pos = next;
        }
        REQUIRE(pos == lastPos);
        REQUIRE_FALSE(random.hasNext());
    }
}
