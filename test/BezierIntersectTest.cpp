#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/BezierIntersect.h"

using namespace distance;

SCENARIO("Cubic bezier intersection is tested", "[bezier][intersection]")
{
    GIVEN("Two intersecting cubic beziers")
    {
        std::array<Vec2d, 4> cubic1 = {Vec2d{0.0, 0}, Vec2d{1, 1}, Vec2d{3, 1},
                                       Vec2d{4, 0}};
        std::array<Vec2d, 4> cubic2 = {Vec2d{0.0, 1}, Vec2d{1, 0}, Vec2d{3, 0},
                                       Vec2d{4, -1}};

        THEN("The should intersect")
        {
            {
                REQUIRE(intersects(CubicBezier<Vec2d>(cubic1),
                                   CubicBezier<Vec2d>(cubic2), 1e-6));
            }
        }
    }
    GIVEN("Two 'touching' beziers")
    {
        Vec2d common{4, 0};
        std::array<Vec2d, 4> cubic1 = {Vec2d{0.0, 0}, Vec2d{1, 1}, Vec2d{3, 1},
                                       common};
        std::array<Vec2d, 4> cubic2 = {common, Vec2d{5, 0}, Vec2d{5, 0},
                                       Vec2d{6, -1}};

        THEN("The should not intersect")
        {
            {
                REQUIRE_FALSE(intersects(CubicBezier<Vec2d>(cubic1),
                                         CubicBezier<Vec2d>(cubic2), 1e-6));
            }
        }
    }
}

namespace Catch {
template <typename T>
std::vector<T> between(T start, T endInclusive, T stepSize)
{
    std::vector<T> range;
    int i = 0;
    do {
        range.push_back(start + i++ * stepSize);
    } while (range.back() < endInclusive);
    return range;
}
}  // namespace Catch
