###############################################################################
# Find Catch
#
# This sets the following variables:
# CATCH2_FOUND - True if Catch2 was found.
# CATCH2_INCLUDE_DIRS - Directories containing the Catch2 include files.
# CATCH2_DEFINITIONS - Compiler flags for Catch2.

find_path(CATCH2_INCLUDE_DIR
  NAMES catch2/catch.hpp
  HINTS "${CATCH_ROOT}" "${CATCH_ROOT}/single_include" 
  "$ENV{CATCH_ROOT}"  "$ENV{CATCH_ROOT}/single_include" 
  "/usr/include/")

mark_as_advanced(CATCH2_INCLUDE_DIR)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Catch2 DEFAULT_MSG CATCH2_INCLUDE_DIR)

if(CATCH2_FOUND)
  set(CATCH2_INCLUDE_DIRS ${CATCH2_INCLUDE_DIR})  
  message(STATUS "Catch2 found (include: ${CATCH2_INCLUDE_DIRS})")
endif(CATCH2_FOUND)

if(CATCH2_FOUND AND NOT TARGET Catch2::Catch2)
    add_library(Catch2::Catch2 INTERFACE IMPORTED)
    set_target_properties(Catch2::Catch2 PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${CATCH2_INCLUDE_DIR}"
    )
endif()
#message(STATUS "CATCH2_INCLUDE_DIR=${CATCH2_INCLUDE_DIR}")
