#include <catch2/catch.hpp>  // first
// second

#include <iostream>
#include <map>

#include "common.h"
#include "distance/PolyLine.h"
#include "distance/clamp.h"

using namespace distance;
using namespace distance::ls;
using vec2d = distance::vec2<double>;

using PolyLined = PolyLine<vec2d>;
using LineSegmentd = LineSegment<vec2d>;
// using Latticed = Lattice<vec2d>;

namespace test1 {
struct Point1
{
    using value_type = float;
    float xy[2];
    auto x() const { return xy[0]; }
    auto y() const { return xy[1]; }
};
// 1. put the member extractor in the same ns as the point class for ADL-lookup
auto first(const Point1& p) { return p.x(); }
auto second(const Point1& p) { return p.y(); }
}  // namespace test1

namespace test2 {
struct Point2
{
    using value_type = double;
    double x, y;
    double operator[](int i) const { return i == 0 ? x : y; }
};
}  // namespace test2
// global ns
auto first(const test2::Point2& p) { return p[0]; }
auto second(const test2::Point2& p) { return p[1]; }

SCENARIO("Point_t member getters ", "[polyline][point_t]")
{
    // 1. put the member extractor free function in the same ns as the point
    // class for ADL-lookup
    // 2. or put the member extractor free function in the global ns

    auto p1 = PolyLine<test1::Point1>({{0, 1}, {4, 7}});
    auto p2 = PolyLine<test2::Point2>();

    // without extractor compilation error
#if 0
    struct P
    {
        using value_type = int;
    };
    auto p3 = PolyLine<P>();
#endif
}
SCENARIO("Constructing ", "[polyline]")
{
    GIVEN("An empty polyline")
    {
        PolyLine<Vec2d> poly;
        THEN("the size should be empty")
        {
            // REQUIRE(poly.count() == 0);
            REQUIRE(poly.points().size() == 0);
        }
        THEN("evaluation should fail")
        {
            // eval empty asserts
            // REQUIRE_THROWS(poly.eval(0.0));
        }
    }
}

SCENARIO("Projections", "[polyline][proj]")
{
    GIVEN("A straight line")
    {
        LineSegment<Vec2d> L({0, 0}, {1, 0});
        auto proj = [](const Vec2d& p, const LineSegment<Vec2d>& L,
                       bool doclamp = true) {
            double t = L.project(p);
            if (doclamp) t = clamp(t, 0., 1.);
            auto projection = L.eval(t);
            return ProjectionPoint<Vec2d>{projection, t};
        };
        auto proj2 = [](const Vec2d& p, const Vec2d& dir,
                        const LineSegment<Vec2d>& L, bool doclamp = true) {
            double t = L.project(p, dir);
            if (doclamp) t = clamp(t, 0., 1.);
            auto projection = L.eval(t);
            return ProjectionPoint<Vec2d>{projection, t};
        };
        WHEN("projecting on the midpoint")
        {
            auto p_t = proj({.5, 1}, L);
            REQUIRE(p_t.parameter == .5);
            REQUIRE(p_t.projection.x == .5);
            REQUIRE(p_t.projection.y == 0);
            auto p_t2 = proj2({.5, 1}, {0., 1.}, L);
            auto p_t3 = proj2({.5, 1}, {0., -1.}, L);
            REQUIRE(p_t2.parameter == p_t.parameter);
            REQUIRE(p_t3.parameter == p_t.parameter);
        }
        WHEN("projecting before start point")
        {
            auto p_t = proj({-.5, 1}, L);
            REQUIRE(p_t.parameter == 0);
            REQUIRE(p_t.projection.x == 0);
            REQUIRE(p_t.projection.y == 0);
        }
        WHEN("projecting before start point without clamp")
        {
            auto p_t = proj({-.5, 1}, L, false);
            REQUIRE(p_t.parameter == -.5);
            REQUIRE(p_t.projection.x == -.5);
            REQUIRE(p_t.projection.y == 0);
        }
    }
}

SCENARIO("LineSegment", "[linesegment]")
{
    GIVEN("A straigh line segment")
    {
        LineSegmentd line({-1, 1}, {3, 1});
        auto midpoint = line.eval(.5);

        struct CaseStruct
        {
            std::string when_descr;
            std::string then_descr;
            vec2d position;
            vec2d proj_position;
            double parameter;
            Side side;
            bool should_fail;
        };
        auto cases = {
            CaseStruct{"center point above", "should project on midpoint",
                       midpoint + vec2d{0, 1}, midpoint, .5, Side::Left, false},
            CaseStruct{"point above right", "should project on endpoint",
                       line.p1_ + vec2d{1, 1}, line.p1_, 1, Side::Left, false},
            CaseStruct{"point below right", "should project on endpoint",
                       line.p1_ + vec2d{1, -1}, line.p1_, 1, Side::Right,
                       false},
            CaseStruct{"point below left", "should project on startpoint",
                       line.p0_ + vec2d{-1, -1}, line.p0_, 0, Side::Right,
                       false},
            CaseStruct{"point above left", "should project on startpoint",
                       line.p0_ + vec2d{-1, 2}, line.p0_, 0, Side::Left, false},
            CaseStruct{"center point below", "should project on midpoint",
                       midpoint + vec2d{0, -1}, midpoint, .5, Side::Right,
                       false},
            CaseStruct{"exactly on tangent right",
                       "should project on endpoint with Interface side, but "
                       "fails due to roundoff errors",
                       vec2d{3, 0}, line.p1_, 1, Side::Interface, true},
            CaseStruct{"exactly on tangent left",
                       "should project on startpoint with Interface side",
                       line.p0_ + vec2d{-1, 0}, line.p0_, 0, Side::Interface,
                       false}

        };

        for (auto c : cases) {
            WHEN(c.when_descr)
            {
                auto signed_closest = line.signed_closest(c.position);
                auto closest = line.closest(c.position);
                THEN(c.then_descr)
                {
                    // INFO("L: " << line << ", pos: " << closest.projection
                    //            << ", t: " << closest.parameter);
                    REQUIRE(signed_closest.projection == c.proj_position);
                    REQUIRE(closest.projection == c.proj_position);
                    REQUIRE(signed_closest.parameter == c.parameter);
                    REQUIRE(closest.parameter == c.parameter);
                    if (c.should_fail)
                        REQUIRE(signed_closest.side != c.side);
                    else
                        REQUIRE(signed_closest.side == c.side);
                }
            }
        }
    }
}
SCENARIO("LineSegment directions", "[linesegment]")
{
    GIVEN("A straigh line segment")
    {
        LineSegment<Vec2d> L({0, 0}, {1, 0});
        REQUIRE(L.dir() == L.p1_);
        REQUIRE(LineSegment<Vec2d>({5, -3}, {2, 2}).dir() == Vec2d(-3, 5));
        REQUIRE(orthogonal(L.dir()) == Vec2d(0, 1));
        REQUIRE(orthogonal(LineSegment<Vec2d>({0, 0}, {0, 1}).dir()) ==
                Vec2d(-1, 0));
    }
}

SCENARIO("halfspace sidedness", "[halfspace]")
{
    GIVEN("Three point classification")
    {
        REQUIRE(halfspace(Vec2d(0, 0), Vec2d(-1, 0), Vec2d(0, 1)) ==
                Side::Right);
        REQUIRE(halfspace(Vec2d(0, 0), Vec2d(-1, 0), Vec2d(-1, .1)) ==
                Side::Right);
        REQUIRE(halfspace(Vec2d(0, 0), Vec2d(-1, 0), Vec2d(-1, -.1)) ==
                Side::Left);
    }
}

SCENARIO("PolyLine closest point sign", "[polyline][closest][sign]")
{
    GIVEN("A straigh line")
    {
        vec2d p0 = {0, 0}, p1{2.1, 0};

        PolyLined line({p0, p1});
        vec2d midpoint = line.eval(.5);

        struct CaseStruct
        {
            std::string when_descr;
            std::string then_descr;
            vec2d position;
            vec2d proj_position;
            double parameter;
            Side side;
        };
        auto cases = {
            CaseStruct{"center point above", "should project on midpoint",
                       midpoint + vec2d{0, 1}, midpoint, .5, Side::Left},
            CaseStruct{"point above right", "should project on endpoint",
                       p1 + vec2d{1, 1}, p1, 1, Side::Left},
            CaseStruct{"point below right", "should project on endpoint",
                       p1 + vec2d{1, -1}, p1, 1, Side::Right},
            CaseStruct{"point below left", "should project on startpoint",
                       p0 + vec2d{-1, -1}, p0, 0, Side::Right},
            CaseStruct{"point above left", "should project on startpoint",
                       p0 + vec2d{-1, 2}, p0, 0, Side::Left},
            CaseStruct{"center point below", "should project on midpoint",
                       midpoint + vec2d{0, -1}, midpoint, .5, Side::Right},
            CaseStruct{"exactly on tangent right",
                       "should project on endpoint with Interface side",
                       vec2d{3, 0}, p1, 1, Side::Interface},
            CaseStruct{"exactly on tangent left",
                       "should project on startpoint with Interface side",
                       p0 + vec2d{-1, 0}, p0, 0, Side::Interface}

        };

        for (auto c : cases) {
            WHEN(c.when_descr)
            {
                auto signed_closest = line.signed_closest(c.position);
                auto closest = line.closest(c.position);
                THEN(c.then_descr)
                {
                    REQUIRE(signed_closest.projection == c.proj_position);
                    REQUIRE(closest.projection == c.proj_position);
                    REQUIRE(signed_closest.parameter == c.parameter);
                    REQUIRE(closest.parameter == c.parameter);
                    REQUIRE(signed_closest.side == c.side);
                }
            }
        }
    }
}

SCENARIO("PolyLine closest point signed invariants",
         "[polyline][closest][sign][invariants]")
{
    GIVEN("A straigh line")
    {
        vec2d p0 = {-16, -12}, p1{18.1, 19.7};
        PolyLined line({p0, p1});
        WHEN("subdividing")
        {
            PolyLined line2({p0, lerp(p0, p1, .25), p1}, {0., .25, 1.});
            THEN("The closest point should be invariant")
            {
                for (double i = -20.1; i <= 20; i++)
                    for (double j = -20.2; j <= 20; j++) {
                        vec2d point(i, j);
                        auto t1 = line.signed_closest(point);
                        auto t2 = line2.signed_closest(point);
                        REQUIRE(t1.projection.x == Approx(t1.projection.x));
                        REQUIRE(t1.projection.y == Approx(t2.projection.y));
                        REQUIRE(t1.parameter == Approx(t2.parameter));
                        REQUIRE(t1.side == t2.side);
                    }
            }
        }
    }
}

SCENARIO("PolyLine closed curve", "[polyline][closed]")
{
    GIVEN("A singulare curve is not closed")
    {
        PolyLined line;
        REQUIRE_FALSE(line.is_closed());
    }
    GIVEN("A point curve is closed")
    {
        PolyLined line({{0.0, 0.0}});
        REQUIRE(line.is_closed());
    }
    GIVEN("A lines curve is not closed")
    {
        PolyLined line({{0.0, 0.0}, {1.0, 1.0}});
        REQUIRE_FALSE(line.is_closed());
    }
    GIVEN("A triangle shape (four points, last==first)")
    {
        vec2d p0 = {0, 0}, p1{1, 0}, p2{1, 1};
        PolyLined line({p0, p1, p2, p0});
        REQUIRE(line.is_closed());
    }
}
