#include <catch2/catch.hpp>
#include <iostream>

#include "common.h"
#include "distance/FastMarching.h"

using namespace distance;

SCENARIO("Distance maps are computed with fast marching", "[fmm]")
{
    GIVEN("a grid")
    {
        Array2<double> d(10, 10);
        std::fill(d.begin(), d.end(), std::numeric_limits<double>::quiet_NaN());
        WHEN("initialized from first grid point")
        {
            d(0, 0) = 0.0;
            FastMarching fm(d, 1, 1);
            fm.initialize();
            fm.march();
            THEN("no more nans")
            {
                for (auto value : d) REQUIRE(!std::isnan(value));
            }
            THEN("some values are known") { REQUIRE(d(9, 0) == 9); }
        }
    }
}
