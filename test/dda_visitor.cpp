#include <array>
#include <fstream>
#include <iostream>
#include <memory>

#include "distance/Bresenham.h"
#include "distance/dda.h"

#if !DISTANCE_HAS_IOSTREAM
// duplicate som code here
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::pair<T, T>& p)
{
    return (os << '(' << p.first << ", " << p.second << ')');
}
namespace distance {
template <template <class> class T, class C>
std::ostream& operator<<(std::ostream& os, const T<C>&)
{
    return os;
}
}  // namespace distance
#endif

template <class stream_type, bool do_delete>
void stream_deleter(stream_type* stream)
{
    if (do_delete) {
        delete stream;
        std::cerr << "Closing file " << ((void*)stream) << std::endl;
    }
}

using namespace distance;

using SPtr = std::unique_ptr<std::istream, void (*)(std::istream*)>;

bool read4(SPtr stream_ptr, std::array<double, 4>& output)
{
    assert(stream_ptr);
    assert(*stream_ptr);
    stream_ptr->read((char*)output.data(), output.size() * sizeof(double));
    // assert(*stream_ptr);
    return !!*stream_ptr;
}

int main(int argc, char* argv[])
{
    std::cerr << "argc: " << argc << std::endl;
    std::array<double, 4> data;

    switch (argc) {
        case 1:
            std::cerr << "Reading cin: " << std::endl;
            if (!read4({&std::cin, stream_deleter<std::istream, false>}, data))
                return -1;
            break;
        case 2:
            std::cerr << "Reading file: " << argv[1] << std::endl;
            if (!read4({new std::ifstream(argv[1], std::ios::in),
                        stream_deleter<std::istream, true>},
                       data))
                return -1;
            break;
        default:
#if !defined(FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION)
            assert(argc >= 5);
            try {
                for (int i = 0; i < 4; i++) {
                    data[i] = std::stod(argv[i + 1]);
                    std::cerr << data[i] << " ";
                }
                std::cerr << std::endl;
                break;
            } catch (...) {
                std::cerr << " failed to read input, aborting." << std::endl;
            }
#endif
            return -3;
    }
    std::cout << std::make_pair(data[0], data[1]) << " -> "
              << std::pair<double, double>(data[2], data[3]) << ":"
              << std::endl;
    std::cout << std::pair<int, int>(std::floor(data[0]), std::floor(data[1]))
              << " -> "
              << std::pair<int, int>(std::floor(data[2]), std::floor(data[3]))
              << ":" << std::endl;
    if (std::any_of(data.begin(), data.end(),
                    [](double d) { return !std::isfinite(d); }))
        return -1;
    if (std::any_of(data.begin(), data.end(),
                    [](double d) { return std::abs(d) > 100; }))
        return -2;
    if (std::abs(data[2] - data[0]) < 1e-6 &&
        std::abs(data[3] - data[1]) < 1e-6) {
        std::cerr << "input does not match precision reqs" << std::endl;
        return -3;
    }

    // using Stepper = distance::BresenhamVisitor;
    using Stepper = distance::dda;

    std::pair<int, int> first(std::floor(data[0]), std::floor(data[1]));
    std::pair<int, int> last(std::floor(data[2]), std::floor(data[3]));

    Stepper line(data[0], data[1], data[2], data[3]);
    assert(line.hasNext());
    auto pos = line.next();
    assert(pos == first);
    while (line.hasNext()) {
        pos = line.next();
        std::cout << pos << " ";
    }
    std::cout << std::endl;
    assert(pos == last);
    return 0;
}
