#include <catch2/catch.hpp>

#include "distance/PartialFactorial.h"

using namespace distance;

SCENARIO("Using partial factorial", "[partialfactorial]")
{
    GIVEN("Some partial factorials")
    {
        auto pf32 = PartialFactorial<3, 2>::value();
        using PF32 = PartialFactorial<3, 2>;
        REQUIRE(pf32 == (3 * 2 * 1) / (2 * 1));
        REQUIRE(PF32::value() == (3 * 2 * 1) / (2 * 1));

        auto pf30 = PartialFactorial<3, 0>::value();
        REQUIRE(pf30 == (3 * 2 * 1) / 1);
        auto pf34 = PartialFactorial<3, 4>::value();
        REQUIRE(pf34 == (3 * 2 * 1) / (4 * 3 * 2 * 1));
        auto pf33 = PartialFactorial<3, 3>::value();
        REQUIRE(pf33 == (3 * 2 * 1) / (3 * 2 * 1));
        auto pf03 = PartialFactorial<0, 3>::value();
        REQUIRE(pf03 == 1 / (3 * 2 * 1));

        auto pf00 = PartialFactorial<0, 0>::value();
        REQUIRE(pf00 == 1 / 1);  // empty product is one, not zero

        auto pf11 = PartialFactorial<1, 1>::value();
        REQUIRE(pf11 == 1 / 1);

        auto pf12_4 = PartialFactorial<12, 4>::value();
        REQUIRE(pf12_4 == 19958400);

        //    auto pfneg = PartialFactorial<-1,0>::value; // negativ input does
        //    not compile
    }
}
