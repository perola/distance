#include "mex.h"
#include "matrix.h"

#include "MarchingSquares.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  char buffer[128];
  const char * expectString = "Expects a two-dimensional double matrix and an optional iso-value.";
  if (nrhs == 0) {
    mexErrMsgTxt((snprintf(buffer, sizeof(buffer), "No input arguments given. %s.", expectString), buffer));
  }

  if (nrhs > 2) {
    mexErrMsgTxt((snprintf(buffer, sizeof(buffer), "Too many input arguments given. %s.", expectString), buffer));
  }

  double isoValue = 0.0;
  if(nrhs == 2)
    isoValue = *mxGetPr(prhs[1]);
  mexPrintf("Iso value: %f.\n", isoValue);

  if(!mxIsDouble(prhs[0]))
    mexErrMsgTxt((snprintf(buffer, sizeof(buffer), "Bad matrix format. %s.", expectString), buffer));

  int numRows = mxGetM(prhs[0]);
  int numCols = mxGetN(prhs[0]);
  if(numRows < 2 || numCols < 2)
    mexErrMsgTxt((snprintf(buffer, sizeof(buffer), "Bad matrix format. %s.", expectString), buffer));

  double * data = mxGetPr(prhs[0]);
  
  std::vector<std::pair<double, double> > lines = march(numRows, numCols, data, isoValue);
  mexPrintf("Number of lines: %d\n", lines.size());

  plhs[0] = mxCreateNumericMatrix(2, lines.size(), mxDOUBLE_CLASS, mxREAL);
  std::copy(lines.begin(), lines.end(), (std::pair<double, double>*)mxGetPr(plhs[0]));
}


